package com.testproject;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import com.appirio.automation.commands.SalesforceCommands;
import com.appirio.automation.commands.TestCommands;
import com.appirio.automation.configuration.Configuration;
import com.appirio.automation.configuration.FieldDetail;
import com.appirio.automation.configuration.Record;
import com.appirio.automation.exception.TestScriptException;
import com.appirio.automation.result.ScreenShot;
import com.appirio.automation.utility.Utility;
import com.google.common.base.Function;

public class TestProjectCommands extends SalesforceCommands {
	
	private Configuration config;
	
	public Configuration getTestProjectCommandsConfig() {
		return config;
	}

	public void setTestProjectCommandsConfig(Configuration config) {
		this.config = config;
		SalesforceCommands.setSalesforceCommandsConfig(config);
		TestCommands.setTestCommandsConfig(config);
	}
	
		
	/**
	 * Method to validate that each Product Group have Name and Image
	 * 
	 * @param productGroupTile
	 *            - Product Group Identifier
	 * @param productGroupName
	 *            - Product Group Name Identifier
	 * @param productGroupImage
	 *            - Product Group Image Identifier
	 * @throws TestScriptException
	 */
	public String[] verifyProductGrpNameImage(String productGroupTile, String productGroupName, String productGroupImage)
			throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that the all the product group have name and image";
		String status = "Pass";
		testStep[1] = "Displayed product group should have name and image";
		String productGroupFolderName = "ProductGroups_" + config.getTestCaseNumber();
		String mainfolder = config.getSnapshotFolder() + config.getPathJunction() + productGroupFolderName; // Gaurang
		String result = "";
		try {
			Path path = Paths.get(mainfolder);
			Files.createDirectory(path);

			// Creating folder for Product Groups with Name and Image
			String correctProductGroups = mainfolder + config.getPathJunction()
					+ "ProductGrps_with_name_and_image"; // Gaurang
			Path pass = Paths.get(correctProductGroups);
			Files.createDirectory(pass);

			// Creating folder for Product Groups with missing Name and Image
			String errorneousProductGroups = mainfolder + config.getPathJunction()
					+ "ProductGrp_with_missing_name_or_image"; // Gaurang
			Path fail = Paths.get(errorneousProductGroups);
			Files.createDirectory(fail);

			List<WebElement> productGroupTiles = findElements(getLocator(productGroupTile));
			for (WebElement wElement : productGroupTiles) {
				WebElement productGrpName = wElement.findElement(getLocator(productGroupName));
				WebElement productGrpImage = wElement.findElement(getLocator(productGroupImage));

				if (!productGrpName.getText().isEmpty() && productGrpImage.getAttribute("style").toLowerCase()
						.contains(productGrpName.getText().toLowerCase())) {

					new Actions(driver).moveToElement(productGrpName).perform();
					ScreenShot.takeElementSnapShot(driver, productGrpName.findElement(By.xpath("..")),
							correctProductGroups);
					// ScreenShot.takeElementSnapShot(driver,
					// productGrpName.findElement(By.xpath("..")), mainfolder);
					waitExplicit(2000);
				} else {
					new Actions(driver).moveToElement(wElement).perform();
					ScreenShot.takeElementSnapShot(driver, wElement, errorneousProductGroups);
					// ScreenShot.takeElementSnapShot(driver, wElement,
					// mainfolder);
					waitExplicit(2000);
					status = "Fail";

				}
			}

			if (status.equalsIgnoreCase("Fail")) {
				result = "Product groups don't having name and image. Please refer screenshot folder for correct and incorrect Product Groups.";
			} else {
				result = "All the product groups have the name and image. Please refer screenshot folder for evidence.";
			}
			// testStep[4] = mainfolder;
			testStep[4] = productGroupFolderName;
		} catch (Exception e) {
			result = "Displayed product group cannot be verified because of some error";
			status = "Fail";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
		} finally {
			testStep[3] = status;
			testStep[2] = result;
			 
			if (status.equalsIgnoreCase("Fail")) {
				throw new TestScriptException(testStep[2]);
			}
		}
		return testStep;
	}

	/**
	 * Method to validate the Product Group Access as per the Device Population
	 * 
	 * @param productGroups
	 *            - List of product grps that should be accessible.
	 * @param element
	 *            - Product Group Name identifier
	 * @throws TestScriptException
	 */
	public String[] verifyPopulatedProductGroups(String productGroups, String element) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that app displays all product group associated with device population";
		String status = "Pass";
		testStep[1] = "App should display all product group associated with device population";
		String result = "";
		String productGroupFolderName = "ProductGroups_" + config.getTestCaseNumber();
		String mainfolder = config.getSnapshotFolder() + config.getPathJunction() + productGroupFolderName; // Gaurang
		try {
			if (productGroups.contains("#")) {
				productGroups = config.getGlobalVariable(productGroups.substring(1));
			}

			// WebElement wElement = findElement(getLocator(element));

			Path path = Paths.get(mainfolder);
			Files.createDirectory(path);

			// Creating folder for Product Groups with Name and Image
			String correctProductGroups = mainfolder + config.getPathJunction() + "Expected ProductGroups"; // Gaurang
			Path pass = Paths.get(correctProductGroups);
			Files.createDirectory(pass);

			// Creating folder for Product Groups with missing Name and Image
			String errorneousProductGroups = mainfolder + config.getPathJunction() + "Unexpected ProductGroups"; // Gaurang
			Path fail = Paths.get(errorneousProductGroups);
			Files.createDirectory(fail);

			String[] productGrps = productGroups.split(",");
			List<String> expectedProductGrps = new ArrayList<String>();
			for (int count = 0; count < productGrps.length; count++) {
				expectedProductGrps.add(productGrps[count].toUpperCase());
			}

			List<WebElement> displayedProductGrpList = findElements(getLocator(element));
			HashMap<String, String> unexpectedProductGrps = new HashMap<String, String>();

			for (WebElement productGrp : displayedProductGrpList) {
				if (expectedProductGrps.contains(productGrp.getText())) {

					new Actions(driver).moveToElement(productGrp.findElement(By.xpath(".."))).perform();
					ScreenShot.takeElementSnapShot(driver, productGrp.findElement(By.xpath("..")),
							correctProductGroups);
					// ScreenShot.takeElementSnapShot(driver,
					// productGrpName.findElement(By.xpath("..")), mainfolder);
					waitExplicit(2000);

					expectedProductGrps.remove(productGrp.getText());
				} else {
					unexpectedProductGrps.put(productGrp.getText(), productGrp.getText());

					new Actions(driver).moveToElement(productGrp.findElement(By.xpath(".."))).perform();
					ScreenShot.takeElementSnapShot(driver, productGrp.findElement(By.xpath("..")),
							errorneousProductGroups);
					// ScreenShot.takeElementSnapShot(driver,
					// productGrpName.findElement(By.xpath("..")), mainfolder);
					waitExplicit(2000);

				}
			}

			if (expectedProductGrps.isEmpty() && !unexpectedProductGrps.isEmpty()) {
				status = "Fail";
				result = "Additional Product Grps " + unexpectedProductGrps.values() + " are there on the app";
			} else if (!expectedProductGrps.isEmpty() && !unexpectedProductGrps.isEmpty()) {
				status = "Fail";
				result = "Missing Product Grps are " + expectedProductGrps + " and Additional Product Grps are "
						+ unexpectedProductGrps.values();
			} else if (!expectedProductGrps.isEmpty() && unexpectedProductGrps.isEmpty()) {
				status = "Fail";
				result = "Missing Product Grps are " + expectedProductGrps;
			} else {
				result = "Expected Product Grps " + productGroups + " are there on the app";
			}
			testStep[4] = productGroupFolderName;
		} catch (Exception e) {
			result = "Displayed product group cannot be verified because of some error";
			status = "Fail";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
		} finally {
			testStep[3] = status;
			testStep[2] = result;
			 
			if (status.equalsIgnoreCase("Fail")) {
				throw new TestScriptException(testStep[2]);
			}
		}
		return testStep;
	}

	/**
	 * Method to parse provide Product Group Loaner Information from String to
	 * HashMap with each Product Grp as key.
	 * 
	 * @param loanerInformation
	 * @return
	 */
	private HashMap<String, HashMap<String, Integer>> parseLoanerInfo(String loanerInformation) {
		String[] inputData = loanerInformation.split(";");
		HashMap<String, HashMap<String, Integer>> allProductGroupLoanerData = new HashMap<String, HashMap<String, Integer>>();
		for (int dataCount = 0; dataCount < inputData.length; dataCount++) {

			String productGrpName = inputData[dataCount].substring(0, inputData[dataCount].indexOf("(")).toUpperCase();
			String expectedLoanerData = inputData[dataCount].substring(inputData[dataCount].indexOf("(") + 1,
					inputData[dataCount].indexOf(")"));
			String[] loanerInfo = expectedLoanerData.split(",");
			List<String> loanerInfoList = Arrays.asList(loanerInfo);
			HashMap<String, Integer> eachProductGrpLoanerData = new HashMap<String, Integer>();
			eachProductGrpLoanerData.put("Count", Integer.parseInt(loanerInfoList.get(0)));
			eachProductGrpLoanerData.put("Due", Integer.parseInt(loanerInfoList.get(1)));
			eachProductGrpLoanerData.put("Soon", Integer.parseInt(loanerInfoList.get(2)));

			allProductGroupLoanerData.put(productGrpName, eachProductGrpLoanerData);
		}

		return allProductGroupLoanerData;
	}

	/**
	 * Verifies the Loaner Information for the Product Groups
	 * 
	 * @param loanerInformation
	 *            - Loaner for each product group separated by ;. Eg -
	 *            Handpieces(21,4,0);Chargers(10,6,6)
	 * @param element
	 *            - Identifier for all the element accessed in this method.
	 *            Order should be Product Grp, Product GrpName, Loaner Section,
	 *            Loaner Header, Loaner Count, Loaner Due, Loaner Soon
	 * @throws TestScriptException
	 */
	public String[] verifyPopulatedLoaner(String loanerInformation, String element) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that app displays the loaner information as per the device population";
		String status = "Pass";
		testStep[1] = "App should display all the loaner information as per the device population";
		String productGroupLoanerFolder = "ProductGroupsLoaner_" + config.getTestCaseNumber();
		String mainfolder = config.getSnapshotFolder() + config.getPathJunction() + productGroupLoanerFolder; // Gaurang
		String result = "";
		try {

			Path path = Paths.get(mainfolder);
			Files.createDirectory(path);

			// Creating folder for Product Groups with Correct Loaner
			// Information
			String correctProductGroups = mainfolder + config.getPathJunction()
					+ "ProductGrps_with_correct_LoanerInformation"; // Gaurang
			Path pass = Paths.get(correctProductGroups);
			Files.createDirectory(pass);

			// Creating folder for Product Groups with Incorrect Loaner
			// Information
			String errorneousProductGroups = mainfolder + config.getPathJunction()
					+ "ProductGrp_with_incorrect_LoanerInformation"; // Gaurang
			Path fail = Paths.get(errorneousProductGroups);
			Files.createDirectory(fail);

			String[] identifiers = element.split(",");
			List<String> localIdentifiers = Arrays.asList(identifiers);
			HashMap<String, HashMap<String, Integer>> allProductGroupLoanerData = parseLoanerInfo(loanerInformation);
			HashMap<String, Integer> eachProductGrpLoanerData;

			List<WebElement> displayedProductGrps = findElements(getLocator(localIdentifiers.get(0)));
			for (WebElement productGrp : displayedProductGrps) {
				String productGrpName = productGrp.findElement(getLocator(localIdentifiers.get(1))).getText();

				// To handle the scenario when Product Grp should not have the
				// Loaner section
				if (!allProductGroupLoanerData.containsKey(productGrpName)) {
					try {
						productGrp.findElement(getLocator(localIdentifiers.get(2)));
						status = "Fail";
						new Actions(driver).moveToElement(productGrp).perform();
						ScreenShot.takeElementSnapShot(driver, productGrp, errorneousProductGroups);
						waitExplicit(2000);
					} catch (NoSuchElementException e) {
						new Actions(driver).moveToElement(productGrp).perform();
						ScreenShot.takeElementSnapShot(driver, productGrp, correctProductGroups);
						waitExplicit(2000);
					}
				}

				// To handle the scenario when Product Grp should have the
				// Loaner section
				else {
					try {
						productGrp.findElement(getLocator(localIdentifiers.get(2)));
						eachProductGrpLoanerData = allProductGroupLoanerData.get(productGrpName);
						String expectedLoanerCount = eachProductGrpLoanerData.get("Count").toString();
						String expectedLoanerDue = eachProductGrpLoanerData.get("Due").toString();
						String expectedLoanerSoon = eachProductGrpLoanerData.get("Soon").toString();
						String expectedloanerHeader = "Loaner";

						By loanerHeader = getLocator(localIdentifiers.get(3));
						By loanerCount = getLocator(localIdentifiers.get(4));
						By loanerDue = getLocator(localIdentifiers.get(5));
						By loanerSoon = getLocator(localIdentifiers.get(6));

						// To validate the loaner section has header, expected
						// count, expected due and expected soon
						if (productGrp.findElement(loanerHeader).getText().contains(expectedloanerHeader)
								&& productGrp.findElement(loanerCount).getText().equals(expectedLoanerCount)
								&& productGrp.findElement(loanerDue).getText().contains(expectedLoanerDue)
								&& productGrp.findElement(loanerSoon).getText().contains(expectedLoanerSoon)) {
							new Actions(driver).moveToElement(productGrp).perform();
							ScreenShot.takeElementSnapShot(driver, productGrp, correctProductGroups);
							waitExplicit(2000);
						} else {
							status = "Fail";
							new Actions(driver).moveToElement(productGrp).perform();
							ScreenShot.takeElementSnapShot(driver, productGrp, errorneousProductGroups);
							waitExplicit(2000);
						}
					} catch (NoSuchElementException e) {
						new Actions(driver).moveToElement(productGrp).perform();
						ScreenShot.takeElementSnapShot(driver, productGrp, errorneousProductGroups);
						waitExplicit(2000);
						status = "Fail";
					}
				}
			}

			if (status.equalsIgnoreCase("Fail")) {
				result = "Product groups don't have correct loaner information. Please refer screenshot folder for correct and incorrect Product Groups.";
			} else {
				result = "All the product groups have the correct loaner information. Please refer screenshot folder for evidence.";
			}

			// testStep[4] = mainfolder;
			testStep[4] = productGroupLoanerFolder;

		} catch (Exception e) {
			result = "Displayed loaner information for product group cannot be verified because of some error";
			status = "Fail";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
		} finally {
			testStep[3] = status;
			testStep[2] = result;
			 

			if (status.equalsIgnoreCase("Fail")) {
				throw new TestScriptException(testStep[2]);
			}
		}
		return testStep;
	}

	/**
	 * Method to capture Device Graph Down Values and assign weight to it
	 * 
	 * @param graphDropDown
	 * @return
	 * @throws TestScriptException
	 */
	private HashMap<Integer, String> deviceGraphDropDownValues(String graphDropDown) throws TestScriptException {
		try {
			WebElement wElement = findElement(getLocator(graphDropDown));
			wElement.click();
			List<WebElement> dropdownValues = wElement.findElements(By.xpath("..//li"));
			HashMap<Integer, String> graphDropDownValues = new HashMap<Integer, String>();
			int weight = 0;
			for (WebElement dropdownValue : dropdownValues) {
				graphDropDownValues.put(weight, dropdownValue.getText());
				weight++;
			}

			wElement.click();
			return graphDropDownValues;
		} catch (Exception e) {
			throw new TestScriptException("Graph Drop Down values cannot be captured");
		}
	}

	/**
	 * Method to calculate the sum of bars for Uses Per Day GraphValues
	 * 
	 * @param element
	 * @return
	 * @throws TestScriptException
	 */
	private int usesPerDayGraphValuesSum(String barIdentifier) throws TestScriptException {
		try {
			List<WebElement> graphBarValues = findElements(getLocator(barIdentifier));
			int sumOfValues = 0;
			for (WebElement graphBarValue : graphBarValues) {
				String exactValue = graphBarValue.getText();
				Double exactDoubleValue = Double.parseDouble(exactValue);
				sumOfValues = sumOfValues + exactDoubleValue.intValue();
			}
			return sumOfValues;
		} catch (Exception e) {
			throw new TestScriptException("Graph values cannot be accessed");
		}

	}

	/**
	 * Method for selecting the value from the dropdown of the graph.
	 * 
	 * @param graphDropDown
	 * @param weightForValueToBeSelected
	 * @throws TestScriptException 
	 */

	private void usesPerDayGraphSelection(String graphDropDown, int weightForValueToBeSelected) throws TestScriptException {
		WebElement wElement = findElement(getLocator(graphDropDown));
		wElement.click();

		int dropDownValueToBeSelected = weightForValueToBeSelected + 1;

		WebElement newWeekSelection = wElement.findElement(By.xpath("..//li[" + dropDownValueToBeSelected + "]"));
		newWeekSelection.click();
		waitExplicit(2000);
	}

	/**
	 * Method to verify Uses Per Day Graph Functionality
	 * 
	 * @param usesPerDayGraphIdentifier
	 * @param graphDropDown
	 * @param graphBarValue
	 * @throws TestScriptException
	 */
	public String[] verifyUsesPerDayGraph(String usesPerDayGraphIdentifier, String graphDropDown, String graphBarValue)
			throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that Uses per graph is working properly";
		String status = "Pass";
		testStep[1] = "Uses per Day graph should work properly";
		String UsesPerGraphFolder = "UsesPerDayGraph_" + config.getTestCaseNumber();
		String mainfolder = config.getSnapshotFolder() + config.getPathJunction() + UsesPerGraphFolder; // Gaurang
		String result = "";
		try {

			Path path = Paths.get(mainfolder);
			Files.createDirectory(path);

			WebElement usesPerDayGraph = findElement(getLocator(usesPerDayGraphIdentifier));

			HashMap<Integer, String> graphWeekSelector = deviceGraphDropDownValues(graphDropDown);
			String defautlWeekSelected = findElement(getLocator(graphDropDown)).getText().trim();
			int weightForNewWeekToBeSelected = -1;
			int weightForSelectedWeek = -1;
			int sumOfValues = 0;
			int newSumOfValues = 0;

			for (Integer i : graphWeekSelector.keySet()) {
				String valueLoop = graphWeekSelector.get(i);

				if (valueLoop.equals(defautlWeekSelected)) {
					weightForSelectedWeek = i;
					break;
				}

			}

			sumOfValues = usesPerDayGraphValuesSum(graphBarValue);

			new Actions(driver).moveToElement(usesPerDayGraph).perform();
			ScreenShot.takeElementSnapShot(driver, usesPerDayGraph, mainfolder);
			waitExplicit(2000);

			if (weightForSelectedWeek == 0) {
				weightForNewWeekToBeSelected = graphWeekSelector.size() - 1;
				usesPerDayGraphSelection(graphDropDown, weightForNewWeekToBeSelected);

				newSumOfValues = usesPerDayGraphValuesSum(graphBarValue);

				if (!(newSumOfValues > sumOfValues)) {
					status = "Fail";
				}

				new Actions(driver).moveToElement(usesPerDayGraph).perform();
				ScreenShot.takeElementSnapShot(driver, usesPerDayGraph, mainfolder);
				waitExplicit(2000);

			} else if (weightForSelectedWeek == graphWeekSelector.size() - 1) {
				weightForNewWeekToBeSelected = 0;
				usesPerDayGraphSelection(graphDropDown, weightForNewWeekToBeSelected);

				newSumOfValues = usesPerDayGraphValuesSum(graphBarValue);

				if (!(newSumOfValues < sumOfValues)) {
					status = "Fail";
				}

				new Actions(driver).moveToElement(usesPerDayGraph).perform();
				ScreenShot.takeElementSnapShot(driver, usesPerDayGraph, mainfolder);
				waitExplicit(2000);
			} else {
				weightForNewWeekToBeSelected = weightForSelectedWeek - 1;
				usesPerDayGraphSelection(graphDropDown, weightForNewWeekToBeSelected);

				newSumOfValues = usesPerDayGraphValuesSum(graphBarValue);

				if (!(newSumOfValues < sumOfValues)) {
					status = "Fail";
				}

				new Actions(driver).moveToElement(usesPerDayGraph).perform();
				ScreenShot.takeElementSnapShot(driver, usesPerDayGraph, mainfolder);
				waitExplicit(2000);

				weightForNewWeekToBeSelected = weightForSelectedWeek + 1;
				usesPerDayGraphSelection(graphDropDown, weightForNewWeekToBeSelected);

				newSumOfValues = usesPerDayGraphValuesSum(graphBarValue);

				if (!(newSumOfValues > sumOfValues)) {
					status = "Fail";
				}

				new Actions(driver).moveToElement(usesPerDayGraph).perform();
				ScreenShot.takeElementSnapShot(driver, usesPerDayGraph, mainfolder);
				waitExplicit(2000);
			}

			if (status.equalsIgnoreCase("Fail")) {
				result = "Uses per graph is not functioning properly. Please refer screenshot folder for evidence.";
			} else {
				result = "Uses per graph is functioning properly. Please refer screenshot folder for evidence.";
			}

			// testStep[4] = mainfolder;
			testStep[4] = UsesPerGraphFolder;
		} catch (Exception e) {
			result = "Uses per day graph cannot be verified because of some error";
			status = "Fail";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
		} finally {
			testStep[3] = status;
			testStep[2] = result;
			 

			if (status.equalsIgnoreCase("Fail")) {
				throw new TestScriptException(testStep[2]);
			}

		}
		return testStep;
	}

	/**
	 * Method to select the value for
	 * 
	 * @param dropDownValue
	 * @param variable
	 * @throws TestScriptException
	 */
	public void appSettingsDropDownSelection(String dropDownValue, String variable) throws TestScriptException {
		String value = "";
		try {
			HashMap<Integer, String> graphWeekSelector = deviceGraphDropDownValues(dropDownValue);
			String defautlWeekSelected = findElement(getLocator(dropDownValue)).getText().trim();
			int weightForNewWeekToBeSelected = -1;
			int weightForSelectedWeek = -1;

			for (Integer i : graphWeekSelector.keySet()) {
				String valueLoop = graphWeekSelector.get(i);

				if (valueLoop.equals(defautlWeekSelected)) {
					weightForSelectedWeek = i;
					break;
				}
			}

			if (weightForSelectedWeek == graphWeekSelector.size() - 1) {
				weightForNewWeekToBeSelected = weightForSelectedWeek - 1;
				usesPerDayGraphSelection(dropDownValue, weightForNewWeekToBeSelected);
			} else {
				weightForNewWeekToBeSelected = weightForSelectedWeek + 1;
				usesPerDayGraphSelection(dropDownValue, weightForNewWeekToBeSelected);
			}
			value = graphWeekSelector.get(weightForNewWeekToBeSelected);
			config.setMapGlobalVariable(variable, value);

		} catch (Exception e) {
			throw new TestScriptException(testStep[2]);
		}
	}
	
	/** This method creates My Contacts for the mentioned Account with the specified values.
	 * @param tabName - My Contacts
	 * @param AccountName - Account for which we need to create My Contacts
	 * @param recValues - Path of the file which contains the value of all the fields of the new contact that needs to be created.
	 * @param saveOption - 
	 * @return
	 * @throws TestScriptException
	 */
	public String[] createMyContacts(String tabName, String AccountName, String recValues, String saveOption) throws TestScriptException{
		testStep = getStepInstance();
		testStep[0] = "Verify that the current url is saved";
		testStep[1] = "Current url should be saved";
		String status = "Pass";
		String failedRecords = "";
		try{
			
			List<Record> recordsList = getRecValues(recValues, tabName);
			for (Record record : recordsList) {

				clickTab(tabName);
				
				Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(60, TimeUnit.SECONDS)
						.pollingEvery(5, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);

				wait.until(new Function<WebDriver, WebElement>() {
					public WebElement apply(WebDriver driver) {

						return driver.findElement(By.xpath("//*[contains(@class,'Title') and contains(text(),'My Contacts')]"));
					}
				});
				
				
				WebElement newBtn = findElement(By.xpath("//*[contains(@value,'New')]"));
				newBtn.click();
				
				Utility.sleep(8000);
				
				String defaultWindow = driver.getWindowHandle();
				WebElement webElement = wait.until(new Function<WebDriver, WebElement>() {
					public WebElement apply(WebDriver driver) {

						return driver.findElement(By.xpath("//*[@class='lookupInput']//input[1]"));
					}
				});
				
				WebElement parent = webElement.findElement(By.xpath(".."));
				WebElement lookup = parent.findElement(By.tagName("img"));
				lookup.click();

				Utility.sleep(5000);
				boolean flag = (boolean) switchToWindowUsingTitle("Search");
				String lookupPageHandle = driver.getWindowHandle();
				WebElement searchFrame = driver.findElement(By.id("searchFrame"));
				driver.switchTo().frame(searchFrame);
				WebElement searchString = findElement(By.id("lksrch"));
				searchString.clear();
				searchString.sendKeys(AccountName);
				findElement(By.name("go")).click();
				Utility.sleep(2000);
				driver.switchTo().window(lookupPageHandle);
				WebElement resultFrame = driver.findElement(By.id("resultsFrame"));
				driver.switchTo().frame(resultFrame);

				WebElement lookupSearch = findElement(By.cssSelector(".list"));
				List<WebElement> tr = lookupSearch.findElement(By.tagName("tbody")).findElements(By.tagName("tr"));
				tr.remove(0);
				for (WebElement trElement : tr) {
					if (trElement.getAttribute("class").isEmpty()) {
						break;
					} else {
						WebElement link = trElement.findElement(By.tagName("th")).findElement(By.tagName("a"));
						String linkText = link.getText();
						if (linkText.equalsIgnoreCase(AccountName)) {
							link.click();
							break;
						}
					}
				}
				driver.switchTo().window(defaultWindow);
				
				wait.until(new Function<WebDriver, WebElement>() {
					public WebElement apply(WebDriver driver) {

						return driver.findElement(By.xpath("//*[text()='Smart Contact Search' and @class='pageDescription']"));
					}
				});
				
				newBtn = driver.findElement(By.xpath("//*[contains(@class,'button-group')]//*[contains(@value,'New')]"));
				newBtn.click();
				
				wait.until(new Function<WebDriver, WebElement>() {
					public WebElement apply(WebDriver driver) {

						return driver.findElement(By.xpath("//*[text()='Contact Edit' and @class='mainTitle']"));
					}
				});

				enterValuesForRecCreation(record,tabName);
				String pageTitle = driver.getTitle();
				String pageTitleNew;

				if (saveOption.equalsIgnoreCase("Save") || saveOption.equalsIgnoreCase("Save & New")) {
					String xpath = "//input[@class='btn' and @title='" + saveOption + "']";
					findElement(By.xpath(xpath)).click();
				} else {
					findElement(getLocator(saveOption)).click();
				}

				waitExplicit();
				pageTitleNew = driver.getTitle();

				/*
				 * if(pageTitle.contains("Community")){
				 * if(!pageTitleNew.contains("~")){ status = "Fail"; testStep[4]
				 * = ScreenShot.takeBrowserSnapShot(driver,Configuration.
				 * snapshotFolder); failedRecords = failedRecords + " " +
				 * record.getFieldList().get(0); } } else {
				 * if(pageTitleNew.contains("Edit") &&
				 * !pageTitleNew.contains("Edition")){ status = "Fail";
				 * testStep[4] =
				 * ScreenShot.takeBrowserSnapShot(driver,Configuration.
				 * snapshotFolder); failedRecords = failedRecords + " " +
				 * record.getFieldList().get(0); } }
				 */

				if (!status.equalsIgnoreCase("Fail")) {
					FieldDetail recordID = new FieldDetail();
					recordID.setName("SFDC Record ID");
					recordID.setType("ID");
					waitForElementToBeVisible(waitTime, "Object Detail");
					recordID.setValue(driver.getCurrentUrl().substring(driver.getCurrentUrl().lastIndexOf("/") + 1));
					record.add(recordID);
				}
			}

			if (status.equals("Fail")) {
				testStep[2] = "Following records for " + tabName + " could not be created - " + failedRecords
						+ " Please see the screenshot for error for the last failed records. For other screenshots please refer to the results folder";
			} else {
				testStep[2] = tabName + " record is created";
				testStep[4] = "";
				mapCreatedRecords.put(tabName, recordsList);
			}
		} catch (Exception e) {
			testStep[2] = tabName + " record could not be created because of error";
			status = "Fail";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			throw new TestScriptException(e.getMessage());
		} finally {
			testStep[3] = status;
			if (status.equals("Fail")) {
				throw new TestScriptException(testStep[2]);
			}
		}
		return testStep;
	
  }
	
	
}
