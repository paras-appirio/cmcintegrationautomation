package com.testproject;

import java.util.List;

public class ApiRequest {

	private String storyId;
	private String issueId;
	private String refreshToken;
	private String testCaseList;
	
	public String getStoryId() {
		return storyId;
	}
	public void setStoryId(String storyId) {
		this.storyId = storyId;
	}
	public String getIssueId() {
		return issueId;
	}
	public void setIssueId(String issueId) {
		this.issueId = issueId;
	}
	public String getRefreshToken() {
		return refreshToken;
	}
	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}
	public String getTestCasesList() {
		return testCaseList;
	}
	public void setTestCaseList(String testCaseList) {	
		this.testCaseList = testCaseList;
	}
	
	
}
