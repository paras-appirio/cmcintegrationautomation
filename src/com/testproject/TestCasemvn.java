package com.testproject;

import java.util.List;

public class TestCasemvn {

	private String testCaseId;
	private String status;
	private int totalSize;
	private List records;
	private String done;
	public String getTestCaseId() {
		return testCaseId;
	}
	public void setTestCaseId(String testCaseId) {
		this.testCaseId = testCaseId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getTotalSize() {
		return totalSize;
	}
	public void setTotalSize(int totalSize) {
		this.totalSize = totalSize;
	}
	
	@SuppressWarnings("rawtypes")
	public List getRecords() {
		return records;
	}
	public void setRecords(@SuppressWarnings("rawtypes") List records) {
		this.records = records;
	}
	public String getDone() {
		return done;
	}
	public void setDone(String done) {
		this.done = done;
	}
	
	
	
}
