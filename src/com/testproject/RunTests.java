package com.testproject;

import java.io.IOException; 
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;


import com.appirio.automation.configuration.Configuration;
import com.appirio.automation.configuration.TestCase;
import com.appirio.automation.configuration.TestStep;
import com.appirio.automation.exception.TestScriptException;
import com.appirio.automation.result.TestResult;
import com.appirio.automation.utility.Utility;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import com.testproject.TestProjectCommands;

import jxl.read.biff.BiffException;

public class RunTests {
	public static final Logger logger = Logger.getLogger(RunTests.class.getName());
	private static Configuration config;
	private static TestProjectCommands testProjectCommands;
	private static String executionStatus = "Pass";
	private int testCaseStartIndex;
	private TestCase executingTestCase;
	private HashMap<Integer, TestStep> executedStepsMap;

	public Configuration getConfig() {
		return config;
	}

	public static void setConfig(Configuration config) {
		RunTests.config = config;
	}

	public static void main(String[] args) throws BiffException, IOException {

		testProjectCommands = new TestProjectCommands();
		TestResult testResult = new TestResult();
		RunTests rt = null;
		try {
			if (args.length < 1) {
				System.err.println("Configure file is not supplied as argument. "
						+ "Please provide the path for the config file.");
				return;
			}
			else if (args.length > 1){
				System.err.println("Please provide only config file as the arguments and remove unnecessary files.");
				return;
			}
			else{
			String configureFile = args[0];
			if (configureFile.trim().length() == 0) {
				logger.error("config file name not appropriate, please provide proper file name.");
				return;
			}

			rt = new RunTests();
			config = new Configuration(configureFile, testProjectCommands, logger,testResult);
			testResult.configureTestResult(config);
			testProjectCommands.setTestProjectCommandsConfig(config);
			rt.runAllTestCases();
			}
		} catch (TestScriptException e) {
			logger.error(e.getMessage());
			// e.printStackTrace();
		} catch(Exception e){
			logger.error(e.getMessage());
		} finally {
			if(config!=null){
			if (config.getTestResult() != null && (config.getTestResult().getTestStep() > 0)) {
				config.getTestResult().closeTestResult();
			}		
			}
		}

	}

	/**
	 * run all test cases in spread sheet list.
	 * @throws JSONException 
	 * 
	 * @throws DriveScriptException
	 *             - if there is any problem running test.
	 */
	public void runAllTestCases() throws TestScriptException, JSONException {
		if (config.getSpreadSheetList() == null) {
			throw new TestScriptException("spread sheet list is null. Please run configure first.");
		}

		try {
			for (int i = 0; i < config.getSpreadSheetList().length; i++) {
				if (i == 0) {
					testCaseStartIndex = 0;
				} else {
					testCaseStartIndex = TestResult.tStep.size();
				}
				try {
					RunTests.executionStatus = "Pass";
					config.getLogger().info("");
					config.getLogger().info("------------------------------Started execution for the test case "+config.getSpreadSheetList()[i] +"------------------------------");
					config.getLogger().info("");
					runTestCase(config.getSpreadSheetList()[i]);
					// testResult.testResultStatus = "Pass";
				} catch (Exception e) {
					RunTests.executionStatus = "Fail";
					// testResult.testResultStatus = "Fail";
					if(e.getMessage()!=null){
						logger.info(e.getMessage());							
					}
					else{
						
						
						e.printStackTrace();
					}
					testProjectCommands.closeBrowser("N");
					continue;
				} finally {
					String testCaseResult = RunTests.executionStatus;

					
					config.getTestResult().setTestResulStatus(RunTests.executionStatus);
					if (config.getTestResult().testResultStatus.equalsIgnoreCase("Fail")) {
						logger.info("Test case for: " + config.getSpreadSheetList()[i] + " has failed.");
					}
					if (config.getTestResult().testResultStatus.equalsIgnoreCase("Pass")) {
						logger.info("Test case for: " + config.getSpreadSheetList()[i] + " has passed.");
					}
					if (config.getTestResult() != null && config.getTestResult().getTestStep() > 0) {
						config.getTestResult().generateTestResult(testCaseStartIndex, executingTestCase);
						config.getTestResult().generateTestResult(testCaseStartIndex, executingTestCase);
					}
					//TestCase objTestCase = new TestCase();
					ApiRequest api = new ApiRequest();
					//String testCasestoExecute =(objTestCase.getTestCaseName());
					System.out.println("TestCaseNumber"+config.gettestCaseNumbers().get(i) );
					api.setTestCaseList(config.gettestCaseNumbers().get(i));
					
					runTestSuite("5Aep861VUUSqKxtr92Zv5fhUBcsuR94Z7Y.gYRwoJ_epN295crbClcT5ai4LALCL5Q2At3bXzymbGrPonuHP1IO", api,testCaseResult);

				}
			}
		} finally {
			config.getTestResult().generateHTMLResultReport();
		}
	}

	public void runTestCase(String filename) throws TestScriptException, BiffException, IOException {
		config.setTestStepInstance(null);
		Object object = null;
		int nextTestCommandNumber = 1;
		boolean stepToBeExecuted = true;
		Utility util = new Utility(config);
		String testCaseName ="";
		boolean exception = false;
		List<String> exceptionResult = new ArrayList<String>();
		String[] exceptionTestResult = null;
		// try{
		HashMap<Integer, TestStep> testCase = TestStep.readData(filename, "Sheet1");
		Configuration.setTestCaseLocalInputData(filename);
		testCaseName = filename.substring("resources/".length(), filename.indexOf("."));
		if(testCaseName.lastIndexOf("/")!=-1){
			config.setTestCaseNumber(testCaseName.substring(testCaseName.lastIndexOf("/") + 1));
			executingTestCase = Configuration.getTestCases().get(config.getTestCaseNumber());
		}
		else{
				config.setTestCaseNumber(testCaseName);
				executingTestCase = Configuration.getTestCases().get(config.getTestCaseNumber());
		}
		executedStepsMap = new HashMap<Integer, TestStep>();
		config.setExecutedStepsMap(executedStepsMap);
		
		config.getTestResult().configureTestCaseResult(config.getTestCaseNumber());
		
		TestStep testStep = null;
		while (stepToBeExecuted) {
			String[] testStepInstanceOld = config.getTestStepInstance();
			testStep = testCase.get(nextTestCommandNumber);
			String command = testStep.getCommand().toUpperCase();
			command = command.trim();
			if (config.getMethodsMap().get(command) == null) {
				testProjectCommands.closeBrowser("N");
				exceptionResult.add("Verify that the keyword " + command + " is present in mapRuleFile " + config.getMethodMappingFile());
				exceptionResult.add("Keyword for the" + command + " should be found in mapRuleFile " + config.getMethodMappingFile());
				exceptionResult.add("Could not find keyword " + command + " in mapRuleFile " + config.getMethodMappingFile());
				exceptionResult.add("Fail");
				exceptionResult.add("");
				if(config.isRequirementReference()){
					exceptionResult.add("");
					exceptionTestResult = exceptionResult.toArray(new String[exceptionResult.size()]);
				}
				else {
					exceptionTestResult = exceptionResult.toArray(new String[exceptionResult.size()]);
				}
				TestResult.addItemToReport(exceptionTestResult);
				throw new TestScriptException(
						"Could not find keyword " + command + " in mapRuleFile " + config.getMethodMappingFile());
			}
			Class<?>[] argsTypes = config.getMethodsMap().get(command).getParameterTypes();
			Object[] args = new Object[config.getMethodsMap().get(command).getGenericParameterTypes().length];
			
			/*if(args.length!=testStep.getArgumentList().size()){
				throw new TestScriptException("Argument list for the command " +command+" is not correct, please review it and try again.");
			}*/
			
			// fill args
		  try{
			for (int argIdx = 0; argIdx < args.length; argIdx++) {
				String argValue = testStep.getArgumentList().get(argIdx);
				argValue = util.replaceHashTag(argValue);

				if (argValue.startsWith("#")) {
					if (argValue.contains(",")) {
						String[] tempArray = argValue.split(",");
						argValue = "";
						String temp = "";
						for (int i = 0; i < tempArray.length; i++) {
							temp = config.getGlobalVariable(tempArray[i].substring(1));
							if (i == 0) {
								argValue = temp;
							} else {
								argValue = argValue + "," + temp;
							}
						}

					} else {
						argValue = config.getGlobalVariable(argValue.substring(1));
					}
				}
				try {
					Constructor<?> cons = argsTypes[argIdx].getConstructor(String.class);
					if (argsTypes[argIdx] == Integer.class) {
						DecimalFormat formatter = new DecimalFormat("0");
						// convert number.
						argValue = formatter.format(Double.valueOf(argValue)).toString();
					}
					args[argIdx] = cons.newInstance(argValue);
				} catch (NoSuchMethodException e) {
					// salesforceCommands.closeBrowser();
					throw new TestScriptException(
							"No such construct method: " + argsTypes[argIdx].toString() + e.getMessage(), e);
				} catch (SecurityException e) {
					// salesforceCommands.closeBrowser();
					throw new TestScriptException("Security violation. Class: " + argsTypes[argIdx].toString()
							+ " constructor: " + argsTypes[argIdx].toString() + e.getMessage(), e);
				} catch (InstantiationException e) {
					// salesforceCommands.closeBrowser();
					throw new TestScriptException("New instance error. Class: " + argsTypes[argIdx].toString()
							+ " constructor: " + argsTypes[argIdx].toString() + e.getMessage(), e);
				} catch (IllegalAccessException e) {
					// salesforceCommands.closeBrowser();
					throw new TestScriptException("Illegal Access. Class: " + argsTypes[argIdx].toString()
							+ " constructor: " + argsTypes[argIdx].toString() + e.getMessage(), e);
				} catch (IllegalArgumentException e) {
					// salesforceCommands.closeBrowser();

					throw new TestScriptException("Illegal Argument. Class: " + argsTypes[argIdx].toString()
							+ " constructor: " + argsTypes[argIdx].toString() + e.getMessage(), e);
				} catch (InvocationTargetException e) {
					// salesforceCommands.closeBrowser();
					throw new TestScriptException("Invocation Target error. Class: " + argsTypes[argIdx].toString()
							+ " constructor: " + argsTypes[argIdx].toString() + e.getMessage(), e);
				}
			}
			try {
				int derivedNextCommandNumber = 0;
				object = config.getMethodsMap().get(command).invoke(testProjectCommands, args);

				derivedNextCommandNumber = testProjectCommands.getNextTestCommandRow();
				if (derivedNextCommandNumber != -1) {
					testProjectCommands.resetNextTestCommandRow();
					nextTestCommandNumber = derivedNextCommandNumber;
				} else {
					// this is to make sure that test result is generated for
					// first command
					if (nextTestCommandNumber == 1) {
						if (!object.getClass().equals(Boolean.class)) {
							config.setTestStepInstance((String[]) object);
						if(config.isRequirementReference()){
								config.getTestStepInstance()[5] = testStep.getReqReference();
						}
						TestResult.addItemToReport(config.getTestStepInstance());
						}						
					}
					nextTestCommandNumber++;
				}
				if (command.equalsIgnoreCase("closeBrowser")) {
					stepToBeExecuted = false;
				}
				testStep.setArguments(args);

				config.getExecutedStepsMap().put(testStep.getTestStepNumber(), testStep);
				logger.info(command + " args= " + Arrays.toString(args));

			} catch (IllegalAccessException e) {
				throw new TestScriptException("Illegal Access error. COMMAND = " + command + " method = "
						+ config.getMethodsMap().get(command).getName() + e.getMessage(), e);

			} catch (IllegalArgumentException e) {
				String exceptionMsg = "Illegal Argument error. COMMAND = " + command + " method = "
						+ config.getMethodsMap().get(command).getName();
				exceptionMsg += "argument list: ";
				for (int j = 0; j < args.length; j++) {
					exceptionMsg += args[j].toString() + ",";
				}
				exceptionMsg += e.getMessage();
				throw new TestScriptException(exceptionMsg, e);
			} catch (InvocationTargetException e) {
				// For continuing execution on next command
				object = TestScriptException.object;
				if (!testStep.getToBeAborted().equalsIgnoreCase("N")) {
					throw new TestScriptException(
							"Invocation Target error.An exception thrown by an invoked method. COMMAND = " + command
									+ " method = " + config.getMethodsMap().get(command).getName() + " "
									+ e.getTargetException().getMessage());
				}
				RunTests.executionStatus = "Fail";
				nextTestCommandNumber++;
			} catch (Exception e) {
				throw new TestScriptException("Following exception occured " + e.getMessage());
			} finally {

				if (!object.getClass().equals(Boolean.class)) {
					config.setTestStepInstance((String[]) object);
				}

				if (testStepInstanceOld != null && !testStepInstanceOld.equals(config.getTestStepInstance())) {
					if(config.isRequirementReference()){
						config.getTestStepInstance()[5] = testStep.getReqReference();
					}
					TestResult.addItemToReport(config.getTestStepInstance());
				}
			}
		  } catch (Exception e) {
				exception = true;
				throw new TestScriptException("Following exception occured " + e.getMessage());
			} finally{
				if(exception){
					exceptionResult.add("Verify that the command "+command + " is executed");
					exceptionResult.add(command + " should be executed");
					exceptionResult.add(command + " cannot be executed because of an exception. Please refer logs/info.log for more information");
					exceptionResult.add("Fail");
					exceptionResult.add("");
					if(config.isRequirementReference()){
						exceptionResult.add("");
						exceptionTestResult = exceptionResult.toArray(new String[exceptionResult.size()]);
					}
					else {
						exceptionTestResult = exceptionResult.toArray(new String[exceptionResult.size()]);
					}
					TestResult.addItemToReport(exceptionTestResult);
				}
			}	
		  }
		}	
		
	
	@SuppressWarnings("unchecked")
	public static void runTestSuite(String refreshToken,ApiRequest apiRequest, String resultStatus ) throws JSONException{
		//Execute sample automation script
		//execute();

		String testCases = apiRequest.getTestCasesList();
        System.out.println(testCases);
        for(String testCaseName : testCases.split(";")) {
        // use jersey client library for simple http client use
        final Client client = ClientBuilder.newClient();

        // call oauth token service to get a refresh token
        //final WebTarget tokenTarget = client.target("https://cmc-api.appirio.net/v_beta/oauth/token");
        //final WebTarget tokenTarget = client.target("https://cmc-api.appirio.net/v_beta/oauth/token");
        final WebTarget tokenTarget = client.target("https://cmc-api-dev.appirio.net/v_beta/oauth/token");
   
        // using map for simplicity. could also build pojos
        final Map<String, String> refreshTokenMap = ImmutableMap.of("refreshToken", refreshToken);
        System.out.println("Refresh token "+refreshTokenMap);
        System.out.println("tokenTarget= "+ tokenTarget);
        
        Map<String, String> tokenResult = tokenTarget.request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(refreshTokenMap, MediaType.APPLICATION_JSON_TYPE), Map.class);
        System.out.println("Token Result"+ tokenResult);
        
        final String accessToken = tokenResult.get("accessToken");
        
        
        System.out.println("access token = " + accessToken);

        // use access token to retrieve a story
        //final WebTarget testCaseTarget = client.target("https://cmc-api.appirio.net/v_beta/test-cases");

        //Builder builder = testCaseTarget.request(MediaType.APPLICATION_JSON_TYPE).header("AuthorizationToken", accessToken);
        
        // Create Test Cases
        //Map<String, String> invoke = builder.post(Entity.entity(ImmutableMap.of("testCaseName","Test Case 1", "storyId",apiRequest.getStoryId()), MediaType.APPLICATION_JSON_TYPE),Map.class);
        //String testCaseId = invoke.get("content");
        
        // Create Test Result
        
        	if(testCaseName != ""){
        		final WebTarget testCaseTarget = client.target("https://cmc-api-dev.appirio.net/v_beta/test-cases?query=testCaseNumber='"+testCaseName.toString()+"'");
		        Map<String, Object> testCaseObj = testCaseTarget.request(MediaType.APPLICATION_JSON_TYPE).header("AuthorizationToken", accessToken).get(Map.class);
		        ObjectMapper mapper = new ObjectMapper();
				try {
					
					com.testproject.TestCasemvn testCase = mapper.readValue(mapper.writeValueAsString(testCaseObj.get("content")), com.testproject.TestCasemvn.class);
					final WebTarget testResultTarget = client.target("https://cmc-api-dev.appirio.net/v_beta/test-results");
			        
					//Builder builderTestResult = testResultTarget.request(MediaType.APPLICATION_JSON_TYPE).header("AuthorizationToken", accessToken);
					List<Object> valueMap = testCase.getRecords();
					Builder builderTestResult = testResultTarget.request(MediaType.APPLICATION_JSON_TYPE).header("AuthorizationToken", accessToken);
			        
					 for (Object tc : valueMap) {
				        	JSONObject obj = null;
							obj = new JSONObject(mapper.writeValueAsString(tc));
							String testCaseId = obj.getString("id");
							System.out.println("testCaseId="+testCaseId);
				        	//String type ="Functional";
     			    	    String status= resultStatus;
				    	    String browser = "Chrome";
				    	    String environment="cmcdev";
				    	    ImmutableMap<String, String> testResultArgs =
		 	        	       new ImmutableMap.Builder<String, String>()
			 	        	           .put("testCaseId", testCaseId)
			 	        	           .put("status",status)
			 	        	           //.put("testCaseId", testCaseId)
			 	        	           //.put("type", type)
				 	        	           .put("browser", browser)
				 	        	           //.put("browser", browser)
				 	        	           .put("environment", environment)
				 	        	           .build();
				    	    
			    	    System.out.println(testResultArgs.get("browser"));
					//LinkedHashMap<Object, Object> valueMap = (LinkedHashMap<Object, Object>)testCase.getRecords().get(0);
			        
			       //Map<Object, Object> valueMap = mapper.readValue(testCase.getRecords().get(0), LinkedHashMap.class);
			       // Map<String, String> map = builderTestResult.post(Entity.entity(testResultArgs, MediaType.APPLICATION_JSON_TYPE),Map.class);
			        //Map<String, String> map = builderTestResult.post(Entity.entity(ImmutableMap.of("testCaseId",valueMap.get("id").toString()), MediaType.APPLICATION_JSON_TYPE),Map.class);
				    	    //Map<String, String> map = builderTestResult.post(Entity.entity(testResultArgs, MediaType.APPLICATION_JSON_TYPE),Map.class);
				    	    Map<String, String> map=builderTestResult.post(Entity.entity(testResultArgs,MediaType.APPLICATION_JSON_TYPE), Map.class) ; 
				    	    //Map<String, String> map = builderTestResult.post(Entity.entity(ImmutableMap.of("testCaseId",valueMap.get("id").toString()), MediaType.APPLICATION_JSON_TYPE),Map.class);
				    	    System.out.println(map.get("content"));
					 }
			        //Get test steps under the testcase and create test results
					   // getTestSteps(valueMap.get("id").toString(),client,accessToken);
				} catch (JsonParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JsonMappingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JsonProcessingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        }
       }
	}

		
	}
		  