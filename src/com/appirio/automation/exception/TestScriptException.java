package com.appirio.automation.exception;

public class TestScriptException extends Exception {
	public static Object object;
	
	/**
	 * This is exception with message parameter.
	 * 
	 * @param message
	 *            - exception message.
	 */
	public TestScriptException(String message) {
		super(message);
	}

	/**
	 * This is exception with message and cause parameter.
	 * 
	 * @param message
	 *            - exception message.
	 * @param cause
	 *            - exception cause.
	 */
	public TestScriptException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public TestScriptException(String message, Object object){
		super(message);
		this.object = object;
	}
	
}
