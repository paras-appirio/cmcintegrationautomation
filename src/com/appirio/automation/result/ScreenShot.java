package com.appirio.automation.result;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.internal.WrapsDriver;

public class ScreenShot {
	public static String takeBrowserSnapShot(WebDriver wDriver, String path) {
		File saveAs;
		String fName = String.format("%s/Screenshot_%s.jpg", path,
				new SimpleDateFormat("yyyyMMddHHmmss").format(new Date(System.currentTimeMillis())));
		saveAs = new File(fName);
		try {

			File scrFile = ((TakesScreenshot) wDriver).getScreenshotAs(OutputType.FILE);
			// Now you can do whatever you need to do with it, for example copy
			// somewhere
			FileUtils.copyFile(scrFile, saveAs);
			//Thread.sleep(1000);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return saveAs.getName();

	}

	public static String takeElementSnapShot(WebDriver wDriver, WebElement wElement, String path) throws IOException, InterruptedException {
		File saveAs;
		String fName = String.format("%s/Screenshot_%s.jpg", path,
				new SimpleDateFormat("yyyyMMddHHmmss").format(new Date(System.currentTimeMillis())));
		saveAs = new File(fName);
		WrapsDriver wrapsDriver = (WrapsDriver) wElement;
		File screenshot = ((TakesScreenshot) wrapsDriver.getWrappedDriver()).getScreenshotAs(OutputType.FILE);
	
		Rectangle rectangle = new Rectangle(wElement.getSize().width, wElement.getSize().height);
		org.openqa.selenium.Point location = wElement.getLocation();
		BufferedImage bufferedImage = ImageIO.read(screenshot);
		BufferedImage destImage = bufferedImage.getSubimage(location.x, location.y, rectangle.width, rectangle.height);
		ImageIO.write(destImage, "png", screenshot);
	
		// Now you can do whatever you need to do with it, for example copy
		// somewhere
		try {

			FileUtils.copyFile(screenshot, saveAs);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return saveAs.getName();

	}

}
