package com.appirio.automation.configuration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class ObjectMetadata {

	// private String fieldName;
	// private String fieldType;
	private List<String[]> objList = new ArrayList<String[]>();

	public ObjectMetadata(String fileName) {
		getMetadata(fileName);
	}

	/*
	 * public String getFieldName() { return fieldName; } public void
	 * setFieldName(String fieldName) { this.fieldName = fieldName; } public
	 * String getFieldType() { return fieldType; } public void
	 * setFieldType(String fieldType) { this.fieldType = fieldType; }
	 */
	public List<String[]> getObjList() {
		return objList;
	}

	public void setObjList(List<String[]> objList) {
		this.objList = objList;
	}

	public void getMetadata(String fileName) {
		Workbook book = null;
		// List<objectDataDictionary> obList = new
		// ArrayList<objectDataDictionary>();
		try {
			book = Workbook.getWorkbook(new File(fileName));
		} catch (BiffException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Sheet sheet_summary = book.getSheet("Sheet1");
		int col = 1;
		do {
			try {
				String name = sheet_summary.getCell(col, 0).getContents();
				String type = sheet_summary.getCell(col, 1).getContents();
				objList.add(new String[] { name, type });
				col++;
			} catch (ArrayIndexOutOfBoundsException e) {
				break;
			}
		} while (true);
	}

	public void print() {
		for (String[] arr : objList) {
			System.out.println("Name: " + arr[0] + "\tType: " + arr[1]);
		}
	}

	public String getTypeFromName(String name) {
		for (String[] arr : objList) {
			if (arr[0].equals(name))
				return arr[1];
		}
		return null;
	}
}
