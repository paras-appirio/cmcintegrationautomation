package com.appirio.automation.configuration;

import java.util.ArrayList;
import java.util.List;

public class Record {
	private List<FieldDetail> fieldList;
	private String recType;
	private String recTypeValue;
	private boolean notRecordType;
	
	public List<FieldDetail> getFieldList() {
		return fieldList;
	}

	public void setFieldList(List<FieldDetail> fieldList) {
		this.fieldList = fieldList;
	}

	public String getRecType() {
		return recType;
	}

	public void setRecType(String recType) {
		this.recType = recType;
	}

	public String getRecTypeValue() {
		return recTypeValue;
	}

	public void setRecTypeValue(String recTypeValue) {
		this.recTypeValue = recTypeValue;
	}

	public void add(FieldDetail field) {
		this.fieldList.add(field);

	}
	
	public boolean isNotRecordType() {
		return notRecordType;
	}
	
	public void setNotRecordType(boolean notRecordType) {
		this.notRecordType = notRecordType;
	}	

	public Record() {
		fieldList = new ArrayList<FieldDetail>();
	}

}
