package com.appirio.automation.configuration;

public class FieldDetail {
	private String name;
	private String type;
	private String value;
    private boolean notName;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public boolean isNotName() {
		return notName;
	}
	
	public void setNotName(boolean notName) {
		this.notName = notName;
	}

}
