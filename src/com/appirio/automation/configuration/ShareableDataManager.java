/**
 * 
 */
package com.appirio.automation.configuration;

import java.util.HashMap;

public class ShareableDataManager {
	private static HashMap<String, String> dataMap = new HashMap<String, String>();
	
	public static void setDataMap(HashMap<String, String> cDataMap){
		ShareableDataManager.dataMap = cDataMap;
	}
	
	public static HashMap<String, String> getDataMap(){
		return ShareableDataManager.dataMap;		
	}
}
