package com.appirio.automation.configuration;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.appirio.automation.exception.TestScriptException;
import com.appirio.automation.utility.Utility;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class LocalObject {

	private String objectName;
	private String objectLocatorType;
	private String objectLocator;
	public String getObjectName() {
		return objectName;
	}

	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}

	public String getObjectLocatorType() {
		return objectLocatorType;
	}

	public void setObjectLocatorType(String objectLocatorType) {
		this.objectLocatorType = objectLocatorType;
	}

	public String getObjectLocator() throws TestScriptException {
		// return objectLocator;
		return Utility.replaceHashTag(objectLocator);
	}

	public void setObjectLocator(String objectLocator) {
		this.objectLocator = objectLocator;
	}

	public static Map<String, LocalObject> readData(String fileName) {
		Map<String, LocalObject> localObjectRepo = new HashMap<String, LocalObject>();
		Workbook book = null;
		try {
			book = Workbook.getWorkbook(new File(fileName));
		} catch (BiffException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Sheet sheet_summary = book.getSheet("Sheet1");
		int row = 1;
		do {
			try {
				LocalObject newRecord = new LocalObject();
				String objectName = sheet_summary.getCell(1, row).getContents().toUpperCase();
				newRecord.setObjectName(objectName);
				String objectLocatorType = sheet_summary.getCell(2, row).getContents();
				newRecord.setObjectLocatorType(objectLocatorType);
				String objectLocator = sheet_summary.getCell(3, row).getContents();
				newRecord.setObjectLocator(objectLocator);

				localObjectRepo.put(objectName, newRecord);
				row++;
			} catch (ArrayIndexOutOfBoundsException e) {
				break;
			}
		} while (true);
		return localObjectRepo;
	}

}
