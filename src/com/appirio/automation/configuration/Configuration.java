package com.appirio.automation.configuration;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;

import com.appirio.automation.exception.TestScriptException;
import com.appirio.automation.result.TestResult;
import com.appirio.automation.utility.Utility;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

/**
 * @author Appirio7969
 *
 */
public class Configuration {

	private Map<String, LocalObject> localObjectRepo;
	private HashMap<String, ObjectMetadata> metadataMap = new HashMap<String, ObjectMetadata>();
	private HashMap<Integer, TestStep> executedStepsMap = null;
	private HashMap<String, Method> mapMethods = new HashMap<>();
	private TestResult testResult = null;
	private HashMap<String, String> mapGlobalVariable = new HashMap<String, String>();
	private String driverPath;
	private String testResultFolder;
	private String testResultFolderPath;
	private String testCaseNumber;
	private String snapshotFolder;
	private String mainSnapshotFolderName;
	private String[] spreadSheetList = null;
	private String methodMappingFile = null;
	private Class<?> methodClass;
	private Logger logger;
	private String[] testStepInstance = null;
	private String pathJunction; 
	private String inputTestDataSheet;
	private static HashMap<String,String> testCaseLocalInputData;
	private static LinkedHashMap<String, TestCase> testCases;
	private boolean requirementReference;
	private String browser;
	private String defaultDownloadPath;
	private String defaultUploadPath;
	//Created by paras
	private List<String> testCaseNumbers;
	
	public Configuration(String prop, Object object, Logger mainLogger, TestResult testResult) throws TestScriptException, BiffException, IOException {
		setMethodClass(object.getClass());
		setLogger(mainLogger);
		//this.logger = mainLogger;
		configure(prop);	
		setTestResult(testResult);
	}

	// getter and setter for all private variables

	public String getPathJunction() {
		return pathJunction;
	}

	public void setPathJunction(String pathJunction) {
		this.pathJunction = pathJunction;
	}
	
	//created by paras
	public List<String> gettestCaseNumbers() {
		return testCaseNumbers;
	}

	public void settestCaseNumbers(List<String> testcasenumbers) {
		this.testCaseNumbers = testcasenumbers;
	}
	
	public Map<String, LocalObject> getLocalObjectRepo() {
		return localObjectRepo;
	}

	public void setLocalObjectRepo(HashMap<String, LocalObject> localObjectRepo) {
		this.localObjectRepo = localObjectRepo;
	}

	public HashMap<String, ObjectMetadata> getMetadataMap() {
		return metadataMap;
	}

	public void setMetadataMap(HashMap<String, ObjectMetadata> metadataMap) {
		this.metadataMap = metadataMap;
	}

	public HashMap<Integer, TestStep> getExecutedStepsMap() {
		return executedStepsMap;
	}

	public void setExecutedStepsMap(HashMap<Integer, TestStep> executedStepsMap) {
		this.executedStepsMap = executedStepsMap;
	}

	public HashMap<String, Method> getMethodsMap() {
		return mapMethods;
	}

	public void setMethodsMap(HashMap<String, Method> mapMethods) {
		this.mapMethods = mapMethods;
	}

	public void setMapGlobalVariable(String key, String value) {
		mapGlobalVariable.put(key, value);
	}

	public String getGlobalVariable(String variable) {
		return mapGlobalVariable.get(variable);
	}

	public TestResult getTestResult() {
		return testResult;
	}

	public void setTestResult(TestResult testResult) {
		this.testResult = testResult;
	}

	public String[] getSpreadSheetList() {
		return spreadSheetList;
	}

	public void setSpreadSheetList(String[] spreadSheetList) {
		this.spreadSheetList = spreadSheetList;
	}

	public String getMethodMappingFile() {
		return methodMappingFile;
	}

	public void setMethodMappingFile(String methodMappingFile) {
		this.methodMappingFile = methodMappingFile;
	}

	public String getDriverPath() {
		return driverPath;
	}

	public void setDriverPath(String driverPath) {
		this.driverPath = driverPath;
	}

	public String getTestResultFolder() {
		return testResultFolder;
	}

	public void setTestResultFolder(String testResultFolder) {
		this.testResultFolder = testResultFolder;
	}

	public String getTestResultFolderPath() {
		return testResultFolderPath;
	}

	public void setTestResultFolderPath(String testResultFolderPath) {
		this.testResultFolderPath = testResultFolderPath;
	}

	public String getTestCaseNumber() {
		return testCaseNumber;
	}

	public void setTestCaseNumber(String testCaseNumber) {
		this.testCaseNumber = testCaseNumber;
	}

	public String getSnapshotFolder() {
		return snapshotFolder;
	}

	public void setSnapshotFolder(String snapshotFolder) {
		this.snapshotFolder = snapshotFolder;
	}

	public String getMainSnapshotFolderName() {
		return mainSnapshotFolderName;
	}

	public void setMainSnapshotFolderName(String mainSnapshotFolderName) {
		this.mainSnapshotFolderName = mainSnapshotFolderName;
	}

	public String[] getTestStepInstance() {
		return testStepInstance;
	}

	public void setTestStepInstance(String[] testStepInstance) {
		this.testStepInstance = testStepInstance;
	}

	public Class<?> getMethodClass() {
		return methodClass;
	}

	public void setMethodClass(Class<?> methodClass) {
		this.methodClass = methodClass;
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public String getInputTestDataSheet() {
		return inputTestDataSheet;
	}

	public void setInputTestDataSheet(String inputTestDataSheet) {
		this.inputTestDataSheet = inputTestDataSheet;
	}

	public String getDefaultDownloadPath() {
		return defaultDownloadPath;
	}

	public void setDefaultDownloadPath(String defaultDownloadPath) {
		this.defaultDownloadPath = defaultDownloadPath;
	}

	public static HashMap<String, String> getTestCaseLocalInputData() {
		return testCaseLocalInputData;
	}

	//Method to load local test data into the memory
	public static void setTestCaseLocalInputData(String filename) throws BiffException, IOException, TestScriptException {
		testCaseLocalInputData = new HashMap<String,String>();
		try{
			File DataFile=new File(filename);
			Workbook workbook= Workbook.getWorkbook(DataFile);
			if(workbook.getNumberOfSheets()==2){
			Sheet worksheet= workbook.getSheet(1);
			
			int no_col=worksheet.getColumns();
			for(int j=0;j<no_col;j++)				
			{
				testCaseLocalInputData.put(worksheet.getCell(j,0).getContents().trim(), worksheet.getCell(j,1).getContents().trim());
			}
		  }
		} catch (FileNotFoundException e) {
			throw new TestScriptException(filename+" could not be found.");
		} catch (IOException e) {
			throw new TestScriptException(e.getMessage(), e);
		} catch(Exception e){
			throw new TestScriptException("file " + filename + " cannot be read properly because of the exception "+ e.getMessage(), e);
		}	
	}
	
	
	public static LinkedHashMap<String, TestCase> getTestCases() {
		return testCases;
	}

	public static void setTestCases(LinkedHashMap<String, TestCase> testCases) {
		Configuration.testCases = testCases;
	}

	public String getBrowser() {
		return browser;
	}

	public void setBrowser(String browser) {
		this.browser = browser;
	}	
	
	public boolean isRequirementReference() {
		return requirementReference;
	}

	public void setRequirementReference(boolean requirementReference) {
		this.requirementReference = requirementReference;
	}

	public String getDefaultUploadPath() {
		return defaultUploadPath;
	}

	public void setDefaultUploadPath(String defaultUploadPath) {
		this.defaultUploadPath = defaultUploadPath;
	}

	// Method to load Method Mapping in memory
	private void loadMapRules() throws TestScriptException {
		Properties props = new Properties();
		if (methodMappingFile == null) {
			throw new TestScriptException("MethodMappingFile is null , please check.");
		}
		FileInputStream inputStream = null;
		try {
			inputStream = new FileInputStream(methodMappingFile);

			props.load(inputStream);
			Enumeration<?> en = props.propertyNames();
			while (en.hasMoreElements()) {
				String key = (String) en.nextElement();
				String methodInfo = props.getProperty(key);
				Method method = null;
				Pattern pattern = Pattern.compile("(.*)\\((.*)\\)");
				Matcher matcher = pattern.matcher(methodInfo);
				if (matcher.find()) {
					// declare
					String methodName = matcher.group(1);
					String argsType[] = null;
					Class<?>[] argsList = null;
					if (matcher.group(2).trim().length() != 0) {
						argsType = matcher.group(2).trim().split(",");
						argsList = new Class[argsType.length];
						for (int i = 0; i < argsType.length; i++) {
							try {
								argsList[i] = Class.forName("java.lang." + argsType[i].trim());
							} catch (ClassNotFoundException e) {
								throw new TestScriptException(argsType[i] + " is not a correct java class type!");

							}
						}
					}
					try {
						method = getCommandFromReference(methodClass, methodName, argsList);
					} catch (NoSuchMethodException e) {
						throw new TestScriptException("No such method " + methodInfo, e);
					} catch (SecurityException e) {
						throw new TestScriptException(e.getMessage(), e);
					} catch(Exception e){
						throw new TestScriptException(e.getMessage(), e);
					}
				} else {
					throw new TestScriptException(
							"the value for key :" + key + '=' + methodInfo + " is not a function decare ");
				}
				// add method to hash

				mapMethods.put(key.toUpperCase(), method);
			}
		} catch (FileNotFoundException e) {
			throw new TestScriptException(mapMethods + " could not be found.");
		} catch (IOException e) {
			throw new TestScriptException(e.getMessage(), e);
		} catch (IllegalArgumentException e) {
			throw new TestScriptException(
					"method mapping file is Illegal. methodMappingFile= " + methodMappingFile + e.getMessage(), e);
		} catch(Exception e){
			throw new TestScriptException(
					"method mapping file cannot be read properly because of the exception "+ e.getMessage(), e);
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					throw new TestScriptException(e.getMessage(), e);
				}
			}
		}
	}

	// Method to load Metadata for the objects in the memory
	private void loadMetaData() {
		String folderName = "resources";
		File folder = new File(folderName);
		for (final File fileEntry : folder.listFiles()) {
			if (!fileEntry.isDirectory() && fileEntry.getName().startsWith("MetaData_")) {
				String fileName = fileEntry.getName();
				StringTokenizer st = new StringTokenizer(fileName, ".");
				if (st.hasMoreElements()) {
					String objectName = (String) st.nextElement();
					objectName = objectName.substring("Metadata_".length());
					ObjectMetadata ob = new ObjectMetadata(folderName + getPathJunction() + fileName); // Gaurang
					metadataMap.put(objectName, ob);
				}
			}
		}
	}

	private void configure(String configureFile) throws TestScriptException, BiffException, IOException {
				
		ConsoleAppender console = new ConsoleAppender(); // create appender
		// configure the appender
		String PATTERN = "%d [%p] %m%n";
		console.setLayout(new PatternLayout(PATTERN));
		console.setThreshold(Level.INFO);
		console.activateOptions();
		// add appender to any Logger (here is root)
		Logger.getRootLogger().addAppender(console);

		// SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd_HHmmss");
		String debugLog = String.format("%s/debug_%s.log", "Logs",
		new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss").format(new Date(System.currentTimeMillis())));
		FileAppender faD = new FileAppender();
		faD.setName("FileLogger");
		faD.setFile(debugLog);
		// faD.setFile(String.format("jarvis_debug.log", testResultDir));
		faD.setLayout(new PatternLayout("%d %-5p [%c{1}] %m%n"));
		faD.setThreshold(Level.DEBUG);
		faD.setAppend(true);
		faD.activateOptions();

		// add appender to any Logger (here is root)
		Logger.getRootLogger().addAppender(faD);
		String infoLog = String.format("%s/info_%s.log", "Logs",
				new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss").format(new Date(System.currentTimeMillis())));
		FileAppender faI = new FileAppender();
		faI.setName("FileLogger");
		faI.setFile(infoLog);
		// faI.setFile(String.format("jarvis_info.log", testResultDir));
		faI.setLayout(new PatternLayout("%d %-5p [%c{1}] %m%n"));
		faI.setThreshold(Level.INFO);
		faI.setAppend(true);
		faI.activateOptions();

		// add appender to any Logger (here is root)
		Logger.getRootLogger().addAppender(faI);
		// deleting output folder
		try {
			// File file = new File();
			FileUtils.deleteDirectory(new File("output"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		getLogger().info("Starting the configuration");
		Properties props = new Properties();
		setPathJunction("//");
		FileInputStream fileInputStream = null;

		try {

			fileInputStream = new FileInputStream(configureFile);
			props.load(fileInputStream);
		} catch (IOException e1) {
			if (fileInputStream != null) {
				try {
					fileInputStream.close();
				} catch (IOException e) {
					throw new TestScriptException(
							"Close fileInputStream  failed. please check configure file :%s" + configureFile, e);
				}
			}
		}
		getLogger().info("Started fetching the Test Case numbers to be executed");
		String allSpreadSheet = props.getProperty("spreadSheetList");
		String testcasestobeExecuted = props.getProperty("TestCaseList");
		if (allSpreadSheet == null || allSpreadSheet.trim().length() == 0) {
			throw new TestScriptException(
					"configure item spreadSheetList does not exists or is not set. please check .");
		}
		loadTestCases(allSpreadSheet,testcasestobeExecuted);
		if(getTestCases().size()==0){
			throw new TestScriptException(
					"There are no test cases set for execution. Please check and update TestCases.xls from resources.");
		}		
		getLogger().info("Completed fetching the Test Case numbers to be executed");
		
		getLogger().info("Started fetching the Browser name for the test execution.");
		String browser = props.getProperty("browser");
		if (browser == null || browser.trim().length() == 0) {
			throw new TestScriptException(
					"configure item allBrowsers does not exists or is not set, please check.");
		}
		setBrowser(browser);
		getLogger().info("Completed fetching the Browser name for the test execution.");		
		
		getLogger().info("Started fetching the Global Input Test Data sheet name");
		String inputTestDataSheet = props.getProperty("inputTestData");
		if (inputTestDataSheet == null || inputTestDataSheet.trim().length() == 0) {
			throw new TestScriptException(
					"configure item inputTestDataSheet does not exists or is not set. please check .");
		}
		setInputTestDataSheet(inputTestDataSheet);
		loadInputTestData();
		getLogger().info("Completed fetching the Global Input Test Data.");

		getLogger().info("Started fetching the Global Input Test Data sheet name");
		
		if (props.getProperty("methodMappingFile") == null || props.getProperty("methodMappingFile").trim().length() == 0) {
			throw new TestScriptException("configure item methodMappingFile is not exists or not set . please check.");
		}
		setMethodMappingFile(props.getProperty("methodMappingFile"));
		getLogger().info("Started fetching the Method Mapping");
		loadMapRules();
		getLogger().info("Completed writing the Method Mapping.");
		
		String localRepoFile = props.getProperty("localRepoFile");
		if (localRepoFile == null || localRepoFile.trim().length() == 0) {
			throw new TestScriptException("configure item localRepoFile is not exists or not set. please check.");
		}
		
		getLogger().info("Started fetching the Local Repository.");
		localObjectRepo = LocalObject.readData(localRepoFile);
		getLogger().info("Completed fetching the Local Repository.");
		
		getLogger().info("Started fetching the MetaData.");
		loadMetaData();
		getLogger().info("Completed fetching the MetaData.");
		
		if(props.getProperty("mainSnapshotFolderName") == null || (props.getProperty("mainSnapshotFolderName")).trim().length() ==0){
			throw new TestScriptException(
					"configure item  mainSnapshotFolderName not exists or not set. Please check.");
		}
		else{
			setMainSnapshotFolderName(props.getProperty("mainSnapshotFolderName"));
		}
		
		String testResultDir = props.getProperty("testResultDir");
		if (testResultDir == null || testResultDir.trim().length() == 0) {
			throw new TestScriptException(
					"configure item log4jConfigureFilePath is not exists or not set. please check.");
		} else {
			
			getLogger().info("Started creating the Test Result folders and files.");
			testResultFolder = String.format("TestResult_%s",
			new SimpleDateFormat("yyyyMMddHHmmss").format(new Date(System.currentTimeMillis())));
			testResultFolderPath = testResultDir + getPathJunction() + testResultFolder; // Gaurang
			snapshotFolder = testResultFolderPath + getPathJunction() + getMainSnapshotFolderName(); // Gaurang

			try {
				Path path = Paths.get(testResultFolderPath);
				Files.createDirectory(path);

				path = Paths.get(snapshotFolder);
				Files.createDirectory(path);
				
				getLogger().info("Completed creation of the Test Result folders and files.");

			} catch (Exception e) {
				logger.error("Test Result folder cannot be created because of " + e.getMessage());
				throw new TestScriptException("Test Result folder cannot be created");
			}
		}

		setDriverPath(props.getProperty("driverPath"));
		if (driverPath == null) {
			throw new TestScriptException(
					"Error: configure item driverPath does not exist or is not set . please check. ");
		}
		
		getLogger().info("Started fetching the consent for Requirement Reference.");
		if(props.getProperty("generateRequirementReference") == null || (props.getProperty("generateRequirementReference")).trim().length() ==0){
			throw new TestScriptException(
					"configure item  generateRequirementReference not exists or not set. Please check.");
		}		
		setRequirementReference(Boolean.parseBoolean(props.getProperty("generateRequirementReference")));
		getLogger().info("Completed fetching the consent for Requirement Reference.");
		
		
		getLogger().info("Started fetching the default download Path.");
		if(props.getProperty("defaultDownloadPath") == null || (props.getProperty("defaultDownloadPath")).trim().length() ==0){
			throw new TestScriptException(
					"configure item  defaultDownloadPath not exists or not set. Please check.");
		}		
		setDefaultDownloadPath(props.getProperty("defaultDownloadPath"));
		getLogger().info("Completed fetching the the default download Path.");
		setMapGlobalVariable("defaultDownloadPath", props.getProperty("defaultDownloadPath"));	
		
		
		getLogger().info("Started fetching the default upload Path.");
		if(props.getProperty("defaultUploadPath") == null || (props.getProperty("defaultUploadPath")).trim().length() ==0){
			throw new TestScriptException(
					"configure item  defaultUploadPath not exists or not set. Please check.");
		}		
		setDefaultDownloadPath(props.getProperty("defaultUploadPath"));
		getLogger().info("Completed fetching the the default download Path.");
		setMapGlobalVariable("defaultUploadPath", props.getProperty("defaultUploadPath"));
		
		
	}

	private void loadInputTestData() throws BiffException, IOException, TestScriptException {
		HashMap<String,String> mapTestData=new HashMap<String,String>();
		File DataFile=new File(getInputTestDataSheet());
		Workbook workbook= Workbook.getWorkbook(DataFile);
		Sheet worksheet= workbook.getSheet(0);
		
		int no_col=worksheet.getColumns();
			
		for(int j=0;j<no_col;j++)
			
		{
			//System.out.println("Variable is "+worksheet.getCell(j,0).getContents().trim() + " and Value is "+worksheet.getCell(j,1).getContents().trim());
			//logger.info("Variable is "+worksheet.getCell(j,0).getContents().trim() + " and Value is "+worksheet.getCell(j,1).getContents().trim());
	        mapTestData.put(worksheet.getCell(j,0).getContents().trim(), Utility.replaceHashTag(worksheet.getCell(j,1).getContents().trim()));
		}
			ShareableDataManager.setDataMap(mapTestData);
		}
			

	private Method getCommandFromReference(Class<?> clazz, String methodName, Class<?>[] arglist)
			throws NoSuchMethodException {
		Method method = null;
		try {
			method = clazz.getDeclaredMethod(methodName, arglist);		
		} catch (NoSuchMethodException e) {
			if (clazz.getSuperclass() != null && !clazz.getSuperclass().isInstance(new Object()))
				return getCommandFromReference(clazz.getSuperclass(), methodName, arglist);
			else
				throw e;
		} catch (Exception e){
			e.getMessage();
			e.printStackTrace();
		} catch(Error e){
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return method;
	}
	
	@SuppressWarnings("null")
	private void loadTestCases(String fileName ,String testcaseslist){
		System.out.println("testcaseslist in Starting="+testcaseslist);
		String[] testcaseNoarg= testcaseslist.split(",");
		List<String> testCaseNo = new ArrayList<String>();
		for(String testcasesnumbers: testcaseNoarg){
		testCaseNo.add(testcasesnumbers);
		}
		settestCaseNumbers(testCaseNo);
		System.out.println("testcaseslist in after getter="+getTestCaseNumber());
		setTestCases(TestCase.readData(fileName ,testcaseslist));
		Set<String> set = getTestCases().keySet();
		List<String> list = new ArrayList<String>();
		for(String testCaseNumber:set){
			list.add(getTestCases().get(testCaseNumber).getTestCasePath());
		}
		spreadSheetList = list.toArray(new String[list.size()]);		
	}

}
