package com.appirio.automation.configuration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class TestStep {

	private int tStepNumber = 0; // Added to repeat range of test stepprivate
									// String command=null;
	private List<String> argumentList;
	private String command = null;
	private String retunValues;
	private String result = null;
	private String toBeAborted = "Y";
	private static int command_col = 1;
	private Object[] args = null;
	private static Map<String, Integer> mapGroupNameTestRow = new TreeMap<String, Integer>(
			String.CASE_INSENSITIVE_ORDER);
	private static int idxGroupName = 0;
	private static int abortColumn = 6;
	private String reqReference = "";
	private static int reqReferenceColumn = 7;

	public void setReqReference(String reqReference) {
		this.reqReference = reqReference;
	}

	public String getReqReference() {
		return reqReference;
	}

	public void setToBeAborted(String toBeAborted) {
		this.toBeAborted = toBeAborted;
	}

	public String getToBeAborted() {
		return toBeAborted;
	}

	public void setArguments(Object[] args) {
		this.args = args;
	}

	public Object[] getArguments() {
		return args;
	}

	public void setTestStepNumber(int number) {
		this.tStepNumber = number;
	}

	public int getTestStepNumber() {
		return this.tStepNumber;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public List<String> getArgumentList() {
		return argumentList;
	}

	public void setArgumentList(List<String> argumentList) {
		this.argumentList = argumentList;
	}

	public String getRetunValues() {
		return retunValues;
	}

	public void setRetunValues(String retunValues) {
		this.retunValues = retunValues;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public static HashMap<Integer, TestStep> readData(String fileName, String sheetName) {
		int stepNumber = 1;
		// List<TestStep> testCase = new ArrayList<TestStep>();
		HashMap<Integer, TestStep> mapTestSteps = new HashMap<Integer, TestStep>();
		Workbook book = null;
		String groupName = "";
		try {
			book = Workbook.getWorkbook(new File(fileName));
		} catch (BiffException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Sheet sheet_summary = book.getSheet(sheetName);
		int row = 1;
		List<String> argumentList = new ArrayList<String>();

		do {
			try {
				TestStep newRecord = new TestStep();
				Cell commandCell = sheet_summary.getCell(command_col, row);
				Cell groupNameCell = sheet_summary.getCell(idxGroupName, row);
				String command = "";
				// continue with next iteration either cell instance or command
				// is null(empty in case of command).
				if (commandCell != null) {
					command = commandCell.getContents();
					if (command == "" || command.equals(null)) {
						row++;
						continue;
					}
				} else {
					row++;
					continue;
				}
				newRecord.setCommand(command);
				newRecord.setTestStepNumber(stepNumber);

				argumentList = new ArrayList<String>();
				int argCol = command_col + 1;
				do {
					try {
						if (!sheet_summary.getCell(argCol, row).getContents().equals("")) {
							argumentList.add(sheet_summary.getCell(argCol, row).getContents());
							argCol++;
						} else if (sheet_summary.getCell(argCol, row).getContents().equals("")) {
							break;
						}
					} catch (ArrayIndexOutOfBoundsException e) {
						break;
					}
				} while (true);

				newRecord.setArgumentList(argumentList);

				// It is used for getting user preference on whether the
				// execution should be aborted or not for command failure.
				String abortedStatus = sheet_summary.getCell(abortColumn, row).getContents();

				if (!abortedStatus.equalsIgnoreCase("") || !abortedStatus.equals(null)) {
					newRecord.setToBeAborted(abortedStatus);
				}

				// It is used for getting SRS reference in Test Results
				String SRSID = sheet_summary.getCell(reqReferenceColumn, row).getContents();

				if (!SRSID.equalsIgnoreCase("") || !SRSID.equals(null)) {
					newRecord.setReqReference(SRSID);
				}

				mapTestSteps.put(row, newRecord);
				if (groupNameCell != null && !groupNameCell.getContents().equals("")
						&& !groupName.equalsIgnoreCase(groupNameCell.getContents())
						&& !mapGroupNameTestRow.containsKey(groupNameCell.getContents())) {
					mapGroupNameTestRow.put(groupNameCell.getContents(), row);
				}
				// testCase.add(newRecord);
				row++;
				stepNumber++;
			} catch (ArrayIndexOutOfBoundsException e) {
				break;
			}
		} while (true);
		return mapTestSteps;
	}

	public void printArgList() {
		for (String arg : argumentList) {
			System.out.print(arg + "\t");
		}
	}

	public void printCommandWithArgList() {
		String printString = this.command + "\t";
		for (String arg : argumentList) {
			printString += arg + "\t";
		}
		System.out.println(printString);
	}
}
