package com.appirio.automation.configuration;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;

import org.apache.poi.ss.usermodel.Cell;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class TestCase {

	private String testCaseNumber;
	private String testCaseName;
	private String testCasePath;
	private String toBeExecuted;
	
	public String getTestCaseNumber() {
		return testCaseNumber;
	}
	public void setTestCaseNumber(String testCaseNumber) {
		this.testCaseNumber = testCaseNumber;
	}
	public String getTestCaseName() {
		return testCaseName;
	}
	public void setTestCaseName(String testCaseName) {
		this.testCaseName = testCaseName;
	}
	public String getTestCasePath() {
		return testCasePath;
	}
	public void setTestCasePath(String testCasePath) {
		this.testCasePath = testCasePath;
	}
	public String getToBeExecuted() {
		return toBeExecuted;
	}
	public void setToBeExecuted(String toBeExecuted) {
		this.toBeExecuted = toBeExecuted;
	}
	
	public static LinkedHashMap<String, TestCase> readData(String fileName, String testcasesList) {
		if(testcasesList.equals(null)|| testcasesList==null || testcasesList==""){
			System.out.println("Test case list is blank");
			return null;
		}
		
		else{
		String[] testcaseslistarg=testcasesList.split(",");
		LinkedHashMap<String, TestCase> testCases = new LinkedHashMap<String, TestCase>();
		Workbook book = null;
		try {
			book = Workbook.getWorkbook(new File(fileName));
		} catch (BiffException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Sheet sheet_summary = book.getSheet(0);
		int row = 1;
		
		do {
			try {
				for (String testcases : testcaseslistarg){
				String toBeExecuted = sheet_summary.getCell(3, row).getContents();
				if(sheet_summary.getCell(0, row).getContents().equalsIgnoreCase(testcases)){	
					TestCase newRecord = new TestCase();
					String testCaseNumber = sheet_summary.getCell(0, row).getContents();
					newRecord.setTestCaseNumber(testCaseNumber);
					String testCaseName = sheet_summary.getCell(1, row).getContents();
					newRecord.setTestCaseName(testCaseName);
					String testCasePath = sheet_summary.getCell(2, row).getContents();
					newRecord.setTestCasePath(testCasePath);
			        testCases.put(testCaseNumber, newRecord);
					continue;
				}
				}
			} catch (ArrayIndexOutOfBoundsException e) {
				break;
			}
		  row++;
		} while (true);
		return testCases;
		}
	}
	
	public static void setTestCasestoYes(String fileName,String testcasesList) {
		String[] testcaseslistarg=testcasesList.split(",");
		LinkedHashMap<String, TestCase> testCases = new LinkedHashMap<String, TestCase>();
		Workbook book = null;
		try {
			book = Workbook.getWorkbook(new File(fileName));
		} catch (BiffException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Sheet sheet_summary = book.getSheet(0);
		int row = 1;
		do {
			try {
				for (String testcases : testcaseslistarg) {
					if(sheet_summary.getCell(0, row).getContents().equalsIgnoreCase(testcases)){
						
					}
					
				}
				String toBeExecuted = sheet_summary.getCell(3, row).getContents();
				if(toBeExecuted.equalsIgnoreCase("Y")){	
					TestCase newRecord = new TestCase();
					String testCaseNumber = sheet_summary.getCell(0, row).getContents();
					newRecord.setTestCaseNumber(testCaseNumber);
					String testCaseName = sheet_summary.getCell(1, row).getContents();
					newRecord.setTestCaseName(testCaseName);
					String testCasePath = sheet_summary.getCell(2, row).getContents();
					newRecord.setTestCasePath(testCasePath);
					
					testCases.put(testCaseNumber, newRecord);
				}
			} catch (ArrayIndexOutOfBoundsException e) {
				break;
			}
		  row++;
		} while (true);
		
	}
}
