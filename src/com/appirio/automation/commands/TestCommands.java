package com.appirio.automation.commands;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileFilter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.appirio.automation.configuration.Configuration;
import com.appirio.automation.configuration.LocalObject;
import com.appirio.automation.exception.TestScriptException;
import com.appirio.automation.result.ScreenShot;
import com.appirio.automation.utility.Utility;
import com.google.common.base.Function;

import bsh.EvalError;
import bsh.Interpreter;

import java.net.URL;

/**
 * This class is used to collate all the generic commands that can be utilized
 * for any application.
 *
 */
public class TestCommands {

	private static Configuration config;
	protected int waitTime = 180;
	protected int sheetNo = 0;
	protected String[] testStep;
	protected int nextCommandRow = -1;
	protected String methodFailureMessage = null;
	protected WebDriver driver;
	protected static List<WebDriver> driverList = new ArrayList<WebDriver>();
	int timeOutCounter;
	public int TIMEOUT = 10;

	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	public Configuration getTestCommandsConfig() {
		return config;
	}

	public static void setTestCommandsConfig(Configuration config) {
		TestCommands.config = config;
	}

	public String[] getStepInstance() {
		if (config.isRequirementReference()) {
			return new String[6];
		} else {
			return new String[5];
		}
	}

	public int getNextTestCommandRow() {
		return nextCommandRow;
	}

	public void resetNextTestCommandRow() {
		nextCommandRow = -1;
	}

	/**
	 * This method press the specified key for the mentioned element on the page
	 * 
	 * @param element
	 *            Element on which we need to sent the Keys
	 * @param key
	 *            Key that needs to be pressed
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 * @throws TestScriptException
	 */
	public Object keyPress(String element, String key) throws TestScriptException {

		testStep = getStepInstance();
		testStep[0] = "Verify that " + key + "is pressed";
		testStep[1] = key + "should be pressed";
		testStep[2] = key + "is pressed";
		testStep[4] = "";
		String status = "Pass";

		try {
			switch (key.toLowerCase()) {
			case "new tab":
				findElement(getLocator(element)).sendKeys(Keys.CONTROL + "t");
				break;
			case "previous tab":
				findElement(getLocator(element)).sendKeys(Keys.CONTROL + "" + Keys.SHIFT + "" + Keys.TAB);
				break;
			case "next tab":
				findElement(getLocator(element)).sendKeys(Keys.CONTROL + "" + Keys.TAB);
				break;
			case "close tab":
				findElement(getLocator(element)).sendKeys(Keys.CONTROL + "" + "w");
				break;
			case "reopen recently closed tab":
				findElement(getLocator(element)).sendKeys(Keys.CONTROL + "" + Keys.SHIFT + "" + "t");
				break;
			case "enter":
				findElement(getLocator(element)).sendKeys(Keys.RETURN);
				break;
			case "tab":
				findElement(getLocator(element)).sendKeys(Keys.TAB);
				break;
			default:
				findElement(getLocator(element)).sendKeys("Keys" + "." + key);
				break;
			}

		} catch (Exception e) {
			testStep[2] = "Unable to press " + key + " due to an Exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			methodFailureMessage = testStep[2] + " " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
		} finally {
			testStep[3] = status;
			if (status.equals("Fail")) {
				throw new TestScriptException(methodFailureMessage, testStep);
			}
		}
		return true;

	}

	/**
	 * This method is used to evaluate the specified condition and depending on
	 * the result navigate to the mentioned Step or next step in sequence.
	 * 
	 * @param condition
	 *            Expression to be evaluated
	 * @param goTo
	 *            Step Next to be navigated in case result is true
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 * @throws TestScriptException
	 */
	public Object caseCheck(String condition, String goTo) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that condition is checked and execution is navigated to the mentioned step";
		testStep[1] = "Condition should be checked and execution should be navigated to the mentioned step";
		testStep[2] = "Condition is checked and execution is navigated to the mentioned step";
		testStep[4] = "";
		String status = "Pass";

		try {
			Interpreter commandInterpreter = new Interpreter();
			boolean result = (Boolean) commandInterpreter.eval(condition + "? true:false;");
			if (result) {
				if (goTo.toLowerCase().trim().startsWith("gotostep")) {
					int step = Integer.parseInt(goTo.substring(goTo.indexOf("(") + 1, goTo.indexOf(")")));
					goToStep(step);
				} else if (goTo.toLowerCase().trim().startsWith("gotogroup")) {
					String groupName = goTo.substring(goTo.indexOf("(") + 1, goTo.indexOf(")"));
					goToGroup(groupName);
				}
			}
		} catch (EvalError e) {
			testStep[2] = "Unable to parse specified condition. Please check the condition.";
			status = "Fail";
			methodFailureMessage = " Error: caseCheck(String,String) method - unable to parse sepcified condition. Please check the condition.";
			config.getLogger().error(methodFailureMessage);
		} catch (Exception e) {
			testStep[2] = "Case checking cannot be done because of an Exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			methodFailureMessage = testStep[2] + " " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			throw new TestScriptException(methodFailureMessage, testStep);
		} else {
			return true;
		}
	}

	
	private void goToGroup(String groupName) {
		// TODO Auto-generated method stub

	}

	/**
	 * This method is used to save the current url in the browser.
	 * 
	 * @param variable
	 *            Variable in which we need to save the url.
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 * @throws TestScriptException
	 */
	public Object saveCurrentURL(String variable) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that the current url is saved";
		testStep[1] = "Current url should be saved";
		testStep[2] = "Current url is saved";
		String status = "Pass";
		try {
			String value = driver.getCurrentUrl();
			config.setMapGlobalVariable(variable, value);
			// System.out.println("current url is "+value);
		} catch (Exception e) {
			testStep[2] = "Current url cannot be saved because of Exception. Please refer Logs/info.log for more details.";
			status = "fail";
			methodFailureMessage = e.getMessage();
			config.getLogger().error(methodFailureMessage);
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			throw new TestScriptException(methodFailureMessage, testStep);
		} else {
			return true;
		}

	}

	/**
	 * This method moves the execution to the mentioned step number.
	 * 
	 * @param stepNumber
	 *            Step to be navigated to.
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 * @throws TestScriptException
	 */
	public Object goToStep(Integer stepNumber) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that execution should go to step : " + stepNumber;
		testStep[1] = "Execution should go to step : " + stepNumber;
		testStep[2] = "Gone to step : " + stepNumber;
		testStep[4] = "";
		String status = "Pass";

		try {
			nextCommandRow = stepNumber - 1;
		} catch (Exception e) {
			testStep[2] = "Unable to go on step : " + stepNumber + "because of an Exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			methodFailureMessage = testStep[2] + " " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			throw new TestScriptException(methodFailureMessage, testStep);
		} else {
			return true;
		}
	}

	/**
	 * Waits for 1 second.Timeouts if it has already waited TIMEOUT sec. in the
	 * calling function, and fails with @timeoutMessage
	 * 
	 * @param timeoutMessage
	 *            Message to be shown after timeout
	 * @throws TimeoutException
	 * @throws InterruptedException
	 * @throws TestScriptException
	 */
	protected void mWait(String timeoutMessage) throws TimeoutException, InterruptedException, TestScriptException {
		try {
			if (timeOutCounter > TIMEOUT) {
				timeOutCounter = 0;
				throw new TimeoutException(timeoutMessage);
			}
			Thread.sleep(1000);
			timeOutCounter++;
		} catch (Exception e) {
			config.getLogger().error(timeoutMessage);
			throw new TestScriptException(timeoutMessage);
		}
	}

	/**
	 * Finds element based on specified By locator
	 * 
	 * @param locator
	 *            By selector to locate element on webpage
	 * @return WebElement 
	 * 			  WebElement which is identified via provided xpath
	 * @throws TestScriptException
	 */
	protected WebElement findElement(By locator) throws TestScriptException {
		try {
			waitForElementToBeAvailable(locator);
		} catch (Exception e) {
			methodFailureMessage = "FindElement method failed because of exception " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage);
		}
		return driver.findElement(locator);
	}

	/**
	 * Finds all elements based on By selector
	 * 
	 * @param locator
	 *            By selector to locate element on webpage
	 * @return List<WebElement>
	 * 			  List of WebElements which are identified via provided xpath
	 * @throws TestScriptException
	 */
	protected List<WebElement> findElements(By locator) throws TestScriptException {
		List<WebElement> elements = null;
		try {
			elements = driver.findElements(locator);
		} catch (Exception e) {
			methodFailureMessage = "FindElements method failed because of exception " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage);
		}
		return elements;
	}

	/**
	 * Finds all child elements of a particular element
	 * 
	 * @param byChild
	 *            By selector specifying the child element to be found
	 * @param byParent
	 *            By selector specifying the element under which the child
	 *            element is to be found
	 * @return List<WebElement>
	 * 			  List of child elements
	 * @throws TestScriptException
	 */
	protected List<WebElement> findChildElements(By byChild, By byParent) throws TestScriptException {
		List<WebElement> elements = null;
		try {
			WebElement ParentElement = findElement(byParent);
			elements = ParentElement.findElements(byChild);
		} catch (Exception e) {
			methodFailureMessage = "FindChildElements method failed because of exception " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage);
		}
		return elements;
	}

	/**
	 * Returns By object based on name using local object repository. Searches
	 * the local repository for the name and create By object on the basis on
	 * information found(type and locator) Returns null if the name is not found
	 * in object repository
	 * 
	 * @param name
	 *            Name of the element in the local object repository
	 * @return By selector 
	 * 			  Returns the By selector using the Local repository
	 * @throws TestScriptException
	 */
	protected By getLocator(String name) throws TestScriptException {
		name = name.trim();
		By byLocator = null;
		try {
			LocalObject object = config.getLocalObjectRepo().get(name.toUpperCase());

			String locatorType = object.getObjectLocatorType();
			String locator = object.getObjectLocator();
			if (locatorType.equalsIgnoreCase("id"))
				byLocator = By.id(locator);
			else if (locatorType.equalsIgnoreCase("name"))
				byLocator = By.name(locator);
			else if (locatorType.equalsIgnoreCase("xpath"))
				byLocator = By.xpath(locator);
			else if (locatorType.equalsIgnoreCase("linktext"))
				byLocator = By.linkText(locator);
			else if (locatorType.equalsIgnoreCase("partiallinktext"))
				byLocator = By.partialLinkText(locator);
			else if (locatorType.equalsIgnoreCase("tagname"))
				byLocator = By.tagName(locator);
			else if (locatorType.equalsIgnoreCase("cssselector"))
				byLocator = By.cssSelector(locator);
			else if (locatorType.equalsIgnoreCase("classname"))
				byLocator = By.className(locator);
		} catch (Exception e) {
			methodFailureMessage = "GetLocator method failed because of exception " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage);
		}
		return byLocator;
	}

	/**
	 * Launches the specified browser
	 * 
	 * @param browserType
	 *            Browser name
	 * @return String[] 
	 * 			  Returns the result of the execution
	 * @throws TestScriptException
	 */
	  public String[] launchBrowser() throws TestScriptException{
		testStep = getStepInstance();
		String status = "Pass";
		String browserType = null;
		try {			
			browserType = config.getBrowser();
			testStep[0] = "Launch " + browserType;
			testStep[1] = browserType + " should be launched";
			testStep[2] = browserType + " is launched";
			testStep[4] = "";

			String standardBrowserType = browserType.trim().toUpperCase();

			if (standardBrowserType.equalsIgnoreCase("FIREFOX")) {
				String webdriver_firefox_driver = config.getDriverPath() + "\\geckodriver.exe";
				if (webdriver_firefox_driver.trim().length() == 0)
					throw new TestScriptException("Error: Driver path has not specified.");

				System.setProperty("webdriver.gecko.driver", webdriver_firefox_driver);
				FirefoxProfile firefoxProfile = new FirefoxProfile();
				firefoxProfile.setPreference("browser.privatebrowsing.autostart", true);
				driver = new FirefoxDriver(firefoxProfile);

			} else if (standardBrowserType.equalsIgnoreCase("CHROME")) {
				String webdriver_chrome_driver = config.getDriverPath() + "\\chromedriver.exe";
				if (webdriver_chrome_driver.trim().length() == 0)
					throw new TestScriptException("Error: Driver path has not specified.");
				System.setProperty("webdriver.chrome.driver", webdriver_chrome_driver);

				ChromeOptions options = new ChromeOptions();
				options.addArguments("--start-maximized");
				//options.addArguments("incognito");
				options.addArguments("--disable-extensions");
				options.addArguments("test-type");
				options.addArguments("--disable-web-security");
				options.addArguments("--no-proxy-server");
				Map<String, Object> prefs = new HashMap<String, Object>();
				prefs.put("credentials_enable_service", false);
				prefs.put("profile.password_manager_enabled", false);
				options.setExperimentalOption("prefs", prefs);
				driver = new ChromeDriver(options);

			} else if (standardBrowserType.equalsIgnoreCase("IE")) {

				String webdriver_ie_driver = "drivers/IEDriverServer.exe";
				if (webdriver_ie_driver.trim().length() == 0)
					throw new TestScriptException("Error: Driver path has not specified.");

				System.setProperty("webdriver.ie.driver", webdriver_ie_driver);

				DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
				capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
				// For removing slow typing problem
				capabilities.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true);
				capabilities.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
				//capabilities.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, true);
				
				/*capabilities.setCapability("ignore-certificate-error", true);
				capabilities.setCapability(InternetExplorerDriver.FORCE_CREATE_PROCESS, true);
				capabilities.setCapability(InternetExplorerDriver.IE_SWITCHES, "-private");
				capabilities.setCapability(InternetExplorerDriver.UNEXPECTED_ALERT_BEHAVIOR, true);
				capabilities.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
				
				capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				capabilities.setCapability("disable-popup-blocking", true);
				capabilities.setCapability("disable-translate", true);
				capabilities.setCapability("enablePersistentHover", false);
				capabilities.setCapability("allow-running-insecure-content", true);
				capabilities.setCapability("allow-blocked-content", true);
				capabilities.setCapability("allowBlockedContent", true);
				capabilities.setCapability("ignoreProtectedModeSettings", true); */
				driver = new InternetExplorerDriver(capabilities);
										
			} else if (standardBrowserType.equalsIgnoreCase("Safari")) {
				if (!getOperatingSystemType().equalsIgnoreCase("MAC")) {
					testStep[2] = "To launch Safari, please run script on Mac";
					status = "Fail";
					testStep[4] = "";
					methodFailureMessage = testStep[2];
				}
				driver = new SafariDriver();
			} else {
				testStep[2] = "Please choose proper browser (Firefox/Chrome/IE/Safari)";
				status = "Fail";
				testStep[4] = "";
				methodFailureMessage = testStep[2];
			}
			driver.manage().window().maximize();
			driver.manage().timeouts().pageLoadTimeout(5, TimeUnit.MINUTES);
			driverList.add(driver);
			// waitForPageLoaded(driver);
		} catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = browserType + " could not be launched because of an Exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2] + " " + e.getMessage();
				config.getLogger().error(methodFailureMessage);
			}
		} catch(Error err){
			if (testStep[4] == null) {
				testStep[2] = browserType + " could not be launched because of an Error. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2] + " " + err.getMessage();
				config.getLogger().error(methodFailureMessage);
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/**
	 * Returns the Operating System
	 */
	protected String getOperatingSystemType() {
		String detectedOS;
		String OS = System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH);
		if ((OS.indexOf("mac") >= 0) || (OS.indexOf("darwin") >= 0)) {
			detectedOS = "Mac";
		} else if (OS.indexOf("win") >= 0) {
			detectedOS = "Windows";
		} else if (OS.indexOf("nux") >= 0) {
			detectedOS = "Linux";
		} else {
			detectedOS = "Other";
		}
		return detectedOS;
	}

	
	/** This method is used to perform some generic browser actions
	 * @param action
	 * 		  Supported actions are "BACK", "FORWARD", "REFRESH"
	 * 		  "BACK" - provide this action for pressing back button for browser.
	 * 		  "FORWARD" - provide this action for pressing forward button for browser.
	 * 		  "REFRESH" - provide this action for refreshing current browser window.
	 * @return String[]
	 * 		   Returns the result of the action
	 * @throws TestScriptException
	 */
	public String[] browserNavigation(String action) throws TestScriptException{
		String[] testStep = getStepInstance();
		testStep[0] = "Perform "+action +" for browser.";
		testStep[1] = "Action "+ action + " should be performed";
		testStep[2] = "Action "+ action + " is performed";
		testStep[4] = null;
		String status = "Pass";

		try {
			String browserAction = action.toUpperCase();
			
			switch(browserAction){
			case "BACK":
				driver.navigate().back();
				break;
			case "FORWARD":	
				driver.navigate().forward();
				break;
			case "REFRESH":	
				driver.navigate().refresh();
				break;	
			default :
				status = "Fail";
				testStep[2] = "Selected action is not supported, please select appropriate action and try again";
				testStep[4] = "";
				methodFailureMessage = testStep[2];
				throw new Exception();
			}
			
				
		}catch (Exception e) {
			if(testStep[4]==null){
				testStep[2] = "Unable to perform action " + action + " because of an Exception. Please refer Logs/info.log for more details.";
				testStep[4] ="";
				status = "Fail";
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
			
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}
	
	/**
	 * This method utilizes the Saucelabs to launch the browser and do all the
	 * execution over there.
	 * 
	 * @param URL
	 *              The url for the saucelab instance appended with our saucelab
	 *            username and password
	 * @param Capabilities
	 *              Platform, version of the browser that we need to launch.
	 * @return String[] 
	 * 				Returns the result of the executionn
	 * @throws TestScriptException
	 */
	public String[] launchSauceLabs(String URL, String Capabilities) throws TestScriptException {

		String str[] = Capabilities.split(",");
		DesiredCapabilities caps = null;

		testStep = getStepInstance();

		testStep[0] = "Launch " + str[0];
		testStep[1] = str[0] + " should be launched";
		testStep[2] = str[0] + " is launched";
		testStep[4] = "";
		String status = "Pass";

		try {
			if (str[0].equalsIgnoreCase("chrome")) {
				caps = DesiredCapabilities.chrome();
			} else if (str[0].equalsIgnoreCase("IE")) {
				caps = DesiredCapabilities.internetExplorer();
			} else if (str[0].equalsIgnoreCase("Safari")) {
				caps = DesiredCapabilities.safari();
			} else {
				caps = DesiredCapabilities.firefox();
			}
			caps.setCapability("platform", str[1]);
			caps.setCapability("version", str[2]);
			caps.setCapability("screen-resolution", "1280x1024");
			setDriver(new RemoteWebDriver(new URL(URL), caps));
		} catch (Exception e) {
			testStep[2] = "Unable to launch " + str[0] + " because of an Exception. Please refer Logs/info.log for more details.";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			status = "Fail";
			methodFailureMessage = testStep[2] + " " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/**
	 * This method waits maximum for 30 seconds for the page dom status to be
	 * complete.
	 * 
	 * @param driver
	 * 		  Webdriver instance
	 * @throws TestScriptException
	 */
	protected void waitForPageLoaded(WebDriver driver) throws TestScriptException {

		try {
			ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
				public Boolean apply(WebDriver driver) {
					return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
				}
			};
			Wait<WebDriver> wait = new WebDriverWait(driver, 30);
			try {
				wait.until(expectation);
			} catch (Throwable error) {
				methodFailureMessage = "Timeout waiting for Page Load Request to complete " + error.getMessage();
				config.getLogger().error(methodFailureMessage);
				throw new TestScriptException(methodFailureMessage);
			}
		} catch (Exception e) {
			methodFailureMessage = "WaitForPageLoaded method failed because of exception " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage);
		}
	}

	/**
	 * Closes the browser
	 * 
	 * @param flag
	 *            If this is "Y" all the created records will be deleted
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 * @throws TestScriptException
	 */
	public Object closeBrowser(String flag) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Close the browser";
		testStep[1] = "Browser should be closed";
		testStep[2] = "Browser is closed";
		testStep[4] = "";
		String status = "Pass";
		Boolean testResult = true;

		try {
			if (flag.equalsIgnoreCase("N")) {
				for (WebDriver wDriver : driverList) {
					wDriver.quit();
				}
			} else if (flag.equalsIgnoreCase("Y")) {
				//new SalesforceCommands().deleteRecord();
				for (WebDriver wDriver : driverList) {
					wDriver.quit();
				}
			} else {
				status = "Fail";
				testStep[2] = "Argument for closeBrowser should either be Y or N";
				methodFailureMessage = testStep[2];
			}
		} catch (Exception e) {
			testStep[2] = "Browser could not be closed because of an Exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			methodFailureMessage = testStep[2] + " " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			throw new TestScriptException(methodFailureMessage, testStep);
		}

		if (testResult) {
			return testStep;
		}
		return true;
	}

	/**
	 * Opens the URL in the browser
	 * 
	 * @param url
	 *            URL to be opened
	 * @return String[] 
	 * 			  Returns the result of the execution
	 */
	public String[] openUrl(String url) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Hit the url '" + url + "'";
		testStep[1] = url + " should be opened";
		testStep[2] = url + " is opened";
		testStep[4] = "";
		String status = "Pass";

		try {
			driver.get(url);
		} catch (Exception e) {
			testStep[2] = "Unable to open " + url + " due to an Exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			methodFailureMessage = testStep[2] + " " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/**
	 * This method opens the given URL in a new tab
	 * 
	 * @param url
	 *            URL to be opened
	 * @return String[] 
	 * 			  Returns the result of the execution
	 * 
	 * @throws TestScriptException
	 */
	public String[] openURLInTab(String url) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that " + url + " is opened in new tab";
		testStep[1] = url + "should be opened in new Tab";
		testStep[2] = url + " is opened";
		testStep[4] = "";
		String status = "Pass";

		try {
			String script = "var d=document,a=d.createElement('a');a.target='_blank';a.href='%s';a.innerHTML='.';d.body.appendChild(a);return a";
			// injecting the element into the current page using JS
			Object element = ((JavascriptExecutor) driver).executeScript(String.format(script, url));
			if (element instanceof WebElement) {
				WebElement anchor = (WebElement) element;
				anchor.click();
				((JavascriptExecutor) driver).executeScript("var a=arguments[0];a.parentNode.removeChild(a);", anchor);
			} else {
				testStep[2] = "Unable to open " + url + " in new tab";
				status = "Fail";
				methodFailureMessage = "Unable to open tab\t" + url;
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			}
		} catch (Exception e) {
			if (testStep[4].isEmpty()) {
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				testStep[2] = "Unable to open url : " + url + " because of an Exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
			config.getLogger().error(methodFailureMessage);
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/**
	 * Waits till TIMEOUT for the title to load and then asserts page title
	 * 
	 * @param title
	 *            String expected to be present in the title of the webpage
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 * @throws AssertionError
	 * @throws InterruptedException
	 */
	public Object assertPageTitleContains(String title)
			throws AssertionError, InterruptedException, TestScriptException {

		testStep = getStepInstance();
		testStep[0] = "Verify that " + title + "should be present as title on page";
		testStep[1] = title + "should be present as title on page";
		testStep[2] = "Page Title is as required.";
		testStep[4] = "";
		String status = "Pass";
		try {
			if (!driver.getTitle().trim().toLowerCase().contains(title.trim().toLowerCase())) {
				mWait("timed out after " + TIMEOUT + " sec");
				assertPageTitleContains(title);
			} else {
				timeOutCounter = 0;
			}
		} catch (TimeoutException e) {
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			testStep[2] = "Expected page title to contain : " + title + " , found : " + driver.getTitle();
			status = "Fail";
			methodFailureMessage = "Expected page title to contain : " + title + " , found : " + driver.getTitle();
		} catch (Exception e) {
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			testStep[2] = title + " on page cannot be validated because of an Exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			methodFailureMessage = e.getMessage();
		} finally {
			testStep[3] = status;
		}

		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		} else {
			return true;
		}
	}

	/**
	 * Waits till TIMEOUT for title to load and asserts page title that it
	 * does not contain the mentioned string 
	 *
	 * @param notInTitle
	 *            String expected not to be present in the title of the webpage
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 * @throws AssertionError
	 * @throws InterruptedException
	 */
	public Object assertPageTitleDoesNotContain(String notInTitle)
			throws AssertionError, InterruptedException, TestScriptException {

		testStep = getStepInstance();
		testStep[0] = "Verify that " + notInTitle + "should not be present as title on page";
		testStep[1] = notInTitle + "should not be present as title on page";
		testStep[2] = "Page Title is not having the text " + notInTitle;
		testStep[4] = "";
		String status = "Pass";

		try {
			if (driver.getTitle().trim().toLowerCase().contains(notInTitle.trim().toLowerCase())) {
				// wait for page load
				mWait("timed out after " + TIMEOUT + " sec");
				assertPageTitleDoesNotContain(notInTitle);
			} else {
				// reset timeoutCounter
				timeOutCounter = 0;
			}
		} catch (TimeoutException e) {
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			testStep[2] = "Expected page title still contains : " + notInTitle + " , complete title is "
					+ driver.getTitle();
			status = "Fail";
			methodFailureMessage = testStep[2] + " " + e.getMessage();
		} catch (Exception e) {
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			testStep[2] = "Title on page cannot be validated because of an Exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			methodFailureMessage = testStep[2] + " " + e.getMessage();
		} finally {
			testStep[3] = status;
		}

		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		} else {
			return true;
		}
	}

	/**
	 * Click the specified webElement
	 * 
	 * @param name
	 *            Name of the element (should be present in the local
	 *            repository)
	 * @param type
	 *            Type of the element like button, link and so on
	 * @return String[] 
	 * 			  Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] clickElement(String name, String type) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Click the element " + name;
		testStep[1] = "Element should be clicked";
		testStep[2] = "Element is clicked";
		testStep[4] = "";
		String status = "Pass";

		try {
			if (type.equalsIgnoreCase("tab"))
				clickTab(name);
			else {

				By webEle = getLocator(name);
				WebElement webElement = findElement(webEle);
				waitForElementToBeClickable(webEle);
				webElement.click();
			}
		} catch (Exception e) {
			testStep[2] = "Element could not be clicked because of exception. Please refer Logs/info.log for more details. ";
			status = "Fail";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			methodFailureMessage = testStep[2] + " " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
		} finally {

			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			throw new TestScriptException(methodFailureMessage, testStep);
		}

		return testStep;
	}

	/**
	 * Clicks and opens SFDC tab specified. If the tab is not found in the Tab
	 * Navigation ribbon, All tabs page is opened and the specified tab is
	 * clicked from there
	 * 
	 * @param name
	 *            Name of the tab
	 * @return String[] 
	 * 			  Returns the result of the execution
	 * @throws TestScriptException
	 */
	protected String[] clickTab(String name) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Click the tab " + name;
		testStep[1] = "Tab should be clicked";
		testStep[2] = "Tab is clicked";
		testStep[4] = "";
		String status = "Pass";

		try {
			WebElement webElement = null;
			try {
				By webEle = getLocator(name);
				webElement = findElement(webEle);
				webElement.click();
			} catch (Exception e) {
				webElement = findElement(By.xpath("//*[@id='AllTab_Tab']/a"));
				webElement.click();
				String tabXpath = "//a[contains(@class,'listRelatedObject') and text()='" + name + "']";
				// String tabXpath="//*[@id='tabBar']/li/a[text()='"+name+"']";
				WebElement reqTab = findElement(By.xpath(tabXpath));
				reqTab.click();
			}
		} catch (Exception e) {
			testStep[2] = "Tab could not be clicked because of an Exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			methodFailureMessage = testStep[2] + " " + e.getMessage();
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}

		return testStep;
	}

	/**
	 * This method clicks the specified element via JavaScript
	 * 
	 * @param name
	 *            Name of the element (should be present in the local
	 *            repository)
	 * @return String[] 
	 * 			  Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] clickElement(String name) throws TestScriptException {

		testStep = getStepInstance();
		testStep[0] = "Verify that " + name + " is clicked";
		testStep[1] = name + " should be clicked";
		testStep[2] = name + " is clicked";
		testStep[4] = "";
		String status = "Pass";

		try {
			WebElement element = driver.findElement(getLocator(name));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", element);
		} catch (Exception e) {
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			testStep[2] = "Unable to click " + name + " because of an Exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			methodFailureMessage = testStep[2] + " " + e.getMessage();
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/**
	 * Enters the provided value in the specified element
	 * 
	 * @param name
	 *            Name of the element (details present in local object
	 *            repository)
	 * @param type
	 *            Supported values are text,picklist,checkbox,lookup,multipicklist,radio
	 * @param value
	 *            Value to be entered
	 * @return String[] 
	 * 			  Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] inputData(String name, String type, String value) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Input the data for the element " + name;
		testStep[1] = "Element should be populated with the required data";
		testStep[2] = "Element is populated with the provided data";
		testStep[4] = "";
		String status = "Pass";

		try {
			if (type.equalsIgnoreCase("text") || type.equalsIgnoreCase("integer") || type.equalsIgnoreCase("double"))
				inputText(name, value);
			else if (type.equalsIgnoreCase("date"))
				inputDate(name, value);
			else if (type.equalsIgnoreCase("picklist"))
				selectPicklistValue(name, value);
			else if (type.equalsIgnoreCase("checkbox"))
				selectCheckbox(name, value);
			else if (type.equalsIgnoreCase("lookup"))
				selectLookup(name, value);
			else if (type.equalsIgnoreCase("multipicklist"))
				selectMultiplicklistValue(name, value);
			else if (type.equalsIgnoreCase("radio"))
				selectRadioButton(name, value);
		} catch (Exception e) {
			testStep[2] = "Element could not be populated with the provided data because of an Exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			methodFailureMessage = testStep[2] + " " + e.getMessage();
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}

		return testStep;
	}

	/**
	 * Input value to the alert and click ok.
	 * 
	 * @param Value
	 *            Value to be entered in the alert
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 * @throws TestScriptException
	 */
	public Object inputToAlert(String value) throws TimeoutException, InterruptedException, TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Input the alert with the provided value ";
		testStep[1] = "Alert should be populated with the provided data";
		testStep[2] = "Alert is populated with the provided data";
		testStep[4] = "";
		String status = "Pass";
		try {
			WebDriverWait wait = new WebDriverWait(driver, 1);
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
				if (value != "N") {
				alert.sendKeys(value);
			}
			alert.accept();
		} catch (NoAlertPresentException e) {
			testStep[2] = "TimeOut after waiting " + TIMEOUT + " sec. for the alert to appear.";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			methodFailureMessage = testStep[2] + " " + e.getMessage();
			mWait("TimeOut after waiting " + TIMEOUT + " sec. for the alert to appear");
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return true;
	}

	/**
	 * This method selects the provided value from the provided lookup
	 * 
	 * @param name
	 *            - Identifier for the lookup
	 * @param value
	 *            - Value to be selected in the lookup
	 * @throws TestScriptException
	 */
	protected void selectLookup(String name, String value) throws TestScriptException {
		try {
			String defaultWindow = driver.getWindowHandle();
			LocalObject object = config.getLocalObjectRepo().get(name.toUpperCase());
			String locatorType = object.getObjectLocatorType();
			String locator = object.getObjectLocator();

			String lookupXpath = locator + "//following::img[contains(@class,'lookupIcon')][1]";
			WebElement lookup = findElement(By.xpath(lookupXpath));
			// WebElement lookup = parent.findElement(By.tagName("img"));

			// WebElement lookup =
			// parent.findElement(By.className("lookupIcon"));*/
			// WebElement lookup =
			// driver.findElement(By.xpath("//img[contains(@class,'lookupIcon')]"));
			// WebElement lookup = findElement(webEle);
			lookup.click();

			Utility.sleep(5000);
			boolean flag = (boolean) switchToWindowUsingTitle("Search");
			String lookupPageHandle = driver.getWindowHandle();
			WebElement searchFrame = driver.findElement(By.id("searchFrame"));
			driver.switchTo().frame(searchFrame);
			WebElement searchString = findElement(By.id("lksrch"));
			searchString.clear();
			searchString.sendKeys(value);
			findElement(By.name("go")).click();
			Utility.sleep(2000);
			driver.switchTo().window(lookupPageHandle);
			WebElement resultFrame = driver.findElement(By.id("resultsFrame"));
			driver.switchTo().frame(resultFrame);

			WebElement lookupSearch = findElement(By.cssSelector(".list"));
			List<WebElement> tr = lookupSearch.findElement(By.tagName("tbody")).findElements(By.tagName("tr"));
			tr.remove(0);
			for (WebElement trElement : tr) {
				if (trElement.getAttribute("class").isEmpty()) {
					break;
				} else {
					WebElement link = trElement.findElement(By.tagName("th")).findElement(By.tagName("a"));
					String linkText = link.getText();
					if (linkText.equalsIgnoreCase(value)) {
						link.click();
						break;
					}
				}
			}
			driver.switchTo().window(defaultWindow);
		} catch (Exception e) {
			methodFailureMessage = "Select Lookup method failed because of exception " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage);
		}
	}

	/**
	 * Selects values in the multi-picklist
	 * 
	 * @param name
	 *            Name of the multi-picklist
	 * @param value
	 *            Values to be selected separated by comma
	 * @throws TestScriptException
	 */
	protected void selectMultiplicklistValue(String name, String value) throws TestScriptException {
		try {
			WebElement unselected = findElement(getLocator(name));
			WebElement add = findElement(getLocator(name + "Add"));
			String[] values = value.split(",");
			for (String val : values) {
				List<WebElement> options = unselected.findElements(By.tagName("option"));
				for (WebElement option : options) {
					String text = option.getText();
					if (text.equalsIgnoreCase(val)) {
						option.click();
						add.click();
						break;
					}
				}
			}
		} catch (Exception e) {
			methodFailureMessage = "Select Multipicklist method failed because of exception " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage);
		}
	}

	/**
	 * Types the value in the text box specified
	 * 
	 * @param name
	 *            Name of the element
	 * @param value
	 *            Value to be entered
	 * @throws TestScriptException
	 */
	protected void inputText(String name, String value) throws TestScriptException {
		try {
			waitForElementToBeAvailable(getLocator(name));
			By webEle = getLocator(name);
			WebElement webElement = findElement(webEle);

			webElement.sendKeys(Keys.CONTROL + "a");
			webElement.sendKeys(Keys.DELETE);

			webElement.clear();
			webElement.sendKeys(value);
		} catch (Exception e) {
			methodFailureMessage = "InputText method failed because of exception " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage);
		}
	}

	/**
	 * Enters the date in the date type element
	 * 
	 * @param name
	 *            Name of the element
	 * @param value
	 *            Value to be entered
	 * @throws TestScriptException
	 */
	protected void inputDate(String name, String value) throws TestScriptException {
		try {
			By webEle = getLocator(name);
			WebElement webElement = findElement(webEle);
			webElement.sendKeys(value);
		} catch (Exception e) {
			methodFailureMessage = "InputDate method failed because of exception " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage);
		}
	}

	/**
	 * Selects or de-selects the checkbox
	 * 
	 * @param name
	 *            Name of the checkbox
	 * @param value
	 *            Value to be entered; should be 'check' or 'uncheck'savd
	 * @throws TestScriptException
	 */
	protected void selectCheckbox(String name, String value) throws TestScriptException {
		try {
			By webEle = getLocator(name);
			WebElement webElement = findElement(webEle);
			if ((value.equalsIgnoreCase("uncheck") && webElement.isSelected())
					|| (value.equalsIgnoreCase("check") && !webElement.isSelected()))
				webElement.click();
		} catch (Exception e) {
			methodFailureMessage = "SelectCheckbox method failed because of exception " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage);
		}
	}

	/**
	 * Selects the provided value from the mentioned picklist
	 * 
	 * @param name
	 *            Name of the picklist
	 * @param value
	 *            Values to be selected
	 * @throws TestScriptException
	 */
	protected void selectPicklistValue(String name, String value) throws TestScriptException {
		try {
			By webEle = getLocator(name);
			WebElement webElement = findElement(webEle);
			Select picklist = new Select(webElement);
			picklist.selectByVisibleText(value);
		} catch (Exception e) {
			methodFailureMessage = "SelectPicklistValue method failed because of exception " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage);
		}
	}

	/**
	 * This method stores the selected value for the picklist in the provided
	 * variable
	 * 
	 * @param element
	 *            Picklist whose selected value needs to be stored.
	 * @param variable
	 *            Variable to be used for storing picklist value
	 * @return String[] 
	 * 			  Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] storePicklistSelectedValue(String element, String variable) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Store the selected value for picklist " + element + " in variable " + variable;
		testStep[1] = "Selected value for picklist " + element + " should be stored in " + variable;
		testStep[2] = "Selected Picklist value is stored in the " + element + " variable " + variable;
		testStep[4] = "";
		String status = "Pass";

		try {
			By webEle = getLocator(element);
			WebElement webElement = findElement(webEle);
			Select picklist = new Select(webElement);
			String picklistSelectedValue = picklist.getFirstSelectedOption().getText().trim();
			config.setMapGlobalVariable(variable, picklistSelectedValue);
		} catch (Exception e) {
			testStep[2] = "Element could not be verified because of an Exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			methodFailureMessage = testStep[2] + " " + e.getMessage();
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/**
	 * Verify the text for the particular element
	 * 
	 * @param name
	 *            Name of the element
	 * @param text
	 *            Text that is to be verified
	 * @return String[] 
	 * 			  Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyText(String name, String text) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that the element " + name + " is having the text " + text;
		testStep[1] = "Element " + name + " should have the required text";
		testStep[2] = "Verified text for " + name + ". It is as expected";
		testStep[4] = "";
		String status = "Pass";

		try {
			String screenText;
			By webEle = getLocator(name);
			WebElement webElement = findElement(webEle);
			mouseHover(name);
			screenText = webElement.getText().trim();

			if (screenText.equalsIgnoreCase(text)) {
				config.getLogger().info("Verified text for " + name + ". It is as expected " + text);
			} else {
				testStep[2] = "Verified text for " + name + ". Expected text was " + text + " found this instead "
						+ screenText;
				status = "Fail";

				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2];
			}
		} catch (Exception e) {
			if (testStep[4].isEmpty()) {
				testStep[2] = "Element could not be verified because of an Exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/**
	 * Verify that the element contains the mentioned text
	 * 
	 * @param name
	 *            Name of the element
	 * @param text
	 *            Text that is to be verified
	 * @return String[] 
	 * 			  Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyElementContainsText(String name, String text) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that the element " + name + " is having the text " + text;
		testStep[1] = "Element " + name + " should contain the required text " + text;
		testStep[2] = "Element " + name + " contains the mentioned text";
		testStep[4] = "";
		String status = "Pass";

		try {
			By webEle = getLocator(name);
			WebElement webElement = findElement(webEle);
			mouseHover(name);
			String screenText = webElement.getText().trim();
			screenText = screenText.toLowerCase();
			text = text.toLowerCase();
			if (screenText.contains(text)) {
				config.getLogger().info("Verified text for " + name + ". It is as expected " + text);
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			} else {
				testStep[2] = "Element " + name + " does not contains the mentioned text";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2];
			}
		} catch (Exception e) {
			if (testStep[4].isEmpty()) {
				testStep[2] = "Element could not be verified because of an Exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/**
	 * Verify that the text for the mentioned element is blank
	 * 
	 * @param name
	 *            Name of the element
	 * @param text
	 *            Text that is to be verified
	 * @return String[] 
	 * 			  Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyBlankText(String element) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that the text for the " + element + " is blank ";
		testStep[1] = element + " text should be blank";
		testStep[2] = "Verified text for " + element + ". It is blank as expected ";
		testStep[4] = "";
		String status = "Pass";
		try {
			By webEle = getLocator(element);
			WebElement webElement = findElement(webEle);
			mouseHover(element);
			String screenText = webElement.getText().trim();
			if (screenText == null || screenText.equalsIgnoreCase("")) {
				config.getLogger().info("Verified text for " + element + ". It is blank as expected ");
			} else {
				testStep[2] = "Verified text for " + element
						+ ". It was expected to be blank but found to be this instead " + screenText;
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				status = "Fail";
				methodFailureMessage = testStep[2];
			}
		} catch (Exception e) {
			if (testStep[4].isEmpty()) {
				testStep[2] = "Element could not be verified because of an Exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/**
	 * Verifies the given attribute of the element
	 * 
	 * @param name
	 *            Name of the element
	 * @param attribute
	 *            Attribute of the element that needs to be verified
	 * @param value
	 *            Expected value of the attribute
	 * @return String[] 
	 * 			  Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyAttribute(String name, String attribute, String value) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that the element " + name + " has the required value for attribute " + attribute;
		testStep[1] = "Attribute " + attribute + " should have value " + value;
		testStep[2] = "Attribute " + attribute + " have value " + value;
		testStep[4] = "";
		String status = "Pass";

		try {
			By webEle = getLocator(name);
			WebElement webElement = findElement(webEle);
			mouseHover(name);
			String attval = webElement.getAttribute(attribute);
			if (value.equalsIgnoreCase(attval)) {
				config.getLogger().info("Verified value of attribute " + attribute + " for element " + name
						+ ". It is as expected " + value);
			} else {
				testStep[2] = "Attribute " + attribute + " don't have value " + value;
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2];
			}
		} catch (Exception e) {
			if (testStep[4].isEmpty()) {
				testStep[2] = "Element could not be verified because of an Exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/**
	 * Verifies that the given attribute of the element contains / don't contains the expected text
	 * 
	 * @param name
	 *            Name of the element
	 * @param attribute
	 *            Attribute of the element that needs to be verified
	 * @param value
	 *            Expected value that the attribute should contain. Also, if
	 *            expected value contains !, it indicates we are testing the
	 *            attribute does not contains this value
	 * @return String[] 
	 * 		      Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyAttributeContainsText(String name, String attribute, String value)
			throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that the element " + name + " attribute " + attribute + " contains mentioned value";
		testStep[1] = "Attribute " + attribute + " should contain value " + value;
		testStep[2] = "Attribute " + attribute + " contains value " + value;
		String status = "Pass";

		try {
			By webEle = getLocator(name);
			WebElement webElement = findElement(webEle);
			mouseHover(name);
			String attval = webElement.getAttribute(attribute);
			Boolean MethodResult;

			if (value.startsWith("!")) {
				value = value.substring(1);
				if (!attval.contains(value)) {
					MethodResult = true;
					testStep[0] = "Verify that the element " + name + " attribute " + attribute + " don't contains mentioned value";
					testStep[1] = "Attribute " + attribute + " should not contain value " + value;
					testStep[2] = "Attribute " + attribute + " does not contains value " + value;
					testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					config.getLogger().info(
							"Attribute " + attribute + " for element " + name + " doesn't contains value " + value);
				} else {
					MethodResult = false;
					testStep[2] = "Attribute " + attribute + " contains value " + value;
					status = "Fail";
					testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					methodFailureMessage = testStep[2];
				}
			} else {

				if (attval.contains(value)) {
					MethodResult = true;
					testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					config.getLogger()
							.info("Attribute " + attribute + " for element " + name + " contains value " + value);
				} else {
					MethodResult = false;
					testStep[2] = "Attribute " + attribute + " don't contain value " + value;
					status = "Fail";
					testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					methodFailureMessage = testStep[2];
				}
			}
		} catch (Exception e) {
			if (testStep[4].isEmpty()) {
				testStep[2] = "Element could not be verified because of an Exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/**
	 * Verifies the presence of the specified element
	 * 
	 * @param name
	 *            Name of the element
	 * @return String[] 
	 * 			  Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyPresence(String name) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that the element " + name + " is present on the page";
		testStep[1] = "Element " + name + " should be there on the page";
		testStep[2] = "Element " + name + " is there on the page";
		String status = "Pass";

		try {
			By webEle = getLocator(name);
			WebElement webElement = findElement(webEle);
			webElement.isDisplayed();
			mouseHover(name);
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			config.getLogger().info("Element found " + name);
		} catch (NoSuchElementException e) {
			testStep[2] = "Element " + name + " is not there on the page";
			status = "Fail";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			methodFailureMessage = testStep[2] + " " + e.getMessage();
		} catch (Exception e) {
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			testStep[2] = "Element could not be verified because of an Exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			methodFailureMessage = testStep[2] + " " + e.getMessage();
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/**
	 * Verifies the picklist values for the webelement
	 * 
	 * @param name
	 *            Name of the picklist
	 * @param value
	 *            Expected picklist values
	 * @return String[] 
	 * 			  Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyValuesForPicklist(String name, String value) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that the picklist values for field " + name + " is " + value;
		testStep[1] = "Picklist values for " + name + " should be " + value;
		testStep[2] = "Picklist values for " + name + " should have " + value;
		testStep[4] = "";
		String status = "Pass";

		try {
			By webEle = getLocator(name);
			Select picklist = new Select(findElement(webEle));
			mouseHover(name);
			String expectedValue = "";
			String actualValue = "";

			String[] test = value.split(",");
			List<String> expected = new ArrayList<String>();
			List<String> actual = new ArrayList<String>();

			for (int i = 0; i < test.length; i++) {
				if (test[i].contains(",")) {
					String[] temp = test[i].split(",");
					for (int j = 0; j < temp.length; j++) {
						expected.add(temp[j]);
					}
				} else {
					expected.add(test[i]);
				}
			}

			List<WebElement> options = picklist.getOptions();
			int i = 0;
			for (WebElement opt : options) {
				String optText = opt.getText().trim();
				if (!optText.contains("None"))
					actual.add(optText);
			}

			Collections.sort(actual);
			Collections.sort(expected);

			String[] actualValues = new String[actual.size()];
			actualValues = actual.toArray(actualValues);

			String[] expectedValues = new String[expected.size()];
			expectedValues = expected.toArray(expectedValues);

			for (int k = 0; k < expectedValues.length; k++) {
				if (expectedValue.isEmpty()) {
					expectedValue = expectedValues[k];
				} else {
					expectedValue = expectedValue + "," + expectedValues[k];
				}
			}

			for (int l = 0; l < actualValues.length; l++) {
				if (actualValue.isEmpty()) {
					actualValue = actualValues[l];
				} else {
					actualValue = actualValue + "," + actualValues[l];
				}
			}

			boolean flag = Arrays.equals(expectedValues, actualValues);
			if (flag) {
				config.getLogger().info("Values of picklist is as expected");
				testStep[2] = "Picklist values for the field " + name + " is " + expectedValue
						+ " and it is as expected.";
				testStep[4] = "";
			} else {
				config.getLogger().error("Values of picklist do not match");
				testStep[2] = "Picklist values for the field " + name + " is " + actualValue + " instead of "
						+ expectedValue + ".";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2];
			}
		} catch (Exception e) {
			if (testStep[4].isEmpty()) {
				testStep[2] = "Element could not be verified because of an Exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/**
	 * Switches driver to the window containing the given title
	 * 
	 * @param title
	 *            Title to be present in the window
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failu
	 * @throws TestScriptException
	 */
	public Object switchToWindowUsingTitle(String title) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that the execution is switched to window with the title " + title;
		testStep[1] = "Execution should be switched to window with the title " + title;
		testStep[2] = "Switched to window with the title " + title;
		testStep[4] = "";
		String status = "Pass";
		Boolean flag = false;

		try {
			String currentWindow = driver.getWindowHandle();
			Set<String> availableWindows = driver.getWindowHandles();
			if (!availableWindows.isEmpty()) {
				for (String windowId : availableWindows) {
					if (windowId.equals(currentWindow)) {
						continue;
					}
					driver.switchTo().window(windowId);
					String wintitle = driver.getTitle();
					if ((wintitle.equals(title)) || (wintitle.contains(title))) {
						System.out.println("Switched to required window");
						flag = true;
						break;
					}
				}
			}
			if (!flag) {
				driver.switchTo().window(currentWindow);
			}
		} catch (Exception e) {
			testStep[2] = "Cannnot switch to window because of exception. Please refer Logs/info.log for more details.";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			status = "Fail";
			methodFailureMessage = testStep[2] + e.getMessage();
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return true;

	}

	/**
	 * Switches driver to the window containing the given URL
	 * 
	 * @param url
	 *            URL of the window
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 * @throws TestScriptException
	 */
	public Object switchToWindowUsingURL(String url) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that the execution is switched to window with the url " + url;
		testStep[1] = "Execution should be switched to window with the url " + url;
		testStep[2] = "Switched to window with the url " + url;
		testStep[4] = "";
		String status = "Pass";

		try {
			String currentWindow = driver.getWindowHandle();
			Set<String> availableWindows = driver.getWindowHandles();
			availableWindows.remove(currentWindow);
			if (!availableWindows.isEmpty()) {
				for (String windowId : availableWindows) {
					if ((driver.switchTo().window(windowId).getCurrentUrl().equals(url))
							|| (driver.switchTo().window(windowId).getCurrentUrl().contains(url))) {
						break;
					} else {
						driver.switchTo().window(currentWindow);
					}
				}
			}

		} catch (Exception e) {
			testStep[2] = "Cannnot switch to window because of exception. Please refer Logs/info.log for more details.";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			status = "Fail";
			methodFailureMessage = testStep[2] + e.getMessage();
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}

		return true;
	}

	/**
	 * Switches the driver to the mentioned iframe
	 * 
	 * @param iframe
	 *            Name of the iframe
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 * @throws TestScriptException
	 */
	public Object switchToIFrame(String iframe) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that the execution is switched to mentioned iframe";
		testStep[1] = "Execution should be switched to mentioned iframe";
		testStep[2] = "Switched to mentioned iframe";
		testStep[4] = "";
		String status = "Pass";

		try {
			WebElement frame = findElement(getLocator(iframe));
			this.driver.switchTo().frame(frame);
		} catch (Exception e) {
			testStep[2] = "Cannnot switch to iframe because of exception. Please refer Logs/info.log for more details.";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			status = "Fail";
			methodFailureMessage = testStep[2] + e.getMessage();
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return true;
	}

	/**
	 * Switches the driver to the iframe which contains the mentioned webelement.
	 * 
	 * @param name
	 *            Name of the webelement
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 * @throws TestScriptException
	 */
	public Object switchToIFrameBasedOnElement(String name) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that the execution is switched to mentioned iframe";
		testStep[1] = "Execution should be switched to iframe based on mentioned element";
		testStep[2] = "Switched to iframe based on the mentioned element";
		testStep[4] = "";
		String status = "Fail";

		try {
			List<WebElement> iframes = findElements(By.tagName("iframe"));
			for (WebElement iframe : iframes) {
				config.getLogger().debug(iframe.getAttribute("id"));
				this.driver.switchTo().frame(iframe);
				try {
					findElement(getLocator(name));
					config.getLogger().info("IFrame found");
					status = "Pass";
					break;
				} catch (Exception ex) {
					this.driver.switchTo().defaultContent();
				}
			}
			if (!status.equals("Pass")) {
				testStep[2] = "Cannnot switch to iframe because iFrame not found";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2];
			}
		} catch (Exception e) {
			if (testStep[4].isEmpty()) {
				testStep[2] = "Cannnot switch to iframe because of exception. Please refer Logs/info.log for more details.";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				status = "Fail";
				methodFailureMessage = testStep[2] + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		} else {
			return true;
		}
	}

	/**
	 * Switch to default context of DOM
	 * 
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 * @throws TestScriptException
	 */
	public Object switchToDefault() throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that the execution is switched to default window";
		testStep[1] = "Execution should be switched to default window";
		testStep[2] = "Switched to default window";
		testStep[4] = "";
		String status = "Pass";

		try {
			driver.switchTo().defaultContent();
		} catch (Exception e) {
			testStep[2] = "Cannnot switch to default window. Please refer Logs/info.log for more details.";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			status = "Fail";
			methodFailureMessage = testStep[2] + e.getMessage();

		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return true;
	}

	/**
	 * Selects all the values of the multi select picklist
	 * 
	 * @param by
	 *            Name of the multi select picklist
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 * @throws TestScriptException
	 */
	public Object multiSelectSelectAll(By by) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that all the values of the multi select picklist is selected";
		testStep[1] = "All the values of the multi select picklist should be selected";
		testStep[2] = "All the values of the multi select picklist are selected";
		testStep[4] = "";
		String status = "Pass";

		try {
			WebElement MultiSelectElement = findElement(by);
			Select MultiSelectSelectionBox = new Select(MultiSelectElement);
			MultiSelectSelectionBox.deselectAll();
			List<WebElement> MultiSelectItemsList = MultiSelectSelectionBox.getOptions();
			for (WebElement CurrentItem : MultiSelectItemsList) {
				MultiSelectSelectionBox.selectByVisibleText(CurrentItem.getText());
			}
		} catch (Exception e) {
			testStep[2] = "Cannnot select all the values of the multi select picklist. Please refer Logs/info.log for more details.";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			status = "Fail";
			methodFailureMessage = testStep[2] + e.getMessage();
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return true;
	}

	/**
	 * Select the radio button based on the mentioned value
	 * 
	 * @param name
	 *            Name of the element
	 * @param value
	 *            Value to be selected
	 * @throws TestScriptException
	 */
	protected void selectRadioButton(String name, String value) throws TestScriptException {
		try {
			By webEle = getLocator(name);
			List<WebElement> RadioButtons = findElements(webEle);
			for (WebElement RadioButton : RadioButtons) {
				String onScreenValue = RadioButton.getAttribute("value");
				if (onScreenValue.equals(value)) {
					RadioButton.click();
					break;
				}
			}
		} catch (Exception e) {
			methodFailureMessage = "Select RadioButton method failed because of exception " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage);
		}
	}

	/**
	 * Waits explicitly for 6 second
	 * 
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 */
	public Object waitExplicit() throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that process is on wait for 6 seconds";
		testStep[1] = "Process should be Waiting for 6 seconds";
		testStep[2] = "Process waited for 6 seconds";
		testStep[4] = "";
		String status = "Pass";

		try {
			Utility.sleep(6000);
		} catch (Exception e) {
			testStep[2] = "Unable to wait because of an Exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			methodFailureMessage = testStep[2] + e.getMessage();
			config.getLogger().error(methodFailureMessage);
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			throw new TestScriptException(methodFailureMessage, testStep);
		}

		return true;

	}

	/**
	 * Waits explicitly for the mentioned milliseconds
	 * 
	 * @param ms
	 *            - Milliseconds to wait
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 */
	public Object waitExplicit(Integer ms) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that process is on wait for " + ms + " seconds";
		testStep[1] = "Process should be Waiting for " + ms + " seconds";
		testStep[2] = "Process waited for " + ms + " seconds";
		testStep[4] = "";
		String status = "Pass";

		try {
			Utility.sleep(ms);
		} catch (Exception e) {
			testStep[2] = "Unable to wait because of an Exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			methodFailureMessage = testStep[2] + e.getMessage();
			config.getLogger().error(methodFailureMessage);
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return true;

	}

	/**
	 * Clear the existing text for any webelement
	 * 
	 * @param name
	 *            - The field that needs to be cleared
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 * @throws TestScriptException
	 */
	public Object clearText(String name) throws TestScriptException {

		testStep = getStepInstance();
		testStep[0] = "Verify that text is cleared on " + name;
		testStep[1] = "Text should be cleared on " + name;
		testStep[2] = "Element cleared ";
		String status = "Pass";

		try {
			By webEle = getLocator(name);
			WebElement webElement = findElement(webEle);
			webElement.clear();
			config.getLogger().info("Element cleared " + name);
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
		} catch (Exception e) {
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			testStep[2] = "Element is not cleared : " + name + " because of an Exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			methodFailureMessage = testStep[2] + e.getMessage();
			config.getLogger().error(methodFailureMessage);
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return true;
	}

	/**
	 * This method is used for scrolling down.
	 * 
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 * 
	 */
	public Object scrollDown() throws TestScriptException {

		testStep = getStepInstance();
		testStep[0] = "Verify that window is scrolled down";
		testStep[1] = "Window should be scrolled down";
		testStep[2] = "Scroll down performed";
		testStep[4] = "";
		String status = "Pass";

		try {
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript(
					"window.scrollTo(0,Math.max(document.documentElement.scrollHeight,document.body.scrollHeight,document.documentElement.clientHeight));");
		} catch (Exception e) {
			testStep[2] = "Unable to scroll down because of an Exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			methodFailureMessage = testStep[2] + e.getMessage();
			config.getLogger().error(methodFailureMessage);
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return true;

	}

	/**
	 * This method is used for scrolling up.
	 * 
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 * 
	 */
	public Object scrollUp() throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that window is scrolled up";
		testStep[1] = "Window should be scrolled up";
		testStep[2] = "Scroll up performed";
		testStep[4] = "";
		String status = "Pass";

		try {
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript(
					"window.scrollTo(Math.max(document.documentElement.scrollHeight,document.body.scrollHeight,document.documentElement.clientHeight),0);");
		} catch (Exception e) {
			testStep[2] = "Unable to scroll up because of an Exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			methodFailureMessage = testStep[2] + e.getMessage();
			config.getLogger().error(methodFailureMessage);
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return true;

	}

	/**
	 * Verify that the mentioned web element is absent on the screen.
	 * 
	 * @param name
	 *            Element whose absence we need to verify.
	 * @return String[] 
	 * 			  Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyAbsence(String name) throws TestScriptException {

		testStep = getStepInstance();
		testStep[0] = "Verify that the element " + name + " is not present on the page";
		testStep[1] = "Element " + name + " should not be there on the page";
		testStep[2] = "Element is not there on the page";
		testStep[4] = "";
		String status = "Pass";

		try {
			By webEle = getLocator(name);
			WebElement webElement = driver.findElement(webEle);

			if (webElement != null) {
				status = "Fail";
				testStep[2] = "Element " + name + " is there on the page";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				config.getLogger().error("Element " + name + " is there on the page");
				methodFailureMessage = testStep[2];
			}

		} catch (NoSuchElementException e) {
			if (testStep[4].isEmpty()) {
				testStep[2] = "Element " + name + " is not there on the page";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			}
		} catch (Exception e) {
			if (testStep[4].isEmpty()) {
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				testStep[2] = "Element could not be verified because of an Exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				methodFailureMessage = testStep[2] + e.getMessage();
			}

		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/**
	 * Save the specified text in the mentioned variable after performing some
	 * manipulation as per the specified type
	 * 
	 * @param text
	 *            Text that needs to be saved.
	 * @param variable
	 *            Variable that is used for saving the text.
	 * @param type
	 *            Type of the value that we need to save.
	 *            Supported values are percentage,formula,currency,formulainteger
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 */
	 public Object saveText(String text, String variable, String type) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that " + text + " is Saved in " + variable;
		testStep[1] = text + " should get saved in " + variable;
		testStep[2] = text + " is saved in " + variable;
		testStep[4] = "";
		String status = "Pass";

		try {
			switch (type.toLowerCase()) {
			case "percentage":
				Double percentage = Double.parseDouble(text);
				percentage = percentage / 100;
				text = String.format("%.2f", percentage);
				break;
			case "formula":
				String[] express = text.split(",");
				String precision;
				if (express.length > 1)
					precision = express[1];
				else
					precision = "2";
				evaluateExpression(express[0], precision, text);
				break;
			case "currency":
				text = text.replaceAll("[^\\d.]", "");
				break;
			case "formulainteger":
				String[] expressInt = text.split(",");
				String precisionInt = "0";
				evaluateExpression(expressInt[0], precisionInt, text);
				text = Integer.parseInt(text) + "";
				break;
			}
			config.setMapGlobalVariable(variable, text);
		} catch (Exception e) {
			config.getLogger().error("Text " + text + "cannot be saved");
			testStep[2] = text + " is not saved in " + variable + "because of an Exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			methodFailureMessage = testStep[2] + e.getMessage();
			config.getLogger().error(methodFailureMessage);
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return true;
	}

	/**
	 * Double click on element
	 * 
	 * @param name
	 *            Element to be double clicked on.
	 * @param type
	 * 			  Can have any value
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 * @throws TestScriptException
	 */
	public Object doubleClick(String name, String type) throws TestScriptException {

		testStep = getStepInstance();
		testStep[0] = "Verify that " + name + " is double clicked";
		testStep[1] = name + " should be double clicked";
		testStep[2] = name + " is double clicked";
		testStep[4] = "";
		String status = "Pass";

		try {
			Actions action = new Actions(driver);
			By webEle = getLocator(name);
			WebElement element = driver.findElement(webEle);
			action.doubleClick(element).perform();
		} catch (Exception e) {
			testStep[2] = "Unable to double click " + name + " because of an Exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			methodFailureMessage = testStep[2] + e.getMessage();
			config.getLogger().error(methodFailureMessage);
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return true;

	}

	/**
	 *  Mouse hover on any element
	 * 
	 * @param name
	 *            Element to be hovered on
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 * @throws TestScriptException
	 */
	public Object mouseHover(String name) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that " + name + " is Hovered";
		testStep[1] = name + " should be Hovered";
		testStep[2] = name + " is Hovered";
		testStep[4] = "";
		String status = "Pass";
		try {
			Actions action = new Actions(driver);
			By webEle = getLocator(name);
			WebElement element = driver.findElement(webEle);
			action.moveToElement(element).build().perform();
		} catch (Exception e) {
			testStep[2] = "Unable to hover " + name + " because of an Exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			methodFailureMessage = testStep[2] + e.getMessage();
			config.getLogger().error(methodFailureMessage);
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return true;
	}

	/**
	 * Wait for the element until it can be used.
	 * 
	 * @param By
	 * 		  By locator for the element
	 * @throws TestScriptException
	 */
	protected void waitForElementToBeUsed(By by) throws TestScriptException {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.elementToBeClickable(by));
		} catch (Exception e) {
			methodFailureMessage = "WaitForElementToBeUsed method failed because of exception " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage);
		}
	}

	/**
	 * Fluent wait for element to be visible
	 * 
	 * @param By
	 * 		  By locator for the element
	 * @throws TestScriptException
	 */
	protected void waitForElementToBeAvailable(By by) throws TestScriptException {
		// Wait first to list appear..
		try {
			Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(60, TimeUnit.SECONDS)
					.pollingEvery(5, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);
			wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		} catch (Exception e) {
			methodFailureMessage = "WaitForElementToBeAvailable method failed because of exception " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage);
		}
	}

	/**
	 * Fluent wait for the Element to be clickable
	 * 
	 * @param By
	 * 		  By locator for the element
	 * @throws TestScriptException
	 */
	protected void waitForElementToBeClickable(By by) throws TestScriptException {
		try {
			// Wait first to list appear..
			Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(60, TimeUnit.SECONDS)
					.pollingEvery(5, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);
			WebElement welement = wait.until(ExpectedConditions.elementToBeClickable(by));
			// System.out.println("Buttone Text: "+welement.getText());
		} catch (Exception e) {
			methodFailureMessage = "WaitForElementToBeClickable method failed because of exception " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage);
		}
	}

	/**
	 * Wait for the specified element to be Invisible
	 * 
	 * @param name
	 *            Element for which we need to wait for it to be invisible
	 * @return String[] 
	 * 			  Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] waitForElementToBeInvisible(String name) throws TestScriptException {

		testStep = getStepInstance();
		testStep[0] = "Verify that process is on wait for element : " + name + " to get invisible";
		testStep[1] = "Process should Waiting for element : " + name + " to get invisible";
		testStep[2] = "Waited for element : " + name + " to get invisible";
		testStep[4] = "";
		String status = "Pass";

		try {
			By element = getLocator(name);
			// Wait first to list appear..
			Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(60, TimeUnit.SECONDS).pollingEvery(5,
					TimeUnit.SECONDS);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(element));
			// testStep[4] = ScreenShot.takeBrowserSnapShot(driver,
			// config.getSnapshotFolder());
		} catch (Exception e) {
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			testStep[2] = "Unable to wait because of an Exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			methodFailureMessage = testStep[2] + " " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/**
	 * Fluent wait for element to be visible
	 * 
	 * @param timeoutInSec
	 * 			Time for which application should poll for the element
	 * @param name
	 * 			Element for which application should wait
	 * @return String[] 
	 * 			Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] waitForElementToBeVisible(Integer timeoutInSec, String name) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Waiting for the element " + name + " to be visible";
		testStep[1] = "Waiting for the element to be visible";
		testStep[2] = "Element is visible on the page";
		testStep[4] = null;
		String status = "Pass";
		// Wait first to list appear..
	
		boolean screenshot = false;
		try {
			Pattern pattern = Pattern.compile("screenshot\\((.+?)\\)",Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(name);
            if(matcher.find()){
            	name = matcher.group(1);
            	screenshot = true;
            }
        	final By element = getLocator(name);
			
			Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(timeoutInSec, TimeUnit.SECONDS)
					.pollingEvery(5, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);

			WebElement foo = wait.until(new Function<WebDriver, WebElement>() {
				public WebElement apply(WebDriver driver) {

					return driver.findElement(element);
				}
			});
			
			if(screenshot){
				mouseHover(name);
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			}
			
		} catch (TimeoutException e) {
			testStep[2] = "Element is still not visible even after waiting";
			status = "Fail";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			methodFailureMessage = testStep[2] + " " + e.getMessage();

		} catch (Exception e) {
			if(testStep[4]==null){
				testStep[2] = "Element " + name + " visibility could not be verified because of the some exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}
	
	/**
	 * Saves the specified element text in the mentioned variable
	 * 
	 * @param element
	 *            Element whose text needs to be saved
	 * @param variable
	 *            Variable in which we need to save the element text
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 * @throws TestScriptException
	 */
	public Object saveElementData(String element, String variable) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Save element " + element + " into the variable " + variable;
		testStep[1] = element + " text should be saved into the variable " + variable;
		testStep[2] = element + " text is saved into the variable " + variable;
		testStep[4] = "";
		String status = "Pass";
		try {
			By webEle = getLocator(element);
			WebElement webElement = findElement(webEle);
			String value = webElement.getText();

			if (element.contains("Created Date") || element.contains("Modified Date")) {
				value = value.substring(value.lastIndexOf(","));
				value = value.substring(2, 12);
			}
			config.setMapGlobalVariable(variable, value);
		} catch (Exception e) {
			testStep[2] = element + " could not be saved because of an Exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			methodFailureMessage = testStep[2] + " " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return true;
	}

	/**
	 * Verify that the mentioned element is not blank
	 * 
	 * @param element
	 *            Element whose value needs to be verified
	 * @return String[] 
	 * 			  Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyElementNotBlank(String element) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that the " + element + " is not blank ";
		testStep[1] = element + " should not be blank";
		testStep[2] = element + " is not blank";
		testStep[4] = "";
		String status = "Pass";
		try {
			By webEle = getLocator(element);
			WebElement webElement = findElement(webEle);
			mouseHover(element);
			String screenText = webElement.getText().trim();
			if (screenText == null || screenText.equalsIgnoreCase("")) {
				testStep[2] = element + " is blank instead of having some value.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2];
			} else {
				testStep[2] = element + " is not blank as expected.";
				testStep[4] = "";
			}
		} catch (Exception e) {
			if (testStep[4].isEmpty()) {
				testStep[2] = "Element could not be verified because of an Exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2] + " " + e.getMessage();

			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}

		return testStep;
	}

	/**
	 * Verifies that the default picklist value for the mentioned element is same as
	 * the value of the specified variable
	 * 
	 * @param element
	 *            Picklist whose default value we need to verify
	 * @param variable
	 *            Variable storing the expected default value
	 * @param index
	 *            Index at which the expected value is there in the set of the
	 *            values mentioned in the variable, in case variable has
	 *            multiple values separated by comma.
	 * @return String[] 
	 * 			  Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyDefaultPicklistValue(String element, String variable, String index)
			throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify the default selected value for the picklist " + element + ".";
		testStep[1] = "Default selected value for the picklist " + element + " should be as expected.";
		testStep[2] = "Default selected value for the picklist " + element + " is as expected.";
		testStep[4] = "";
		String status = "Pass";
		String expectedValue = "";
		String actualValue = "";
		try {
			storePicklistSelectedValue(element, element);
			mouseHover(element);
			if (variable.contains("#")) {
				expectedValue = config.getGlobalVariable(variable.substring(1));
				if (expectedValue.contains(",")) {
					String[] temp = expectedValue.split(",");
					expectedValue = temp[Integer.parseInt(index) - 1];
				}
			} else {
				expectedValue = variable;
			}
			actualValue = config.getGlobalVariable(element);

			testStep[1] = "Default selected value for the picklist " + element + " should be " + expectedValue + ".";

			if (expectedValue.equalsIgnoreCase(actualValue)) {
				testStep[2] = "Default selected value for the picklist " + element + " is " + expectedValue
						+ " which is as expected.";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			} else {
				testStep[2] = "Default selected value for the picklist " + element + " is " + actualValue
						+ " instead of " + expectedValue;
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				status = "Fail";
				methodFailureMessage = testStep[2];
			}
		} catch (Exception e) {
			if (testStep[4].isEmpty()) {
				testStep[2] = "Default value for picklist " + element + " cannot be verified because of an Exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/**
	 * Waits for the element to be Invisible
	 * 
	 * @param name
	 *            Element for which we need to wait for invisibility
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 * @throws TestScriptException
	 */
	public Object waitForElementInVisible(String name) throws TestScriptException {
		// Wait first to list appear..
		final By element = getLocator(name);
		testStep = getStepInstance();
		testStep[0] = "Verify that process is on wait for element : " + name + " to be invisible";
		testStep[1] = "Process should wait for element : " + name + " to be invisible";
		testStep[2] = "Waited for element : " + name + " to be invisible";
		testStep[4] = "";
		String status = "Pass";
		try {
			Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(100, TimeUnit.SECONDS)
					.pollingEvery(5, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);

			wait.until(ExpectedConditions.invisibilityOfElementLocated(element));
		} catch (Exception e) {
			testStep[2] = "Unable to wait because of an Exception. Please refer Logs/info.log for more details.";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			status = "Fail";
			methodFailureMessage = testStep[2] + " " + e.getMessage();

		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;

	}

	/**
	 * Waits for the mentioned element to have the specified value for the
	 * specified attribute
	 * 
	 * @param name
	 *            Element for which attribute needs to be checked
	 * @param attribute
	 *            Attribute whose value needs to be checked
	 * @param value
	 *            Expected Value for Attribute
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 * @throws TestScriptException
	 */
	public Object waitForElementToHaveAttributeValue(String name, String attribute, String value)
			throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that process is on wait for attribute : " + attribute + " of element :" + name +" to have the mentioned value.";
		testStep[1] = "Process should wait for attribute : " + attribute + " of element :" + name+" to have the mentioned value.";
		testStep[2] = "Waited for attribute : " + attribute + " on element :" + name+" to have the mentioned value.";
		testStep[4] = "";
		String status = "Pass";

		try {
			final By element = getLocator(name);

			Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(180, TimeUnit.SECONDS)
					.pollingEvery(5, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);
			wait.until(ExpectedConditions.visibilityOfElementLocated(element));

			WebElement wElement = driver.findElement(element);
			long startTime = System.currentTimeMillis();

			while (true) {
				long endTime = System.currentTimeMillis();
				long elapsedMilliSeconds = endTime - startTime;
				double elapsedSeconds = elapsedMilliSeconds / 1000.0;
				String screenValue = wElement.getAttribute(attribute);
				if (screenValue.equalsIgnoreCase(value) || screenValue.contains(value)) {
					config.getLogger().info("Attribute " + attribute + " has value " + value);
					break;
				} else {
					if (elapsedSeconds == 60.0) {
						status = "Fail";
						testStep[2] = "Timeout after waiting for element's atrribute to have the required value.";
						testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						methodFailureMessage = testStep[2];
						break;
					}
					continue;
				}
			}

		} catch (Exception e) {
			if (testStep[4].isEmpty()) {
				testStep[2] = "Unable to wait because of an Exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				methodFailureMessage = testStep[2] + " " + e.getMessage();

			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return true;

	}

	/**
	 * Waits for the mentioned elements to have the mentioned value for the
	 * specified attribute
	 * 
	 * @param name
	 *            Element for which attribute needs to be checked
	 * @param attribute
	 *            Attribute whose value needs to be checked
	 * @param value
	 *            Expected Value for Attribute
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 * @throws TestScriptException
	 */
	public Object waitForElementsToHaveAttributeValue(String name, String attribute, String value)
			throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that process is on wait for attribute : " + attribute + " on elements" + name +" to have the mentioned value.";
		testStep[1] = "Process should wait for attribute : " + attribute + " on elements :" + name +" to have the mentioned value.";
		testStep[2] = "Waited for attribute : " + attribute + " on elements :" + name +" to have the mentioned value.";
		testStep[4] = "";
		String status = "Pass";
		try {
			final By element = getLocator(name);
			Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(180, TimeUnit.SECONDS)
					.pollingEvery(5, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);
			wait.until(ExpectedConditions.visibilityOfElementLocated(element));

			List<WebElement> wElements = findElements(element);

			waitForPageLoaded(driver);

			for (WebElement wElement : wElements) {
				String screenValue = wElement.getAttribute(attribute);

				if (screenValue.equalsIgnoreCase(value) || screenValue.contains(value)) {
					config.getLogger().info("Attribute " + attribute + " has value " + value);
				} else {
					status = "Fail";
					break;
				}
			}

			if (status.equalsIgnoreCase("Fail")) {
				config.getLogger().info(
						"Element " + name + " don't have required attribute value even after waiting for page load.");
				testStep[2] = "Timeout after waiting for element's atrribute to have the required value.";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2];
			}
		} catch (Exception e) {
			if (testStep[4].isEmpty()) {
				testStep[2] = "Unable to wait because of an Exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				methodFailureMessage = testStep[2] + " " + e.getMessage();

			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return true;
	}

	/**
	 * Wait for the page to have the required url
	 * 
	 * @param url
	 *            Expected url of the page
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 * @throws TestScriptException
	 */
	public Object waitForPageToHaveUrl(String url) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that the page URL is as expected";
		testStep[1] = "Page URL should be " + url + ".";
		testStep[2] = "Page URL is having the " + url + ".";
		testStep[4] = "";
		String status = "Pass";
		String pageURL;
		try {
			long startTime = System.currentTimeMillis();

			while (true) {
				pageURL = driver.getCurrentUrl();
				long endTime = System.currentTimeMillis();
				long elapsedMilliSeconds = endTime - startTime;
				double elapsedSeconds = elapsedMilliSeconds / 1000.0;
				if (pageURL.equals(url)) {
					testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					testStep[2] = "Page URL is as expected";
					config.getLogger().info("Page URL is as expected");
					break;
				} else {
					if (elapsedSeconds == 60.0) {
						testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						testStep[2] = "Page URL is " + pageURL + " which is not as expected";
						status = "Fail";
						methodFailureMessage = testStep[2];
						config.getLogger().error("Timeout after waiting for the page url to have the expected value");
						break;
					}
					continue;
				}
			}
		} catch (Exception e) {
			if (testStep[4].isEmpty()) {
				testStep[2] = "Page URL cannot be verified because of some exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2] + " " + e.getMessage();
				config.getLogger().error(methodFailureMessage);
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return true;
	}

	/**
	 * Waits for the mentioned element to have the required attribute
	 * 
	 * @param name
	 *            Element for which we need to wait for attribute presence
	 * @param attribute
	 *            Expected attribute for the element
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 * @throws TestScriptException
	 */
	public Object waitForElementToHaveAttribute(String name, String attribute) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that application is waiting for the element to have the specified attribute";
		testStep[1] = "Element should be the specified attribute.";
		testStep[2] = "Element is having the specified attribute.";
		testStep[4] = "";
		String status = "Pass";
		try {
			final By element = getLocator(name);

			Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(180, TimeUnit.SECONDS)
					.pollingEvery(5, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);
			wait.until(ExpectedConditions.visibilityOfElementLocated(element));

			WebElement wElement = driver.findElement(element);
			String screenValue = "";
			long startTime = System.currentTimeMillis();

			while (true) {
				long endTime = System.currentTimeMillis();
				long elapsedMilliSeconds = endTime - startTime;
				double elapsedSeconds = elapsedMilliSeconds / 1000.0;
				screenValue = wElement.getAttribute(attribute);
				if (screenValue != null) {
					config.getLogger().info("Attribute " + attribute + " is present");
					break;
				} else {
					if (elapsedSeconds == 60.0) {
						status = "Fail";
						testStep[2] = "Timeout after waiting for element to have the specified atrribute.";
						testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
						methodFailureMessage = testStep[2];
						break;
					}
					continue;
				}
			}
		} catch (Exception e) {
			if (testStep[4].isEmpty()) {
				testStep[2] = "Element cannot be verified for the specified attribute because of some exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2] + " " + e.getMessage();
				config.getLogger().error(methodFailureMessage);
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return true;

	}

	/**
	 * Selects the specified file from the mentioned file path
	 * 
	 * @param filePath
	 *            Path where the file is located
	 * @param fileName
	 *            File to be selected
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 * @throws Exception
	 */
	public Object selectFileFromWindows(String filePath, String fileName) throws Exception {
		testStep = getStepInstance();
		testStep[0] = "Verify that the process should select the file : " + fileName;
		testStep[1] = "Process should select the file : " + fileName;
		testStep[2] = fileName + " is selected";
		testStep[4] = "";
		String status = "Pass";
		try {
			filePath = filePath.concat("\\" + fileName);
			StringSelection ss = new StringSelection(filePath);
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);

			// imitate mouse events like ENTER, CTRL+C, CTRL+V
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
		} catch (Exception e) {
			testStep[2] = "Unable to select : " + fileName + " because of an Exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			methodFailureMessage = testStep[2] + " " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return true;
	}

	/**
	 * Verifies that whether the specified file is present or not at the
	 * downloaded location
	 * 
	 * @param downloadedPath
	 *            Path where the file was downloaded
	 * @param fileName
	 *            Name of the downloaded file
	 * @return String[] 
	 * 			  Returns the result of the method execution
	 * @throws TestScriptException
	 */
	public String[] isFileDownloaded(String downloadedPath, String fileName) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that the file : " + fileName + " is downloaded";
		testStep[1] = "File : " + fileName + " should be downloaded";
		testStep[2] = "File : " + fileName + " is downloaded";
		testStep[4] = null;
		String status = "Pass";
		try {				
			File theNewestFile = null;
		    File dir = new File(downloadedPath);
		    File[] files = dir.listFiles();
		    		
		    if (files.length > 0) {
		        /** The newest file comes first **/
		        Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
		        theNewestFile = files[0];
		    } 
	        if(!theNewestFile.getName().contains(fileName)){
	        testStep[2] = fileName + " is not downloaded at the specified download location";
			testStep[4] = "";
			status = "fail";
			methodFailureMessage = testStep[2];
   		    }
		} catch (Exception e) {
			if (testStep[4]==null) {
				testStep[2] = fileName + " is not downloaded because of an Exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;

	}

	/**
	 * This method stores the currentTimeStamp in the specified variable.
	 * 
	 * @param variable
	 *            Variable used for storing the current timeStamp
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 * @throws TestScriptException
	 * 
	 */
	public Object currentTimeStamp(String variable) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that the currentTimeStamp is stored in the specified variable";
		testStep[1] = "CurrentTimeStamp should be stored in the specified variable";
		testStep[2] = "CurrentTimeStamp is stored in the specified variable";
		testStep[4] = "";
		String status = "Pass";

		try {
			String currentTimeStamp = new SimpleDateFormat("ddMMyyHHmmss").format(new Date());
			config.setMapGlobalVariable(variable, currentTimeStamp);
		} catch (Exception e) {
			testStep[2] = "CurrentTimeStamp is not stored in the specified variable";
			status = "Fail";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			methodFailureMessage = testStep[2] + " " + e.getMessage();
			config.getLogger().error(methodFailureMessage);

		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return true;
	}

	/**
	 * Evaluates the specified operation on 2 mentioned operands and stores the
	 * result in the specified variable
	 * 
	 * @param operator
	 *            Mathematical operation that needs to be performed.
	 *            Supported values are IntegerAdd,IntegerSubtract,IntegerMultiply,IntegerDivide,
	 *            DoubleAdd,DoubleSubtract,DoubleMultiply,DoubleDivide,
	 * @param operand1
	 *            Operand 1 for the performing mathematical operation.
	 * @param operand2
	 *            Operand 2 for the performing mathematical operation.
	 * @param variable
	 *            Variable for storing the result for the performed mathematical
	 *            operation
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 * @throws TestScriptException
	 */
	public Object mathematicalOperation(String operator, String operand1, String operand2, String variable)
			throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that the mathematical Operation is completed.";
		testStep[1] = "Mathematical Operation should be completed";
		testStep[2] = "Mathematical Operation is completed";
		testStep[4] = "";
		String status = "Pass";

		try {
			Boolean commaSeparatedResult = false;
			String operationResult = "";
			if (operand1.contains(",") || operand2.contains(",")) {
				commaSeparatedResult = true;
				if (operand1.contains(",")) {
					operand1 = operand1.replaceAll(",", "");
				}
				if (operand2.contains(",")) {
					operand2 = operand2.replaceAll(",", "");
				}
			}
			if (operator.contains("Integer")) {
				Integer argument1 = Integer.parseInt(operand1);
				Integer argument2 = Integer.parseInt(operand2);
				Integer result = 0;
				switch (operator) {
				case "IntegerAdd":
					result = argument1 + argument2;
					break;
				case "IntegerSubtract":
					result = argument1 - argument2;
					break;
				case "IntegerMultiply":
					result = argument1 * argument2;
					break;
				case "IntegerDivide":
					result = argument1 / argument2;
					break;
				default:
					status = "fail";
					testStep[2] = "Unsupported Integer operation. Please select appropriate operation.";
					testStep[4] = null;
					methodFailureMessage = testStep[2];
					throw new Exception();
				}
				if (commaSeparatedResult) {
					NumberFormat formatter = new DecimalFormat();
					operationResult = formatter.getIntegerInstance().format(result);
				} else {
					operationResult = result.toString();
				}
			} else if (operator.contains("Double")) {
				Double argument1 = Double.parseDouble(operand1);
				Double argument2 = Double.parseDouble(operand2);
				Double resultant = 0.0;
				switch (operator) {
				case "DoubleMultiply":
					resultant = argument1 * argument2;
					break;
				case "DoubleDivide":
					resultant = argument1 / argument2;
					break;
				case "DoubleAdd":
					resultant = argument1 + argument2;
					break;
				case "DoubleSubtract":
					resultant = argument1 - argument2;
					break;
				default:
					status = "fail";
					testStep[2] = "Unsupported Double operation. Please select appropriate operation.";
					testStep[4] = null;
					methodFailureMessage = testStep[2];
					throw new Exception();
				}
				if (commaSeparatedResult) {
					NumberFormat formatter = new DecimalFormat();
					formatter.setMaximumFractionDigits(2);
					operationResult = formatter.format(resultant);
				} else {
					DecimalFormat formatter = new DecimalFormat("##.##");
					operationResult = formatter.format(resultant);
				}
			} else {
				status = "fail";
				testStep[2] = "Unsupported operation for the method. Please select appropriate operation.";
				testStep[4] = null;
				methodFailureMessage = testStep[2];
				throw new Exception();
			}
			config.setMapGlobalVariable(variable, operationResult);
		} catch (Exception e) {
			if (testStep[4] != null) {
				testStep[2] = "Unable to perform mathematical Operation because of an Exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return true;
	}

	/**
	 * Evaluate the specified expression and returns the result as per the
	 * mentioned precision. If precision is 0 then it returns the exact result
	 * otherwise it returns the Double result formatted with the mentioned
	 * precision.
	 * 
	 * @param expression
	 *            Expression to be evaluated.
	 * @param precision
	 *            Precision by which result should be formatted.
	 * @param variable
	 *            Variable in which the evaluated expression is stored.
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 * @throws EvalError
	 * @throws TestScriptException
	 */
	public Object evaluateExpression(String expression, String precision, String variable)
			throws EvalError, TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that the evaluated operation is completed.";
		testStep[1] = "Expression should be evaluated";
		testStep[2] = "Expression is evaluated";
		testStep[4] = "";
		String status = "Pass";

		try {
			Interpreter interpreter = new Interpreter();
			int exactPrecision = Integer.parseInt(precision);
			String result = "";
			if (exactPrecision == 0) {
				result = ((Integer) interpreter.eval(expression)).toString();
			} else {
				Double resultDouble = ((Double) interpreter.eval(expression));
				result = String.format("%." + precision + "f", resultDouble);
			}
			config.setMapGlobalVariable(variable, result);

		} catch (EvalError e) {
			testStep[2] = "Unable to evaluate expression because of evalerror. Please refer Logs/info.log for more details.";
			status = "Fail";
			methodFailureMessage = testStep[2] + " " + e.getMessage();

		} catch (Exception e) {
			testStep[2] = "Unable to evaluate expression because of exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			methodFailureMessage = testStep[2] + " " + e.getMessage();

		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return true;
	}

	/**
	 * Verifies whether there is any element or not with the specified xpath
	 * 
	 * @param xpath
	 *            Xpath of the element that needs to be verified
	 * @return boolean 
	 * 			  Return whether the element with the mentioned xpath is
	 *            there or not
	 * @throws TestScriptException
	 */
	protected boolean isElementExists(String xpath) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that existence status for the element with specified xpath is saved.";
		testStep[1] = "Existence status for the element with specified xpath should be saved.";
		testStep[2] = "Existence status for the element with specified xpath is saved.";
		testStep[4] = "";
		String status = "Pass";
		try {
			List<WebElement> lsElement = driver.findElements(By.xpath(xpath));
			if (lsElement.size() > 0)
				return true;
		} catch (Exception e) {
			testStep[2] = "Unable to save element existence status because of exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			methodFailureMessage = testStep[2] + " " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return false;
	}

	/**
	 * Saves the status of whether the element with the mentioned Xpath is there
	 * or not in the specified variable
	 * 
	 * @param elementXpath
	 *            Xpath of the element that needs to be checked.
	 * @param variable
	 *            Variable in which the status of whether the element with the
	 *            mentioned Xpath is there or not.
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 * @throws TestScriptException
	 */
	public Object saveElementExistence(String elementXpath, String variable) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that " + elementXpath + " should be saved in variable : " + variable;
		testStep[1] = elementXpath + " should be saved in variable : " + variable;
		testStep[2] = elementXpath + " is saved in variable : " + variable;
		testStep[4] = "";
		String status = "Pass";

		try {
			String value = Boolean.toString(isElementExists(elementXpath));
			config.setMapGlobalVariable(variable, value);
		} catch (Exception e) {
			testStep[2] = "Unable to save " + elementXpath + " in variable : " + variable + " because of an Exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			methodFailureMessage = testStep[2] + " " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return true;
	}

	/**
	 * Evaluates the mentioned condition and verifies whether the result matches
	 * with the expected outcome
	 * 
	 * @param condition
	 *            Condition to be evaluated
	 * @param expectedOutcome
	 *            Expected Outcome of the condition to be evaluated
	 * @return String[] 
	 * 			  Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyCondition(String condition, String expectedOutcome) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that the outcome of the mentioned condition is as expected or not";
		testStep[1] = "Result of the verified condition " + condition + " should be " + expectedOutcome;
		testStep[2] = "Result of the verified condition " + condition + " is " + expectedOutcome;
		testStep[4] = "";
		String status = "Pass";

		try {
			Interpreter commandInterpreter = new Interpreter();
			boolean result = (Boolean) commandInterpreter.eval(condition + "? true:false;");
			boolean expectedResult = Boolean.parseBoolean(expectedOutcome);

			if (result == expectedResult) {
				testStep[2] = "Condition is as expected";
				testStep[4] = "";
			} else {
				testStep[2] = "Condition is not as expected";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				status = "Fail";
				methodFailureMessage = testStep[2];

			}
		} catch (EvalError e) {
			if (testStep[4].isEmpty()) {
				testStep[2] = "Error: VerifyCondition method is unable to parse specified condition. Please refer Logs/info.log for more details.";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				status = "Fail";
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} catch (Exception e) {
			if (testStep[4].isEmpty()) {
				testStep[2] = "Exception: VerifyCondition method is unable to parse specified condition. Please refer Logs/info.log for more details.";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				status = "Fail";
				methodFailureMessage = testStep[2] + " " + e.getMessage();
				}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}

		return testStep;
	}

	/**
	 * Method to verify that the datetime field on the page has the expected
	 * value in the provided format
	 * 
	 * @param format
	 *            Expected format for datetime
	 * @param value
	 *            Expected value for the datetime field
	 * @return String[] 
	 * 			  Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyDateTime(String format, String value) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that the mentioned date time is in provided format";
		String status = "Pass";
		testStep[1] = "Mentioned date time should be in provided format";
		testStep[2] = "Mentioned date time is in provided format";
		testStep[4] = null;

		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		    dateFormat.setLenient(false);
		    dateFormat.parse(value.trim());
		    testStep[2] = "Mentioned date time" + value + " is in provided format -" + format;
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
		} catch (Exception e) {
				testStep[2] = "Mentioned date time format cannot be verified because of some exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2] + " " + e.getMessage();
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/**
	 * Counts the number of the elements with the provided xpath and save the count
	 * in the mentioned variable
	 * 
	 * @param name
	 *            Name of the local object repository variable whose count needs
	 *            to be calculated
	 * @param variable
	 *            Variable in which count is stored
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 * @throws TestScriptException
	 */
	public Object countNumberOfElements(String name, String variable) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Count of the number of instances of the mentioned element should be calculated.";
		testStep[1] = "Count should be stored in the mentioned variable";
		testStep[2] = "Count is stored in the mentioned variable";
		testStep[4] = "";
		String status = "Pass";
		Integer count = 0;
		try {
			List<WebElement> listOfElementsReturnedByXpath = new ArrayList<WebElement>();
			List<WebElement> listDisplayedOnThePage = new ArrayList<WebElement>();
			By webEle = getLocator(name);
			listOfElementsReturnedByXpath = findElements(webEle);
			count = listOfElementsReturnedByXpath.size();
			for (int i = 0; i < count; i++) {
				if (listOfElementsReturnedByXpath.get(i).isDisplayed()) {
					listDisplayedOnThePage.add(listOfElementsReturnedByXpath.get(i));
				}
			}
			count = listDisplayedOnThePage.size();
			config.setMapGlobalVariable(variable, count.toString());
		} catch (Exception e) {
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			testStep[2] = "Element could not be verified because of an Exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			config.getLogger().error("Element not found " + name);
			throw new TestScriptException(e.getMessage());
		} finally {
			testStep[3] = status;
		}
		return true;
	}

	/**
	 * Clicks at the mentioned x & y offset from the specified element
	 * 
	 * @param element
	 *            Element from which the offset needs to be calculated.
	 * @param x
	 *            x axis offset
	 * @param y
	 *            y axis offset
	 * @return String[] 
	 * 			  Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] clickAt(String element, String x, String y) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that the element can be clicked at the mentioned x and y offset";
		String status = "Pass";
		testStep[1] = "Element should be clicked at the mentioned x and y offset";
		testStep[2] = "Element is clicked at the mentioned x and y offset";

		try {
			WebElement wElement = findElement(getLocator(element));
			Actions action = new Actions(driver);
			action.moveToElement(wElement, Integer.parseInt(x), Integer.parseInt(y)).click().build().perform();
		} catch (Exception e) {
			testStep[2] = "Element is not clicked at the mentioned x and y offset because of an exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			methodFailureMessage = testStep[2] + " " + e.getMessage();
			config.getLogger().error(methodFailureMessage);

		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/**
	 * Method to retrieve the mentioned attribute from the currentdatetime and
	 * stores it in the variable
	 * 
	 * @param parameter
	 *            Attribute to be extracted. Allowed values are Year, Month,
	 *            MonthValue, DayOfYear, DayOfMonth, DayOfWeek, Hour, Minute,
	 *            Second
	 * @param variable
	 *            Variable to store the retrieved attribute
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failuren
	 * @throws TestScriptException
	 */
	public Object retrieveCurrentDateTimeAttribute(String parameter, String variable) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Retrieve the mentioned date / time parameter from the current date time";
		String status = "Pass";
		testStep[1] = "Mentioned date / time parameter should be stored.";
		testStep[2] = "Mentioned date / time parameter is stored.";
		testStep[4] = "";
		try {
			LocalDateTime localDate = LocalDateTime.now();
			String result = "";
			switch (parameter) {
			case "Year":
				result = String.valueOf(localDate.getYear());
				break;
			case "Month":
				result = String.valueOf(localDate.getMonth());
				break;
			case "MonthValue":
				result = String.valueOf(localDate.getMonthValue());
				break;
			case "DayOfYear":
				result = String.valueOf(localDate.getDayOfYear());
				break;
			case "DayOfMonth":
				result = String.valueOf(localDate.getDayOfMonth());
				break;
			case "DayOfWeek":
				result = String.valueOf(localDate.getDayOfWeek());
				break;
			case "Hour":
				result = String.valueOf(localDate.getHour());
				break;
			case "Minute":
				result = String.valueOf(localDate.getMinute());
				break;
			case "Second":
				result = String.valueOf(localDate.getSecond());
				break;
			default:
				testStep[4] = null;
				status = "fail";
				testStep[2] = "Invalid input for the attribute to be extracted from the currentdatetime.";
				methodFailureMessage = testStep[2];
				break;
			}
			config.setMapGlobalVariable(variable, result);
		} catch (Exception e) {
			testStep[2] = "Mentioned date / time parameter should be stored because of an exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			methodFailureMessage = testStep[2] + " " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return true;
	}

	/**
	 * Verifies the mentioned css property of a webelement
	 * 
	 * @param objectElement
	 *            Name of the element
	 * @param property
	 *            Css property of element that needs to be verified
	 * @param value
	 *            Expected value of the property
	 * @return String[] 
	 * 			  Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyCssProperty(String objectElement, String property, String value) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that the element " + objectElement + " has the required value for attribute " + property;
		testStep[1] = "Attribute " + property + " should have value " + value;
		testStep[2] = "Attribute " + property + " have value " + value;
		testStep[4] = null;
		String status = "Pass";

		try {	
						
			By webEle = getLocator(objectElement);
			WebElement webElement = findElement(webEle);
			String ActualPropertyValue = webElement.getCssValue(property);
			
			if (ActualPropertyValue.equalsIgnoreCase(value)) {
				config.getLogger().info("Verified value of attribute " + property + " for element " + objectElement
						+ ". It is as expected " + value);
			} else {
				testStep[2] = "Attribute " + property + " don't have value " + value;
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2];
			}
		} catch (Exception e) {
			if (testStep[4]==null) {
				testStep[2] = "Element could not be verified because of an Exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/**
	 * This function verifies the sorted order of Data based on Date in
	 * Ascending / Descending Form
	 * 
	 * @param objectElement
	 *            Name of the element
	 * @param sortOrder
	 *            Asc - For verifying the data in ascending order 
	 *            Desc - For verifying the data in descending order
	 * @return String[] 
	 * 			  Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifySortingOnDate(String objectElement, String sortOrder) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that the Data has been sorted on  " + objectElement + "Date";
		testStep[1] = "Elements should be sorted";
		testStep[2] = "Elements is sorted";
		testStep[4] = "";
		String status = "Pass";
		try {
			List<WebElement> webElements = findElements(getLocator(objectElement));
			ArrayList<Date> ActualDate = new ArrayList<Date>();
			SimpleDateFormat d = new SimpleDateFormat("MM/dd/yyyy");
			for (WebElement we : webElements) {
				String Stringdate = we.getText().toString();
				ActualDate.add(d.parse(Stringdate));
			}

			ArrayList<Date> ExpectedDate = new ArrayList<Date>();
			for (Date ald : ActualDate) {
				ExpectedDate.add(ald);
			}
			Collections.sort(ExpectedDate);
			if (sortOrder.equalsIgnoreCase("Desc")) {
				Collections.reverse(ExpectedDate);
			}
			for (int i = 0; i < ActualDate.size(); i++) {
				if ((ActualDate.get(i)).equals((ExpectedDate).get(i)))
					continue;
				else {
					testStep[2] = "Dates are not in " + sortOrder + "ending order";
					status = "Fail";
					testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
					methodFailureMessage = testStep[2];
				}
			}
		} catch (Exception e) {
			if (testStep[4].isEmpty()) {
				testStep[2] = "Sorted date data not be verified because of an Exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}
	
	/**
	 * This function verifies the text pattern of a webElement
	 * 
	 * @param objectElement
	 *            Name of the element whose text pattern will be verified	 
	 * @param format
	 *            Format that needs to be verified for the text
	 * @return String[] 
	 * 			  Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] verifyTextPattern(String objectElement,String format) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that the element " + objectElement + " text has the format: "+format ;
		testStep[1] = "Element "+objectElement+" should have the format: "+format;
		testStep[2] = "Element "+objectElement+" has the expected format: "+format;
		testStep[4] = null;
		String status = "Pass";

		try {
			WebElement webElement = findElement(getLocator(objectElement));
			String text = webElement.getText().toString().trim();
			System.out.println("Result for text "+text+" and format "+format+ " is "+text.matches(format));
			if(text.matches(format)){
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());	
			}			
			else{
				testStep[2] = "Element's text is not matching with expected pattern";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2];
				throw new Exception();
			}
		} catch (Exception e) {
			if (testStep[4] == null) {
				testStep[2] = "Element could not be verified because of an Exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}
	
	
	/**
	 * This method is using Mouse to resize any element.
	 * 
	 * @param elementToResize
	 *            Element from which resizing should be done.
	 * @param x
	 *            x axis offset by which element should be resized
	 * @param y
	 *            y axis offset by which element should be resized
	 * @return String[] 
	 * 			  Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] resizeElement(String elementToResize, String x, String y) throws TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that the element can be resized for the mentioned x and y offset";
		String status = "Pass";
		testStep[1] = "Element should be resized for the mentioned x and y offset";
		testStep[2] = "Element is resized for the mentioned x and y offset";
		
		try {
			WebElement wElement = findElement(getLocator(elementToResize));
			Actions action = new Actions(driver);
			action.clickAndHold(wElement).moveByOffset(Integer.parseInt(x), Integer.parseInt(y)).release().build()
					.perform();
			waitExplicit(1000);
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			waitExplicit(2000);
		} catch (Exception e) {
			testStep[2] = "Element is not resized for the mentioned x and y offset because of an exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			methodFailureMessage = testStep[2] + " " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}
		
	/**
	 * Evaluate the specified expression and stores the result in the variable.
	 * 
	 * @param expression
	 *            Expression to be evaluated.
	 * @param variable
	 *            Variable in which the evaluated expression is stored.
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 * @throws EvalError
	 * @throws TestScriptException
	 */
	public Object evaluateStringExpression(String expression, String variable)
			throws EvalError, TestScriptException {
		String[] testStep = getStepInstance();
		testStep[0] = "Verify that the evaluate operation is completed.";
		testStep[1] = "Expression should be evaluated";
		testStep[2] = "Expression is evaluated";
		testStep[4] = "";
		String status = "Pass";
		try {
			Interpreter interpreter = new Interpreter();
			String result = (String)interpreter.eval(expression);
			config.setMapGlobalVariable(variable, result);

		} catch (EvalError e) {
			testStep[2] = "Unable to evaluate expression because of evalerror. Please refer Logs/info.log for more details.";
			status = "Fail";
			methodFailureMessage = testStep[2] + " " + e.getMessage();

		} catch (Exception e) {
			testStep[2] = "Unable to evaluate expression because of exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			methodFailureMessage = testStep[2] + " " + e.getMessage();

		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return true;
	}	

}