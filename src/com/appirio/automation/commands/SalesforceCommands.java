package com.appirio.automation.commands;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import com.appirio.automation.configuration.Configuration;
import com.appirio.automation.configuration.FieldDetail;
import com.appirio.automation.configuration.Record;
import com.appirio.automation.configuration.ShareableDataManager;
import com.appirio.automation.exception.TestScriptException;
import com.appirio.automation.result.ScreenShot;
import com.appirio.automation.utility.SalesforceAPIUtility;
import com.appirio.automation.utility.Utility;
import com.google.common.base.Function;
import com.sforce.soap.apex.ExecuteAnonymousResult;
import com.sforce.soap.apex.LogCategory;
import com.sforce.soap.apex.LogCategoryLevel;
import com.sforce.soap.apex.LogInfo;
import com.sforce.soap.apex.LogType;
import com.sforce.soap.apex.SoapConnection;
import com.sforce.ws.ConnectionException;
import com.sforce.ws.ConnectorConfig;

/**
 * This class is used to collate all the generic commands that can be utilized for Salesforce applications.
 *
 */
public class SalesforceCommands extends TestCommands {

	private static Configuration config;
	protected static HashMap<String, List<Record>> mapCreatedRecords = new HashMap<String, List<Record>>();
	protected static SalesforceAPIUtility sfUtility = new SalesforceAPIUtility();
	protected String[] testStep;

	public Configuration getSalesforceCommandsConfig() {
		return config;
	}

	public static void setSalesforceCommandsConfig(Configuration config) {
		SalesforceCommands.config = config;
		sfUtility.setSalesforceAPIUtilityConfig(config);
	}

	public static void setmapCreatedRecords(String object, List<Record> recordList) {
		mapCreatedRecords.put(object, recordList);
	}

	public static List<Record> getmapCreatedRecords(String object) {
		return mapCreatedRecords.get(object);
	}

	/**
	 * Selects the App form the picklist if it is not already selected
	 * 
	 * @param name
	 *            Name of the App
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 * @throws TestScriptException
	 */
	public Object selectApp(String name) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that mentioned app" + name + " is selected";
		testStep[1] = "Mentioned app should be selected";
		testStep[2] = "Mentioned app is selected";
		testStep[4] = "";
		String status = "Pass";

		try {
			WebElement webElement = findElement(By.id("tsidLabel"));
			if (!webElement.getText().equalsIgnoreCase(name)) {

				webElement.click();
				findElement(By.xpath("//div[@id='tsid-menuItems']/a[text()='" + name + "']")).click();
				Utility.sleep(30000);
			}
		} catch (Exception e) {
			testStep[2] = "Mentioned app is not selected because of an Exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			methodFailureMessage = testStep[2] + " " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return true;

	}

	/**
	 * Select the list element from the Service dropdown list
	 * 
	 * @param name
	 *            Name of the list element to be selected
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 * @throws TestScriptException
	 */
	public Object selectServiceDropdown(String name) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that mentioned list element is selected";
		testStep[1] = "Mentioned list element should be selected";
		testStep[2] = "Mentioned list element is selected";
		testStep[4] = "";
		String status = "Pass";

		try {
			findElement(By.xpath("//*[@id='navigatortab']/div[1]")).click();
			WebElement parent = findElement(getLocator(name));
			WebElement child = parent.findElement(By.xpath("./button"));
			Dimension parentDim = parent.getSize();
			Dimension childDim = child.getSize();
			int xCoord = childDim.getWidth() + ((parentDim.getWidth() - childDim.getWidth()) / 2);
			int yCoord = childDim.getHeight() + ((parentDim.getHeight() - childDim.getHeight()) / 2) - 1;
			Actions builder = new Actions(driver);

			builder.moveToElement(parent, xCoord, yCoord);
			builder.click();
			builder.build().perform();
		} catch (Exception e) {
			testStep[2] = "Mentioned list element is not selected because of an Exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			methodFailureMessage = testStep[2] + " " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return true;
	}

	/**
	 * Login to the SFDC
	 * 
	 * @param username
	 *            Username to enter with
	 * @param password
	 *            Password of the user
	 * @return String[] 
	 * 			  Returns the result of the execution
	 * @throws AssertionError
	 * @throws TestScriptException
	 */
	public String[] login(String username, String password) throws AssertionError, TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Login with " + username;
		testStep[1] = username + " should be able to login to application";
		testStep[2] = username + " is logged into the application";
		testStep[4] = "";
		String status = "Pass";
		WebElement login_page_ctrl = null;
		try {
			By usernameXpath = By.xpath("//*[text()='Username']//following::input[1]");
			login_page_ctrl = findElement(usernameXpath);
			login_page_ctrl.clear(); 
			login_page_ctrl.sendKeys(username);
			By passwordXpath = By.xpath("//*[text()='Password']//following::input[1]");
			login_page_ctrl = findElement(passwordXpath);
			login_page_ctrl.clear();
			login_page_ctrl.sendKeys(password);
			login_page_ctrl = findElement(By.name("Login"));
			login_page_ctrl.click();

			Utility.sleep(15000);
			String result = Boolean.toString(isElementExists("//*[text()='Register Your Mobile Phone']"));
			if (result.equalsIgnoreCase("true")) {
				login_page_ctrl = driver
						.findElement(By.xpath("//*[contains(text(),\"I Don't Want to Register My Phone\")]"));
				login_page_ctrl.click();
			}

			Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(60, TimeUnit.SECONDS)
					.pollingEvery(5, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);

			wait.until(new Function<WebDriver, WebElement>() {
				public WebElement apply(WebDriver driver) {

					return driver.findElement(By.id("tabContainer"));
				}
			});

			// Utility.sleep(10000);

		} catch (Exception e) {
			testStep[2] = "User cannot login to application because of exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			methodFailureMessage = testStep[2] + " " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			throw new TestScriptException(methodFailureMessage, testStep);
		}

		return testStep;
	}

	/**
	 * This method does the global search for record name and clicks on the
	 * search element under the searched related list for the mentioned
	 * objectName Cannot be used for Objects who don't render to Detail Page
	 * 
	 * @param objectName
	 *            Name of the object to be searched
	 * @param recordName
	 *            Name of the record to be searched
	 * @return String[] 
	 * 			  Returns the result of the execution
	 * @throws TestScriptException
	 */

	public String[] globalSearchRecord(String objectName, String recordName) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that the mentioned detail page of the global searched record is displayed";
		testStep[1] = "Mentioned detail page of the global searched record should be displayed";
		testStep[2] = "Mentioned detail page of the global searched record is displayed";
		testStep[4] = "";
		String status = "Pass";
		WebElement wElement;
		boolean flag = false;
		
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(60, TimeUnit.SECONDS)
				.pollingEvery(5, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);
		
		try {
			wElement = driver.findElement(By.id("phSearchInput"));
			wElement.sendKeys(Keys.CONTROL + "a");
			wElement.sendKeys(Keys.DELETE);
			wElement.clear();
			wElement.sendKeys(recordName);
			wElement = driver.findElement(By.id("phSearchButton"));
			wElement.click();
			
			String xpath = "(//*[@class='searchFirstCell' and contains(text(),'" + objectName
					+ "')]//ancestor::*[@class='bRelatedList']//following::a[text()='" + recordName + "'])[1]";
			
			try {			
				wElement = driver.findElement(By.xpath(xpath));
				
				waitExplicit(4000);
				
				String result = Boolean.toString(isElementExists("//*[@id='helpBubble']"));
				if (result.equalsIgnoreCase("true")) {
					wElement = driver.findElement(By.id("helpBubbleCloseX"));
					wElement.click();
				}
				
				waitExplicit(2000);
				
				result = Boolean.toString(isElementExists("//*[text()=\"Find What You're Looking For\"]"));
				if (result.equalsIgnoreCase("true")) {
					wElement = driver.findElement(By.xpath("//*[text()='No Thanks']"));
					wElement.click();
				}
					
				saveCurrentURL("CurrentSearchedSetting");	
					
			}catch(NoSuchElementException e){
					flag = true;
					wElement = driver.findElement(By.id("searchMoreOptionsLink"));
					wElement.click();
					
					waitExplicit(4000);
					wElement = driver.findElement(By.id("moreOptionsExactPhrase"));
					wElement.click();
					
					waitExplicit(2000);
					wElement = driver.findElement(By.id("moreOptionsSearchBtn"));
					wElement.click();
					
					wait.until(new Function<WebDriver, WebElement>() {
						public WebElement apply(WebDriver driver) {

							return driver.findElement(By.id("searchMoreOptionsText"));
						}
					});			
				}	


			wElement = driver.findElement(By.xpath(xpath));
			wElement.click();
			
			wait.until(new Function<WebDriver, WebElement>() {
				public WebElement apply(WebDriver driver) {

					return driver.findElement(By.xpath("//*[@class='mainTitle' and contains(text(),'Detail')]"));
				}
			});
			
			saveCurrentURL("CurrentSearchedRecord");
			
		} catch (NoSuchElementException e) {
			status = "Fail";
			testStep[2] = "Global Search was failed because of the missing Element.";
			status = "Fail";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			methodFailureMessage = testStep[2] + " " + e.getMessage();
		} catch (TimeoutException e) {
			status = "Fail";
			testStep[2] = "Global Search was failed because the searched recod page was not opened.";
			status = "Fail";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			methodFailureMessage = testStep[2] + " " + e.getMessage();
		} catch (Exception e) {
			status = "fail";
			testStep[2] = "Global Search was failed because of some exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			methodFailureMessage = testStep[2] + " " + e.getMessage();
		} finally {
			testStep[3] = status;
			
			if(flag){
				String tempurl = config.getGlobalVariable("CurrentSearchedRecord");
				String settingurl = config.getGlobalVariable("CurrentSearchedSetting");
				
				if(tempurl.contains("006")){
					openUrl(settingurl);
				}
				
				wElement = driver.findElement(By.id("searchMoreOptionsLink"));
				wElement.click();
				
				waitExplicit(4000);
				wElement = driver.findElement(By.id("moreOptionsExactPhrase"));
				wElement.click();
				
				waitExplicit(2000);
				wElement = driver.findElement(By.id("moreOptionsSearchBtn"));
				wElement.click();	
				
				wait.until(new Function<WebDriver, WebElement>() {
					public WebElement apply(WebDriver driver) {

						return driver.findElement(By.id("searchMoreOptionsText"));
					}
				});
				
				if(tempurl.contains("006")){
					openUrl(tempurl);
				}
			}
		}
		if (status.equals("Fail")) {
			throw new TestScriptException(methodFailureMessage, testStep);
		}

		return testStep;
	}

	/**
	 * Logs out of SFDC
	 * 
	 * @return String[] 
	 * 			  Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] logOut() throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that user is logged out of the application";
		testStep[1] = "User should be logged out of the application";
		testStep[2] = "User is logged out of the application";
		testStep[4] = "";
		String status = "Pass";
		try {
			driver.switchTo().defaultContent();
			WebElement userNavArrow = findElement(By.id("userNav-arrow"));
			userNavArrow.click();
			List<WebElement> menuLinks = findChildElements(By.cssSelector("a.menuButtonMenuLink"),
					By.id("userNav-menuItems"));
			for (WebElement menuLink : menuLinks) {
				String title = menuLink.getAttribute("title");
				if (title.equalsIgnoreCase("Logout")) {
					menuLink.click();
					Utility.sleep();
				}
			}
			Utility.sleep();
		} catch (Exception e) {
			testStep[2] = "User cannot log out of the application because of an exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			methodFailureMessage = testStep[2] + " " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/**
	 * This method reads the data of the object whose record needs to be created
	 * 
	 * @param recValues
	 *            Name of the excel data sheet for the mentioned object
	 * @param objectName
	 *            Object for which record needs to be created
	 * @return List<Record>
	 *			  List of records data that needs to be created
	 * @throws TestScriptException 
	 */
	protected List<Record> getRecValues(String recValues, String objectName) throws TestScriptException {

		Workbook book = null;
		List<Record> recList = new ArrayList<Record>();
		try {
			FileInputStream inputStream = new FileInputStream(new File(recValues));
			book = WorkbookFactory.create(inputStream);
			/*
			 * if(recValues.endsWith("xlsx")){ book = new
			 * XSSFWorkbook(inputStream); }else if(recValues.endsWith("xls")){
			 * book = new HSSFWorkbook(inputStream); }
			 */
		} catch (FileNotFoundException e) {
			config.getLogger().error("Excel data sheet for the mentioned object name is not found");
			e.printStackTrace();
		} catch (IOException e) {
			config.getLogger().error("Excel data sheet for the mentioned object name cannot be read");
			e.printStackTrace();
		} catch (InvalidFormatException e) {
			config.getLogger().error("Excel data sheet for the mentioned object name is not in proper format");
			e.printStackTrace();
		}
		// Sheet sheet_summary = book.getSheet("Sheet1");

		try {
			Sheet sheet_summary = book.getSheetAt(0);
			int totalCols = sheet_summary.getRow(0).getLastCellNum();
			Iterator<Row> itrRow = sheet_summary.iterator();
			int totalRows = 0;

			String tempValue = "";
			List<String> cells = null;
			Row hRow = itrRow.next();
			while (itrRow.hasNext()) {
				Row row = itrRow.next();
				Record rec = new Record();
				cells = new ArrayList<String>();
				for (int col = 1; col < totalCols; col++) {
					Cell cell = row.getCell(col);
					if (cell != null) {
						switch (cell.getCellType()) {
						case Cell.CELL_TYPE_STRING:
							tempValue = cell.getStringCellValue();
							break;
						case Cell.CELL_TYPE_NUMERIC:
							tempValue = Double.toString(cell.getNumericCellValue());
							break;
						case Cell.CELL_TYPE_BOOLEAN:
							tempValue = Boolean.toString(cell.getBooleanCellValue());
							break;

						}

						// tempValue = cell.getStringCellValue();
					} else
						tempValue = "";

					String trimmed = tempValue.trim();

					String name = "";
					String value = "";
					String type = "";
					boolean notName = false;
					boolean notRecordType = false;
					Double number = 0.0;
					if (trimmed.length() > 0) {
						switch (hRow.getCell(col).getCellType()) {

						case Cell.CELL_TYPE_STRING:
							name = hRow.getCell(col).getStringCellValue();
							break;
						case Cell.CELL_TYPE_NUMERIC:
							name = Double.toString(hRow.getCell(col).getNumericCellValue());
							break;
						case Cell.CELL_TYPE_BOOLEAN:
							name = Boolean.toString(hRow.getCell(col).getBooleanCellValue());
							break;

						}

						switch (row.getCell(col).getCellType()) {

						case Cell.CELL_TYPE_STRING:
							value = row.getCell(col).getStringCellValue();
							break;
						case Cell.CELL_TYPE_NUMERIC:
							number = row.getCell(col).getNumericCellValue();
							value = Double.toString(number);
							break;
						case Cell.CELL_TYPE_BOOLEAN:
							value = Boolean.toString(row.getCell(col).getBooleanCellValue());
							break;
						}
						if(name.startsWith("!")){
							name = name.substring(1);
							if(name.contains("Record Type")){
								notRecordType = true;
							}
							else{
								notName = true;
							}
						}
						type = config.getMetadataMap().get(objectName).getTypeFromName(name);						

						if (type.equalsIgnoreCase("Integer")) {
							value = String.valueOf(number.intValue());
						}

						if (col == 1 && name.contains("Record Type")) {
							rec.setRecType(name);
							rec.setRecTypeValue(value);
							rec.setNotRecordType(notRecordType);
						} else {
							FieldDetail field = new FieldDetail();
							field.setName(name);
							field.setType(type);
							field.setValue(value);
							field.setNotName(notName);
							rec.add(field);
						}
					}
				}

				if (rec.getRecType() != null || !rec.getFieldList().isEmpty())
					recList.add(rec);
			}
			/*
			 * cells.add(trimmed);}
			 * 
			 * if(!cells.isEmpty()){ recList.add(rec);}
			 */
		} catch (Exception e) {
			methodFailureMessage = "Following error occured while reading data from the object data sheet " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage);			
		}
		return recList;
	}

	/**
	 * This method enters the data of the object whose record needs to be
	 * created in the application
	 * 
	 * @param recValues
	 *            Name of the excel data sheet for the mentioned object
	 * @param objectName
	 *            Object for which record needs to be created
	 */
	protected void enterValuesForRecCreation(Record record, String objectName) throws TestScriptException {
		try {
			List<FieldDetail> fields = record.getFieldList();
			String recType = record.getRecType();
			Boolean notRecordType = record.isNotRecordType();
			if (recType != null) {
				if(notRecordType){
					try{
						inputData(recType, "picklist", record.getRecTypeValue());
						findElement(By.xpath("//input[@value='Continue']")).click();
					}catch(Exception e){
						if(!e.getMessage().contains("Timed out after 60 seconds waiting")){
							throw new Exception();
						}
					}					
				}
				else{
					inputData(recType, "picklist", record.getRecTypeValue());
					findElement(By.xpath("//input[@value='Continue']")).click();
				}
			}	
			for (FieldDetail field : fields) {
				String name = field.getName();
				String type = field.getType();
				String value = field.getValue();
				boolean notName = field.isNotName();
				if (name != null){
					if(notName){
						try{	
							inputData(name, type, value);
						}catch(Exception e){
							if(!e.getMessage().contains("Timed out after 60 seconds waiting")){
								throw new Exception();
							}
							else{
							continue;
							}
						}						
					}
					else{
					inputData(name, type, value);
					}
				}
			}	
				Utility.sleep(5000);
		} catch (Exception e) {
			
			methodFailureMessage = "Following error occured while entering data for the object whose record needs to be created "
					+ e.getMessage();
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage);
		}

	}

	/**
	 * Returns the related list on the SFDC page
	 * 
	 * @param relatedListName
	 *            Complete name of the related list
	 * @return WebElement
	 * 			  WebElement pointing to the Related list
	 * @throws TestScriptException 
	 */
	protected WebElement getRelatedList(String relatedListName) throws TestScriptException {
		try {
			List<WebElement> relatedLists = findElements(By.xpath("//div[contains(@class,'bRelatedList')]"));
			for (WebElement relatedList : relatedLists) {
				WebElement title = relatedList.findElement(By.cssSelector("td.pbTitle"));
				String text = title.getText();
				if (text.equals(relatedListName))
					return relatedList;
			}
		} catch (Exception e) {
			methodFailureMessage = "Following error occured while finding element corresponding to the mentioned related list name"
							+ e.getMessage();
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage);					
		}
		return null;
	}

	/**
	 * Returns the related list on the SFDC page which contains relatedListName
	 * 
	 * @param relatedListName
	 *            Partial or complete name of the related list
	 * @return WebElement
	 * 			  WebElement pointing to the related list
	 * @throws TestScriptException 
	 */
	protected WebElement getRelatedListByPartialName(String relatedListName) throws TestScriptException {
		try {
			List<WebElement> relatedLists = findElements(By.xpath("//div[@class='bRelatedList']"));
			for (WebElement relatedList : relatedLists) {
				WebElement title = relatedList.findElement(By.cssSelector("td.pbTitle"));
				String text = title.getText();
				if (text.contains(relatedListName))
					return relatedList;
			}
		} catch (Exception e) {
			methodFailureMessage = "Following error occured while finding element corresponding to the mentioned partial related list name"
							+ e.getMessage();
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage);
		}
		return null;
	}

	/**
	 * Creates SFDC Records by going on the specified tab.
	 * 
	 * @param tabName
	 *            Name of the tab to be opened to create the specified record
	 * @param objectName
	 *            Name of the object, there should be a metadata file related to
	 *            this object
	 * @param recValues
	 *            Values for various fields on the record
	 * @param saveOption
	 *            Specify Save options as Save, if one records is to be created,
	 *            or Save & New if another record is to be created after this
	 *            one
	 * @return String[] 
	 * 			  Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] createRecord(String tabName, String objectName, String recValues, String saveOption)
			throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Create " + objectName + " record";
		testStep[1] = objectName + " record should be created";
		testStep[2] = objectName + " record is created";
		testStep[4] = "";
		String status = "Pass";
		try {
			List<Record> recordsList = getRecValues(recValues, objectName);
			for (Record record : recordsList) {

				clickTab(tabName);
				WebElement newBtn = findElement(By.xpath("//*[@type='button' and contains(@value,'New')]"));
				newBtn.click();

				enterValuesForRecCreation(record, objectName);
				String pageTitle = driver.getTitle();
				String pageTitleNew;

				String xpath;

				if (saveOption.equalsIgnoreCase("Save") || saveOption.equalsIgnoreCase("Save & New")) {
					if(isElementExists("//input[contains(@class,'btn') and @title='" + saveOption + "']"))
						xpath = "//input[contains(@class,'btn') and @title='" + saveOption + "']";
					else
					xpath = "//input[contains(@class,'btn') and @value='" + saveOption + "']";
					
					findElement(By.xpath(xpath)).click();
				} else {
					findElement(getLocator(saveOption)).click();
				}

				waitExplicit();
				pageTitleNew = driver.getTitle();

				FieldDetail recordID = new FieldDetail();
				recordID.setName("SFDC Record ID");
				recordID.setType("ID");
				waitForElementToBeVisible(waitTime, "Object Detail");
				recordID.setValue(driver.getCurrentUrl().substring(driver.getCurrentUrl().lastIndexOf("/") + 1));
				record.add(recordID);
				mapCreatedRecords.put(objectName, recordsList);
			}
		} catch (Exception e) {
			testStep[2] = objectName + " record could not be created because of exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			methodFailureMessage = testStep[2] + " " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
		} finally {
			testStep[3] = status;
			if (status.equals("Fail")) {
				throw new TestScriptException(methodFailureMessage, testStep);
			}

		}
		return testStep;
	}

	/**
	 * Creates a new record from the Related list of an open record
	 * 
	 * @param relListBtn
	 *            Name of the button on the related list
	 * @param objectName
	 *            Name of the object, there should be a metadata file related to
	 *            this object
	 * @param recValues
	 *            Values for various fields on the record
	 * @param saveOption
	 *            Specify Save options as Save, if one records is to be created,
	 *            or Save & New if another record is to be created after this
	 *            one
	 * @return String[] 
	 * 			  Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] createRecordFromRelatedList(String relListBtn, String objectName, String recValues,
			String saveOption) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Create " + objectName + " record using related list";
		testStep[1] = objectName + " record should be created";
		testStep[2] = objectName + " record is created";
		testStep[4] = "";
		String status = "Pass";
		try {
			
			String relatedListButtonXpath;
			
			if(relListBtn.contains(",")) {
				String temp[] = relListBtn.split(",");
				relatedListButtonXpath = "//h3[text()='" + temp[1] + "']//following::" + "input[contains(@class,'btn') and contains(@value, '" + temp[0] + "')]";
			} else {
				relatedListButtonXpath = "//input[contains(@class,'btn') and contains(@value, '" + relListBtn + "')]";
			}					
			
			List<Record> recordsList = getRecValues(recValues, objectName);
			String currentRecord = driver.getCurrentUrl().substring(driver.getCurrentUrl().lastIndexOf("/") + 1);

			for (Record record : recordsList) {
				if (recordsList.indexOf(record) != 0)
					openRecord(currentRecord);
										
				findElement(By.xpath(relatedListButtonXpath)).click();
				
				enterValuesForRecCreation(record, objectName);

				String pageTitle = driver.getTitle();
				String pageTitleNew;
				String xpath;

				if (saveOption.equalsIgnoreCase("Save") || saveOption.equalsIgnoreCase("Save & New")) {
					if(isElementExists("//input[contains(@class,'btn') and @title='" + saveOption + "']"))
						xpath = "//input[contains(@class,'btn') and @title='" + saveOption + "']";
					else
					xpath = "//input[contains(@class,'btn') and @value='" + saveOption + "']";
					
					findElement(By.xpath(xpath)).click();
				} else {
					findElement(getLocator(saveOption)).click();
				}

				waitExplicit();
				pageTitleNew = driver.getTitle();

				FieldDetail recordID = new FieldDetail();
				recordID.setName("SFDC Record ID");
				recordID.setType("ID");
				waitForElementToBeVisible(waitTime, "Object Detail");
				recordID.setValue(driver.getCurrentUrl().substring(driver.getCurrentUrl().lastIndexOf("/") + 1));
				record.add(recordID);				
			}
			mapCreatedRecords.put(objectName, recordsList);
		} catch (Exception e) {
			testStep[2] = objectName + " record could not be created because of exception. Please refer Logs/info.log for more details. ";
			status = "Fail";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			methodFailureMessage = testStep[2] + " " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
		} finally {
			testStep[3] = status;

			if (status.equals("Fail")) {
				throw new TestScriptException(methodFailureMessage, testStep);
			}

		}
		return testStep;
	}

	/**
	 * Opens the details page for any record with the provided record ID.
	 * 
	 * @param recordID
	 *            recordID of the record
	 * @return String[] 
	 * 			  Returns the result of the execution
	 * @throws TestScriptException
	 */

	public String[] openRecord(String recordID) throws TestScriptException {

		testStep = getStepInstance();
		testStep[0] = "Open record with record ID " + recordID;
		testStep[1] = "Record detail page should be opened";
		String status = "Pass";
		testStep[2] = "Record detail page is opened";
		testStep[4] = "";
		String recordURL = "";
		String currentURL = "";

		try {

			while (true) {
				if (driver.getCurrentUrl().contains("test.salesforce.com")
						|| driver.getCurrentUrl().contains("login.salesforce.com")) {
					continue;
				} else {
					currentURL = driver.getCurrentUrl();
					break;
				}
			}

			String pageTitle = driver.getTitle();
			if (pageTitle.contains("Community")) {
				int index = currentURL.indexOf("force.com/", 8) + "force.com/".length();
				int index2 = currentURL.indexOf("/", index);
				if (index2 == -1) {
					recordURL = currentURL.substring(0, index) + recordID;
				} else {
					index2 = index2 + "force.com/".length();
					recordURL = currentURL.substring(0, index2) + recordID;
				}
			} else {
				recordURL = currentURL.substring(0, currentURL.indexOf("force.com/", 8)) + "force.com/" + recordID;
			}

			driver.get(recordURL);
			waitForElementToBeVisible(waitTime, "Object Detail");
			if (!driver.getCurrentUrl().equalsIgnoreCase(recordURL)) {
				testStep[2] = "Record detail page could not be opened";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2];
			}
		} catch (Exception e) {
			if (testStep[4].isEmpty()) {
				testStep[2] = "Record detail page could not be opened because of error. Please refer Logs/info.log for more details. ";
				status = "Fail";
				testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}
		} finally {
			testStep[3] = status;

			if (status.equals("Fail")) {
				config.getLogger().error(methodFailureMessage);
				throw new TestScriptException(testStep[2]);
			}
		}
		return testStep;
	}

	/**
	 * Stores the specified field's value for the mentioned record of the object
	 * which was created earlier during execution. This method will always be
	 * utilized after createRecord or createRecordFromRelatedList method
	 * 
	 * @param objectName
	 *            Object name whose field value needs to be stored
	 * @param fieldName
	 *            Field name whose value needs to be stored
	 * @param objectIndex
	 *            Index of the object record whose field value need to be
	 *            stored. Index always start from 1 for first record.
	 * @param variable
	 *            Variable in whichi field value needs to be stored
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 * @throws TestScriptException
	 */
	public Object retreiveCreatedRecordData(String objectName, String fieldName, Integer objectIndex, String variable)
			throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Retrieve the field " + fieldName + " value for " + objectName + " and store it in the variable "
				+ variable;
		testStep[1] = "Field " + fieldName + " value for " + objectName + "should be stored in the variable "
				+ variable;
		String status = "Pass";
		testStep[2] = "Field " + fieldName + " value for " + objectName + "is stored in the variable " + variable;
		testStep[4] = "";

		try {
			Record record = mapCreatedRecords.get(objectName).get(objectIndex - 1);
			String fieldvalue = "";
			for (FieldDetail fieldDetail : record.getFieldList()) {
				if (fieldDetail.getName().equalsIgnoreCase(fieldName))
					fieldvalue = fieldDetail.getValue();
			}
			config.setMapGlobalVariable(variable, fieldvalue);
		} catch (Exception e) {
			testStep[2] = "Field " + fieldName + " value for " + objectName + " could not be stored in the variable "
					+ variable + " because of the error. Please refer Logs/info.log for more details. ";
			status = "Fail";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			methodFailureMessage = testStep[2] + " " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return true;
	}

	/**
	 * Sorts the object with by the mentioned sorting parameters and stores the
	 * result in the mentioned variable. It can be used only for single field
	 * This method will always be utilized after createRecord or
	 * createRecordFromRelatedList method
	 * 
	 * @param objectName
	 *            Object name which needs to be sort
	 * @param sortingVariable
	 *            First sorting parameter
	 * @param secondarySortingVariable
	 *            Second sorting parameter
	 * @param variable
	 *            Variable which stores the sorted result
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 * @throws TestScriptException
	 * 
	 */
	public Object sortCreatedRecords(String objectName, String sortingVariable, String secondarySortingVariable,
			String variable) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Verify that the records for the mentioned object is sorted as per the mentioned field name";
		testStep[1] = "Mentioned object should be sorted as per the mentioned field name";
		testStep[2] = "Mentioned object is sorted as per the mentioned field name";
		testStep[4] = "";
		String status = "Pass";

		try {
			TreeMap<Integer, List<String>> sortedMap = new TreeMap<Integer, List<String>>();
			List<Record> recordsList = mapCreatedRecords.get(objectName);
			int key = 0;
			String value;
			int sortingVariablePosition = 0;
			int secondarySortingVariablePosition = 0;
			List<String> list;

			for (FieldDetail fieldDetailName : recordsList.get(0).getFieldList()) {
				if (fieldDetailName.getName().equalsIgnoreCase(sortingVariable)) {
					break;
				}
				sortingVariablePosition++;
			}
			for (FieldDetail fieldDetailValue : recordsList.get(0).getFieldList()) {
				if (!fieldDetailValue.getName().equalsIgnoreCase(secondarySortingVariable)) {
					secondarySortingVariablePosition++;
				}
			}
			for (Record r : recordsList) {
				key = Integer.parseInt(r.getFieldList().get(sortingVariablePosition).getValue());
				value = r.getFieldList().get(0).getValue();

				if (sortedMap.containsKey(key)) {
					list = sortedMap.get(key);
				} else {
					list = new ArrayList<String>();
				}
				list.add(value);
				Collections.sort(list);
				sortedMap.put(key, list);
			}

			String sortedResults = "";
			// int size = sortedMap.keySet().size();
			Set set = sortedMap.keySet();
			Iterator itr = set.iterator();
			while (itr.hasNext()) {
				List<String> values = sortedMap.get(itr.next());
				for (String iterator : values) {
					if (!sortedResults.isEmpty())
						sortedResults = sortedResults + "," + iterator;
					else
						sortedResults = iterator;
				}
			}
			config.setMapGlobalVariable(variable, sortedResults);
			System.out.println("Sorted results are " + sortedResults);
		} catch (Exception e) {
			testStep[2] = "Mentioned object is not sorted because of the exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			methodFailureMessage = testStep[2] + " " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			throw new TestScriptException(methodFailureMessage, testStep);
		}

		return true;
	}

	/**
	 * Returns true if the two string are equal otherwise false. In case strings
	 * contain comma, it checks for equality of all the items though not in
	 * sequence Required for calendar verification in salesforce and portal
	 * 
	 * @param string1
	 *            First string to be compared
	 * @param string2
	 *            Second string to be compared
	 * @return True if two string match, false otherwise
	 * @throws TestScriptException 
	 */
	protected boolean equals(String string1, String string2) throws TestScriptException {
		Boolean result = true;
		try {
			if (string1.contains(",") || string2.contains(",")) {
				int matchItems = 0;
				String st1[] = string1.split(",");
				String st2[] = string2.split(",");
				if (st1.length == st2.length) {
					for (int i = 0; i < st1.length; i++) {
						for (int j = 0; j < st2.length; j++) {
							if (st1[i].trim().equalsIgnoreCase(st2[j].trim())) {
								matchItems++;
								break;
							}
						}
					}
					if (matchItems != st1.length) {
						result = false;
					}
				}
			} else {
				result = string1.equalsIgnoreCase(string2);
			}
		} catch (Exception e) {
			methodFailureMessage = "Equals method failed because of exception " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage);
		}
		return result;
	}

	/**
	 * Concatenates the 2 provided string
	 * 
	 * For type = Date, second string represents the date to be concatenated with the first string and the concatenated
	 * date will be in the format "yyyy-MM-dd"
	 * 
	 * @param firstString
	 *            first string to be concatenated.
	 * @param SecondString
	 *            second string to be concatenated.
	 * @param type
	 *            salesforce datatype for the second string.
	 * @param variable
	 *            variable for the storing the concatenated string.
	 * @return String[] 
	 * 			  Returns the result of the execution
	 * @throws TestScriptException
	 */

	public String[] concatenate(String firstString, String secondString, String type, String variable)
			throws TestScriptException {

		testStep = getStepInstance();
		testStep[0] = "Concatenate " + firstString + " with " + secondString;
		testStep[1] = "Concatenated string should be stored in " + variable;
		testStep[2] = "Concatenated string is stored in " + variable;
		testStep[4] = "";
		String status = "Pass";

		try {
			String resultantString = "";
			
			if (type.equalsIgnoreCase("Date")) {

				SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
				SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");

				Date date = formatter.parse(secondString);
				secondString = formatter1.format(date);

				resultantString = firstString + " " + secondString;

			} else {
				resultantString = firstString.concat(secondString);
			}
			config.setMapGlobalVariable(variable, resultantString);
		} catch (Exception e) {
			testStep[2] = "Concatenate method cannot be executed because of the exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			methodFailureMessage = testStep[2] + " " + e.getMessage();
			config.getLogger().error(methodFailureMessage);
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return testStep;
	}

	/** Executes any anonymous query and returns the true or false depending on whether the query was 
	 *  executed successfully or not
	 * @param username
	 * 				Username for which we have appropriate access for running execute query
	 * @param password
	 * 				Password for the used user appended with security token, if applicable
	 * @param authEndPoint
	 * 				AuthEndPoint for Partner API of the used Salesforce Org
	 * @param query
	 * 				Query to be executed
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 * @throws ConnectionException
	 * @throws TestScriptException 
	 */
	public Object executeAnonymous(String username, String password, String authEndPoint, String query) throws ConnectionException, TestScriptException{
		testStep = getStepInstance();
		testStep[0] = "Execute the mentioned anonymous query "+query;
		testStep[1] = "Mentioned anonymous query should be executed.";
		testStep[2] = "Mentioned anonymous query is executed successfully.";
		testStep[4] = "";
		String status = "Pass";
		Object result = null;		
		try {				
					
			sfUtility.setConnectorConfig(username,password,authEndPoint);
			sfUtility.setPartnerConnection(username, password, authEndPoint);
			ConnectorConfig connectorConfig = sfUtility.getConnectorConfig();
			sfUtility.setSoapConnection(sfUtility.getPartnerConnection(), connectorConfig);
			
			SoapConnection soapConnection = sfUtility.getSoapConnection();
			result = sfUtility.executeAnonymous(connectorConfig, soapConnection, query);
			
			if(result!=(Object)true){
				testStep[2] = result.toString();
				methodFailureMessage = testStep[2];
				status = "Fail";
				testStep[4] = null;
			}			
		}catch (Exception e) {
			if(result==null){
				testStep[2] = "Mentioned anonymous query cannot be executed because of exception.";
				status = "Fail";
				methodFailureMessage = testStep[2] + " " + e.getMessage();
			}			
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return true;
	}
	
	/**It retrieves the value of the field mentioned in the query and stores it in the specified variable
	 * @param query
	 * 			  Query with the field whose values needs to be retrieved
	 * @param variable
	 * 			  Variable to be used for storing the queried field value
	 * @return Object 
	 * 			  Returns true if method is executed successfully otherwise
	 * 			  returns the testResult with the reason for method failure
	 * @throws TestScriptException
	 */
	public Object retrieveInvisibleFieldValue(String query, String variable) throws TestScriptException{
		testStep = getStepInstance();
		testStep[0] = "Retrieve the value of the invisible field.";
		testStep[1] = "Invisible field value is retrieved";
		testStep[2] = "Field value should be retrieved.";
		testStep[4] = "";
		String status = "Pass";
		Object result = null;		
		String username = null;
		String password = null;
		String authEndPoint = null;	
		
		try {				
			username = ShareableDataManager.getDataMap().get("AdminUser");
			password = ShareableDataManager.getDataMap().get("AdminPassword");
			authEndPoint = ShareableDataManager.getDataMap().get("AuthEndPt");
			
			sfUtility.setConnectorConfig(username,password,authEndPoint);
			sfUtility.setPartnerConnection(username, password, authEndPoint);
			ConnectorConfig connectorConfig = sfUtility.getConnectorConfig();
			sfUtility.setSoapConnection(sfUtility.getPartnerConnection(), connectorConfig);
			
			SoapConnection soapConnection = sfUtility.getSoapConnection();
						
			LogInfo[] li = new LogInfo[1];
            li[0] = new LogInfo();
            li[0].setCategory(LogCategory.Apex_code);
            li[0].setLevel(LogCategoryLevel.Debug);          
	        
            soapConnection.setDebuggingHeader(li, LogType.Debugonly);
            result = sfUtility.executeAnonymous(connectorConfig, soapConnection, query);
            
            if(result==(Object)true){
	            String[] logs = soapConnection.getDebuggingInfo().getDebugLog().split("\\R",-1);
	            for(int i=0;i<logs.length;i++){	          	  
	          	  if(logs[i].contains("|USER_DEBUG|[1]|DEBUG|")){
	          		  System.out.println("Line "+i+" is "+logs[i]);
	          		  String temp[] = logs[i].split("\\|DEBUG\\|");
	          		  System.out.println("Debug value is "+temp[1]);
	          		  config.setMapGlobalVariable(variable, temp[1]);
	          	  }
	          	 }        
            }
            else{
            	testStep[2] = result.toString();
				methodFailureMessage = testStep[2];
				status = "Fail";
				testStep[4] = null;
            }			
		} catch (Exception e) {
				testStep[2] = "Field value cannot be retrieved because of the exception. Please refer Logs/info.log for more details.";
				status = "Fail";
				methodFailureMessage = testStep[2] + " " + e.getMessage();
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}
		return true;
	}
	
	/**
	 * Clicks and opens SFDC tab specified. If the tab is not found in the Tab
	 * Navigation ribbon, All tabs page is opened and the specified tab is
	 * clicked from there
	 * 
	 * @param name
	 *            Name of the tab
	 * @return String[] 
	 *            Returns the result of the execution
	 * @throws TestScriptException
	 */
	public String[] clickTab(String name) throws TestScriptException {
		testStep = getStepInstance();
		testStep[0] = "Click the tab " + name;
		testStep[1] = "Tab should be clicked";
		testStep[2] = "Tab is clicked";
		testStep[4] = "";
		String status = "Pass";

		try {
			WebElement webElement = null;
			try {
				By webEle = getLocator(name);
				webElement = findElement(webEle);
				webElement.click();
			} catch (Exception e) {
				webElement = findElement(By.xpath("//*[@id='AllTab_Tab']/a"));
				webElement.click();
				String tabXpath = "//a[contains(@class,'listRelatedObject') and text()='" + name + "']";
				// String tabXpath="//*[@id='tabBar']/li/a[text()='"+name+"']";
				WebElement reqTab = findElement(By.xpath(tabXpath));
				reqTab.click();
			}
		} catch (Exception e) {
			testStep[2] = "Tab could not be clicked because of an Exception. Please refer Logs/info.log for more details.";
			status = "Fail";
			testStep[4] = ScreenShot.takeBrowserSnapShot(driver, config.getSnapshotFolder());
			methodFailureMessage = testStep[2] + " " + e.getMessage();
		} finally {
			testStep[3] = status;
		}
		if (status.equals("Fail")) {
			config.getLogger().error(methodFailureMessage);
			throw new TestScriptException(methodFailureMessage, testStep);
		}

		return testStep;
	}
	
}
