package com.appirio.automation.utility;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.openqa.selenium.WebDriver;

import com.appirio.automation.configuration.Configuration;
import com.appirio.automation.configuration.ShareableDataManager;
import com.appirio.automation.exception.TestScriptException;

public class Utility {
	static int defaultSleepTime = 1000;
	private static Configuration config;
	private static final String KEY_FILE = "./Password Encryption/public.key";
	private static final String AES = "AES";
	
	public Utility(Configuration mainConfig){
		config = mainConfig;
	}

	public static boolean switchToWindowUsingTitle(WebDriver driver, String title) {

		String currentWindow = driver.getWindowHandle();
		Set<String> availableWindows = driver.getWindowHandles();
		if (!availableWindows.isEmpty()) {
			for (String windowId : availableWindows) {
				if ((driver.switchTo().window(windowId).getTitle().equals(title))
						|| (driver.switchTo().window(windowId).getTitle().contains(title))) {
					return true;
				} else {
					driver.switchTo().window(currentWindow);
				}
			}
		}
		return false;
	}

	// --------------------------------------------------------------------------
	/**
	 * Forces the thread to sleep for time specified in the variable -
	 * Global_Settings.sleep_ms
	 */
	public static void sleep() {
		try {
			Thread.sleep(defaultSleepTime);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void sleep(int sleeptime) {
		try {
			Thread.sleep(sleeptime);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String replaceHashTag(String text) throws TestScriptException {
		
		if(text.toLowerCase().contains("#replace")){
        	Pattern pattern = Pattern.compile("#replace\\((.+?)\\)",Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(text);
            while(matcher.find()){
            	            	
            	String actualText=ShareableDataManager.getDataMap().get(matcher.group(1));
            	
            	if(actualText==null){
            		actualText = Configuration.getTestCaseLocalInputData().get(matcher.group(1));
            	}
            	//Treating null cell as blank cell
            	if(actualText==null){
            		throw new TestScriptException("Error: "+matcher.group(1)+" field has not found in input data sheet. ");
            	}
            	if(actualText.contains("$")){
            		text=text.replaceFirst("(?i)\\#replace\\("+matcher.group(1)+"\\)", Matcher.quoteReplacement(actualText));  
            	}
            	else{
            		text=text.replaceFirst("(?i)\\#replace\\("+matcher.group(1)+"\\)", actualText); 
            	}
       		}
            
        }		

		if (text.toLowerCase().contains("#getvalue(")) {
			Pattern pattern = Pattern.compile("#getValue\\((.+?)\\)", Pattern.CASE_INSENSITIVE);
			Matcher matcher = pattern.matcher(text);
			while (matcher.find()) {
				// String
				// actualText=ShareableDataManager.getVariableValue(matcher.group(1));
				String actualText = config.getGlobalVariable(matcher.group(1));
				
				if(actualText.contains("$")){
					text = text.replaceFirst("(?i)\\#getValue\\(" + matcher.group(1) + "\\)", Matcher.quoteReplacement(actualText));  
            	}
            	else{
            		text = text.replaceFirst("(?i)\\#getValue\\(" + matcher.group(1) + "\\)", actualText);
            	}
			}
		}
		
		if (text.toLowerCase().contains("#password(")) {
			Pattern pattern = Pattern.compile("#password\\((.+?)\\)", Pattern.CASE_INSENSITIVE);
			Matcher matcher = pattern.matcher(text);
			while (matcher.find()) {
				String actualText = decrypt(matcher.group(1));
				//text = text.replaceFirst("(?i)\\#password\\(" + matcher.group(1) + "\\)", actualText);
				if(actualText.contains("$")){
					text = text.replaceFirst("(?i)\\#password\\(" + matcher.group(1) + "\\)", Matcher.quoteReplacement(actualText));  
            	}
            	else{
            		text = text.replaceFirst("(?i)\\#password\\(" + matcher.group(1) + "\\)", actualText);
            	}
				
			}
		}

		return text;
	}
	
	public static String decrypt(String ciphertext) {
		String OriginalPassword = null;
		ObjectInputStream inputStream = null;
		try {
			inputStream = new ObjectInputStream(new FileInputStream(KEY_FILE));
			final String tempkey = (String) inputStream.readObject();
			byte[] bytekey = hexStringToByteArray(tempkey);
			SecretKeySpec sks = new SecretKeySpec(bytekey, AES);
			Cipher cipher = Cipher.getInstance(AES);
			cipher.init(Cipher.DECRYPT_MODE, sks);
			byte[] decrypted = cipher.doFinal(hexStringToByteArray(ciphertext));
			OriginalPassword = new String(decrypted);
		} catch (Exception e) {
			e.getMessage();
		}finally{
			
		}
		return OriginalPassword;
	}
	
	public static byte[] hexStringToByteArray(String s) {
		byte[] b = new byte[s.length() / 2];
		for (int i = 0; i < b.length; i++) {
			int index = i * 2;
			int v = Integer.parseInt(s.substring(index, index + 2), 16);
			b[i] = (byte) v;
		}
		return b;
	}

}
